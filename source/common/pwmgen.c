/*
 * File:   pwmgen.c
 * Author: Dorus
 *
 * Created on 15 januari 2017, 17:08
 */

#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <stdbool.h>
#include <math.h>
#include <avr/io.h>

#include "pwmgen.h"
#include "timer.h"
#include "uart.h"

struct PwmGenChannel {
    bool    available;          /* False when channel is in use. */
    bool    enabled;            /* False when channel is disabled. */
    uint8_t duty_cycle;         /* Duty cycle 0-100%. */
    volatile uint32_t T;        /* Period in ms. */
    volatile uint32_t Ton;      /* Period on in ms. */
    volatile uint32_t t;        /* Time in ms. */

    volatile uint8_t* port;     /* Output port. */
    volatile uint8_t mask;      /* Output pin mask. */
};


#define PWMGEN_TIMER_CHANNEL 2

struct PwmGenChannel Channels[PWMGEN_CONFIG_MAX_CHANNELS];
uint16_t max_frequency;
uint32_t dt;

void i_PwmGenUpdateChannels(void);
uint8_t i_PwmGenChannelGetFree(void);
int8_t i_PwmGenChannelSetFree(uint8_t channel);

int8_t PwmGenInit(uint16_t max_frequency)
{

    max_frequency *= 100;
    double time_s = ((double)1 / (double)max_frequency);
    dt =  time_s * 1e6;
    unsigned char result;
    result = timer_Init(max_frequency, i_PwmGenUpdateChannels);
    uint8_t i;
    for(i = 0; i < PWMGEN_CONFIG_MAX_CHANNELS; i++) {
        Channels[i].available = true;
        Channels[i].enabled = false;
    }
    timer_Start();
    return (result);
}

uint8_t PwmGenChannelCreate(uint8_t frequency, uint8_t dc,  volatile uint8_t* port,  uint8_t pin)
{
    uint8_t new_channel = i_PwmGenChannelGetFree();
    if(new_channel != INVALID_CHANNEL) {
        Channels[new_channel].port = port;
        Channels[new_channel].mask = (0 | (1 << pin));
        double time_s = ((double)1 / (double)frequency);
        Channels[new_channel].T =  time_s * 1e6;
        Channels[new_channel].t = 0;
        PwmGenChannelDutyCycleSet(new_channel, dc);
    }
    return (new_channel);
}

int8_t PwmGenChannelDestroy(uint8_t channel)
{
    return (i_PwmGenChannelSetFree(channel));
}

int8_t PwmGenChannelStart(uint8_t channel)
{
    if(channel >= PWMGEN_CONFIG_MAX_CHANNELS) {
        return (-1);
    }
    Channels[channel].enabled = true;
    return (0);
}

int8_t PwmGenChannelStop(uint8_t channel)
{
    if(channel >= PWMGEN_CONFIG_MAX_CHANNELS) {
        return (-1);
    }
    Channels[channel].enabled = false;
    return (0);
}

int8_t PwmGenChannelDutyCycleSet(uint8_t channel, uint8_t dc)
{
    if(channel >= PWMGEN_CONFIG_MAX_CHANNELS || dc > 100) {
        return (-1);
    }

    Channels[channel].duty_cycle = dc;
    Channels[channel].Ton = ((float)dc / 100) *  Channels[channel].T;

    return (0);
}

uint8_t PwmGenChannelDutyCycleGet(uint8_t channel)
{
    return (Channels[channel].duty_cycle);
}


/* Internal functions */

uint8_t i_PwmGenChannelGetFree(void)
{
    uint8_t free_channel = INVALID_CHANNEL;
    uint8_t i;
    for(i = 0; i < PWMGEN_CONFIG_MAX_CHANNELS; i++) {
        if (Channels[i].available == true) {
            free_channel = i;
            Channels[i].available = false;
            break;
        }
    }
    return (free_channel);
}


int8_t i_PwmGenChannelSetFree(uint8_t channel)
{
    if(channel >= PWMGEN_CONFIG_MAX_CHANNELS) {
        return (-1);
    }
    Channels[channel].available = true;
    Channels[channel].enabled = false;
    return (0);
}


void i_PwmGenUpdateChannels(void)
{
    uint8_t i;
    for(i = 0; i < PWMGEN_CONFIG_MAX_CHANNELS; i++) {
        if(Channels[i].enabled == true) {
            Channels[i].t = Channels[i].t + dt;
            if(Channels[i].t == Channels[i].T) {
                *(Channels[i].port) |= Channels[i].mask; /* Set pin high. */
                Channels[i].t = 0; /* Reset time. */
            } else if(Channels[i].t == Channels[i].Ton) {
                *(Channels[i].port) &= ~(Channels[i].mask);  /* Set pin low. */
            }
        }
    }
}

