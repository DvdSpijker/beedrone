/*
 * File:   pwmgen.h
 * Author: Dorus
 *
 * Created on 15 januari 2017, 17:06
 */

#ifndef PWMGEN_H
#define PWMGEN_H

#include <stdint.h>

#define INVALID_CHANNEL 0xFF
#define PWMGEN_CONFIG_MAX_CHANNELS 4
#define F_PB 8e6

int8_t PwmGenInit(uint16_t max_frequency);

uint8_t PwmGenChannelCreate(uint8_t frequency, uint8_t dc,  volatile uint8_t* port,  uint8_t pin);

int8_t PwmGenChannelDestroy(uint8_t channel);

int8_t PwmGenChannelStart(uint8_t channel);

int8_t PwmGenChannelStop(uint8_t channel);

int8_t PwmGenChannelDutyCycleSet(uint8_t channel, uint8_t dc);

uint8_t PwmGenChannelDutyCycleGet(uint8_t channel);


#endif  /* PWMGEN_H */

