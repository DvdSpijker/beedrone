/*
 * led_control.c
 *
 * Created: 18-3-2017 12:08:07
 *  Author: Dorus
 */

#include "led_control.h"

void FlightLedControl(void* parameter)
{
    static Id_t status_id = INV_ID;
    static Id_t Timerid = INV_ID;

    if (status_id != INV_ID) {
        status_id = EventgroupCreate();
        EventgroupFlagsClear(status_id, 0xFF);
    } else {
        if (EventgroupFlagsGet(status_id, TASK_FLIGHTLEDCONTROL_STATE_INIT) == 0) {
            //initialization
            GpioPinModeSet(&TASK_FLIGHTLEDCONTROL_LED_DDR, TASK_FLIGHTLEDCONTROL_LED_CONSTANT_FRONT, Output);
            GpioPinModeSet(&TASK_FLIGHTLEDCONTROL_LED_DDR, TASK_FLIGHTLEDCONTROL_LED_CONSTANT_LEFT_BACK, Output);
            GpioPinModeSet(&TASK_FLIGHTLEDCONTROL_LED_DDR, TASK_FLIGHTLEDCONTROL_LED_CONSTANT_RIGHT_BACK, Output);
            GpioPinModeSet(&TASK_FLIGHTLEDCONTROL_LED_DDR, TASK_FLIGHTLEDCONTROL_LED_CONSTANT_LEFT_FRONT, Output);
            GpioPinModeSet(&TASK_FLIGHTLEDCONTROL_LED_DDR, TASK_FLIGHTLEDCONTROL_LED_CONSTANT_RIGHT_FRONT, Output);
            GpioPinModeSet(&TASK_FLIGHTLEDCONTROL_LED_DDR, TASK_FLIGHTLEDCONTROL_LED_STROBE_BACK, Output);
            GpioPinModeSet(&TASK_FLIGHTLEDCONTROL_LED_DDR, TASK_FLIGHTLEDCONTROL_LED_STROBE_LEFT_BACK, Output);
            GpioPinModeSet(&TASK_FLIGHTLEDCONTROL_LED_DDR, TASK_FLIGHTLEDCONTROL_LED_STROBE_RIGHT_BACK, Output);
            GpioPinModeSet(&TASK_FLIGHTLEDCONTROL_LED_DDR, TASK_FLIGHTLEDCONTROL_LED_STROBE_LEFT_FRONT, Output);
            GpioPinModeSet(&TASK_FLIGHTLEDCONTROL_LED_DDR, TASK_FLIGHTLEDCONTROL_LED_STROBE_RIGHT_FRONT, Output);

            Timerid = TimerCreate(TASK_FLIGHTLEDCONTROL_INTERVAL_STROBE_SHORT, (TIMER_PARAMETER_AR | TIMER_PARAMETER_P));

            EventgroupFlagsSet(status_id, TASK_FLIGHTLEDCONTROL_STATE_INIT);

            GpioPinStateSet(&TASK_FLIGHTLEDCONTROL_LED_PORT, TASK_FLIGHTLEDCONTROL_LED_CONSTANT_FRONT, High);
            GpioPinStateSet(&TASK_FLIGHTLEDCONTROL_LED_PORT, TASK_FLIGHTLEDCONTROL_LED_CONSTANT_LEFT_BACK, High);
            GpioPinStateSet(&TASK_FLIGHTLEDCONTROL_LED_PORT, TASK_FLIGHTLEDCONTROL_LED_CONSTANT_RIGHT_BACK, High);
            GpioPinStateSet(&TASK_FLIGHTLEDCONTROL_LED_PORT, TASK_FLIGHTLEDCONTROL_LED_CONSTANT_LEFT_FRONT, High);
            GpioPinStateSet(&TASK_FLIGHTLEDCONTROL_LED_PORT, TASK_FLIGHTLEDCONTROL_LED_CONSTANT_LEFT_BACK, High);

            TimerStart(Timerid);
        } else {
            //loop
            GpioPinStateSet(&TASK_FLIGHTLEDCONTROL_LED_PORT, TASK_FLIGHTLEDCONTROL_LED_STROBE_BACK, Toggle);
            GpioPinStateSet(&TASK_FLIGHTLEDCONTROL_LED_PORT, TASK_FLIGHTLEDCONTROL_LED_STROBE_LEFT_BACK, Toggle);
            GpioPinStateSet(&TASK_FLIGHTLEDCONTROL_LED_PORT, TASK_FLIGHTLEDCONTROL_LED_STROBE_RIGHT_BACK, Toggle);
            GpioPinStateSet(&TASK_FLIGHTLEDCONTROL_LED_PORT, TASK_FLIGHTLEDCONTROL_LED_STROBE_LEFT_FRONT, Toggle);
            GpioPinStateSet(&TASK_FLIGHTLEDCONTROL_LED_PORT, TASK_FLIGHTLEDCONTROL_LED_STROBE_LEFT_BACK, Toggle);

            if (EventgroupFlagsGet(status_id, TASK_FLIGHTLEDCONTROL_STATE_STROBE_SHORT_ON_1) == 0) {
                TimerIntervalSet(Timerid, TASK_FLIGHTLEDCONTROL_INTERVAL_STROBE_USHORT);
                EventgroupFlagsSet(status_id, TASK_FLIGHTLEDCONTROL_STATE_STROBE_SHORT_ON_1);
            } else if (EventgroupFlagsGet(status_id, TASK_FLIGHTLEDCONTROL_STATE_STROBE_SHORT_OFF_1) == 0) {
                TimerIntervalSet(Timerid, TASK_FLIGHTLEDCONTROL_INTERVAL_STROBE_SHORT);
                EventgroupFlagsSet(status_id, TASK_FLIGHTLEDCONTROL_STATE_STROBE_SHORT_OFF_1);
            } else if (EventgroupFlagsGet(status_id, TASK_FLIGHTLEDCONTROL_STATE_STROBE_SHORT_ON_2) == 0) {
                TimerIntervalSet(Timerid, TASK_FLIGHTLEDCONTROL_INTERVAL_STROBE_USHORT);
                EventgroupFlagsSet(status_id, TASK_FLIGHTLEDCONTROL_STATE_STROBE_SHORT_ON_2);
            } else if (EventgroupFlagsGet(status_id, TASK_FLIGHTLEDCONTROL_STATE_STROBE_SHORT_OFF_2) == 0) {
                TimerIntervalSet(Timerid, TASK_FLIGHTLEDCONTROL_INTERVAL_STROBE_LONG);
                EventgroupFlagsClear(status_id, TASK_FLIGHTLEDCONTROL_STATE_STROBE_RESET);
            }
        }
    }

    TaskWait(FlightLedControl, Timerid, 0, 0);

}