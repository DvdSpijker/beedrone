

#include "uart.h"
#include <avr/interrupt.h>
#include <avr/io.h>
#include <stdint.h>

volatile char RX_Buffer[UARTRX_BUFSIZE];

volatile uint8_t R_i,W_i;
volatile uint8_t data_avail;

volatile uint8_t buf_status; //0 = empty, 1 = data present, 2 = full

void uart_Init(uint16_t baudrate)
{
    R_i = W_i = (UARTRX_BUFSIZE -1);
    buf_status = 0;
    data_avail = 0;

    int UBBR_VALUE = (F_CPU/16/baudrate - 1);
    UBRR0H = (uint8_t)(UBBR_VALUE>>8);
    UBRR0L = (uint8_t)UBBR_VALUE;
    UCSR0B = (1 << RXEN0) | (1 << TXEN0);
    UCSR0C = (3<<UCSZ00); //8 data bits
    UCSR0C &= ~(1<<USBS0); //1 stop bit
    UCSR0B |= (1 << RXCIE0); // Enable the USART Receive Complete interrupt (USART_RXC)

}


void uart_SendChar(char character)
{

    //wait while previous byte is completed
    while(!(UCSR0A&(1<<UDRE0)));
    // Transmit data
    UDR0 = character;
}

void uart_SendString(const char* str)
{
    int i = 0;
    //wait while previous byte is completed
    while(str[i] != '\0') {
        uart_SendChar(str[i]);
        i++;
    }

}


uint8_t uart_Receive(char* target, uint8_t amount)
{
    if(buf_status == 0) {
        return 0;    //no data present
    }

    uint8_t i = 0;
    do {
        if (R_i == (UARTRX_BUFSIZE-1) && data_avail) {
            R_i = 0;
        }

        else if(data_avail) {
            R_i++;
        }

        else {
            buf_status = 0;
            break;
        }

        *(target+i) = RX_Buffer[R_i];
        data_avail--;
        i++;
    } while(i<(amount) && data_avail);

    if(!data_avail) {
        buf_status = 0;
    } else {
        buf_status = 1;
    }

    return i;

}

uint8_t uart_BufferDataAmountGet(void)
{
    return data_avail;
}

uint8_t uart_BufferStatusGet(void)
{
    return buf_status;
}

uint8_t uart_BufferContains(const char check_char)
{
    for (uint8_t i=0; i<data_avail; i++) {
        if(RX_Buffer[i] == check_char) {
            return i;
        }
    }
    return 0;
}

void uart_BufferFlush(void)
{
    R_i = W_i = (UARTRX_BUFSIZE -1);
    buf_status = 0;
}

ISR(USART_RX_vect)
{
//  uart_SendString("rx\n");
    if (buf_status != 2) {

        if (W_i == (UARTRX_BUFSIZE-1) && (UARTRX_BUFSIZE-data_avail)) {
            W_i = 0;
        } else if (UARTRX_BUFSIZE-data_avail) {
            W_i++;
        }

        buf_status = 1;

        if(data_avail == UARTRX_BUFSIZE) {
            buf_status = 2;
        }

        RX_Buffer[W_i] = UDR0;
        data_avail++;

    }

}
