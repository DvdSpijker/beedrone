<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE eagle SYSTEM "eagle.dtd">
<eagle version="7.2.0">
<drawing>
<settings>
<setting alwaysvectorfont="no"/>
<setting verticaltext="up"/>
</settings>
<grid distance="0.1" unitdist="inch" unit="inch" style="lines" multiple="1" display="no" altdistance="0.01" altunitdist="inch" altunit="inch"/>
<layers>
<layer number="1" name="Top" color="4" fill="1" visible="no" active="no"/>
<layer number="2" name="Route2" color="1" fill="3" visible="no" active="no"/>
<layer number="3" name="Route3" color="4" fill="3" visible="no" active="no"/>
<layer number="4" name="Route4" color="1" fill="4" visible="no" active="no"/>
<layer number="5" name="Route5" color="4" fill="4" visible="no" active="no"/>
<layer number="6" name="Route6" color="1" fill="8" visible="no" active="no"/>
<layer number="7" name="Route7" color="4" fill="8" visible="no" active="no"/>
<layer number="8" name="Route8" color="1" fill="2" visible="no" active="no"/>
<layer number="9" name="Route9" color="4" fill="2" visible="no" active="no"/>
<layer number="10" name="Route10" color="1" fill="7" visible="no" active="no"/>
<layer number="11" name="Route11" color="4" fill="7" visible="no" active="no"/>
<layer number="12" name="Route12" color="1" fill="5" visible="no" active="no"/>
<layer number="13" name="Route13" color="4" fill="5" visible="no" active="no"/>
<layer number="14" name="Route14" color="1" fill="6" visible="no" active="no"/>
<layer number="15" name="Route15" color="4" fill="6" visible="no" active="no"/>
<layer number="16" name="Bottom" color="1" fill="1" visible="no" active="no"/>
<layer number="17" name="Pads" color="2" fill="1" visible="no" active="no"/>
<layer number="18" name="Vias" color="2" fill="1" visible="no" active="no"/>
<layer number="19" name="Unrouted" color="6" fill="1" visible="no" active="no"/>
<layer number="20" name="Dimension" color="15" fill="1" visible="no" active="no"/>
<layer number="21" name="tPlace" color="7" fill="1" visible="no" active="no"/>
<layer number="22" name="bPlace" color="7" fill="1" visible="no" active="no"/>
<layer number="23" name="tOrigins" color="15" fill="1" visible="no" active="no"/>
<layer number="24" name="bOrigins" color="15" fill="1" visible="no" active="no"/>
<layer number="25" name="tNames" color="7" fill="1" visible="no" active="no"/>
<layer number="26" name="bNames" color="7" fill="1" visible="no" active="no"/>
<layer number="27" name="tValues" color="7" fill="1" visible="no" active="no"/>
<layer number="28" name="bValues" color="7" fill="1" visible="no" active="no"/>
<layer number="29" name="tStop" color="7" fill="3" visible="no" active="no"/>
<layer number="30" name="bStop" color="7" fill="6" visible="no" active="no"/>
<layer number="31" name="tCream" color="7" fill="4" visible="no" active="no"/>
<layer number="32" name="bCream" color="7" fill="5" visible="no" active="no"/>
<layer number="33" name="tFinish" color="6" fill="3" visible="no" active="no"/>
<layer number="34" name="bFinish" color="6" fill="6" visible="no" active="no"/>
<layer number="35" name="tGlue" color="7" fill="4" visible="no" active="no"/>
<layer number="36" name="bGlue" color="7" fill="5" visible="no" active="no"/>
<layer number="37" name="tTest" color="7" fill="1" visible="no" active="no"/>
<layer number="38" name="bTest" color="7" fill="1" visible="no" active="no"/>
<layer number="39" name="tKeepout" color="4" fill="11" visible="no" active="no"/>
<layer number="40" name="bKeepout" color="1" fill="11" visible="no" active="no"/>
<layer number="41" name="tRestrict" color="4" fill="10" visible="no" active="no"/>
<layer number="42" name="bRestrict" color="1" fill="10" visible="no" active="no"/>
<layer number="43" name="vRestrict" color="2" fill="10" visible="no" active="no"/>
<layer number="44" name="Drills" color="7" fill="1" visible="no" active="no"/>
<layer number="45" name="Holes" color="7" fill="1" visible="no" active="no"/>
<layer number="46" name="Milling" color="3" fill="1" visible="no" active="no"/>
<layer number="47" name="Measures" color="7" fill="1" visible="no" active="no"/>
<layer number="48" name="Document" color="7" fill="1" visible="no" active="no"/>
<layer number="49" name="Reference" color="7" fill="1" visible="no" active="no"/>
<layer number="50" name="dxf" color="7" fill="1" visible="no" active="no"/>
<layer number="51" name="tDocu" color="7" fill="1" visible="no" active="no"/>
<layer number="52" name="bDocu" color="7" fill="1" visible="no" active="no"/>
<layer number="53" name="tGND_GNDA" color="7" fill="9" visible="no" active="no"/>
<layer number="54" name="bGND_GNDA" color="1" fill="9" visible="no" active="no"/>
<layer number="56" name="wert" color="7" fill="1" visible="no" active="no"/>
<layer number="57" name="tCAD" color="7" fill="1" visible="no" active="no"/>
<layer number="59" name="tCarbon" color="7" fill="1" visible="no" active="no"/>
<layer number="60" name="bCarbon" color="7" fill="1" visible="no" active="no"/>
<layer number="90" name="Modules" color="5" fill="1" visible="yes" active="yes"/>
<layer number="91" name="Nets" color="2" fill="1" visible="yes" active="yes"/>
<layer number="92" name="Busses" color="1" fill="1" visible="yes" active="yes"/>
<layer number="93" name="Pins" color="2" fill="1" visible="no" active="yes"/>
<layer number="94" name="Symbols" color="4" fill="1" visible="yes" active="yes"/>
<layer number="95" name="Names" color="7" fill="1" visible="yes" active="yes"/>
<layer number="96" name="Values" color="7" fill="1" visible="yes" active="yes"/>
<layer number="97" name="Info" color="7" fill="1" visible="yes" active="yes"/>
<layer number="98" name="Guide" color="6" fill="1" visible="yes" active="yes"/>
<layer number="99" name="SpiceOrder" color="7" fill="1" visible="yes" active="yes"/>
<layer number="100" name="Muster" color="7" fill="1" visible="no" active="no"/>
<layer number="101" name="Patch_Top" color="12" fill="4" visible="yes" active="yes"/>
<layer number="102" name="Vscore" color="7" fill="1" visible="yes" active="yes"/>
<layer number="103" name="tMap" color="7" fill="1" visible="yes" active="yes"/>
<layer number="104" name="Name" color="16" fill="1" visible="yes" active="yes"/>
<layer number="105" name="tPlate" color="7" fill="1" visible="yes" active="yes"/>
<layer number="106" name="bPlate" color="7" fill="1" visible="yes" active="yes"/>
<layer number="107" name="Crop" color="7" fill="1" visible="yes" active="yes"/>
<layer number="108" name="tplace-old" color="10" fill="1" visible="yes" active="yes"/>
<layer number="109" name="ref-old" color="11" fill="1" visible="yes" active="yes"/>
<layer number="110" name="fp0" color="7" fill="1" visible="yes" active="yes"/>
<layer number="111" name="LPC17xx" color="7" fill="1" visible="yes" active="yes"/>
<layer number="112" name="tSilk" color="7" fill="1" visible="yes" active="yes"/>
<layer number="113" name="IDFDebug" color="7" fill="1" visible="yes" active="yes"/>
<layer number="116" name="Patch_BOT" color="9" fill="4" visible="yes" active="yes"/>
<layer number="118" name="Rect_Pads" color="7" fill="1" visible="yes" active="yes"/>
<layer number="121" name="_tsilk" color="7" fill="1" visible="yes" active="yes"/>
<layer number="122" name="_bsilk" color="7" fill="1" visible="yes" active="yes"/>
<layer number="123" name="tTestmark" color="7" fill="1" visible="yes" active="yes"/>
<layer number="124" name="bTestmark" color="7" fill="1" visible="yes" active="yes"/>
<layer number="125" name="_tNames" color="7" fill="1" visible="yes" active="yes"/>
<layer number="126" name="_bNames" color="7" fill="1" visible="yes" active="yes"/>
<layer number="127" name="_tValues" color="7" fill="1" visible="yes" active="yes"/>
<layer number="128" name="_bValues" color="7" fill="1" visible="yes" active="yes"/>
<layer number="129" name="Mask" color="7" fill="1" visible="yes" active="yes"/>
<layer number="131" name="tAdjust" color="7" fill="1" visible="yes" active="yes"/>
<layer number="132" name="bAdjust" color="7" fill="1" visible="yes" active="yes"/>
<layer number="144" name="Drill_legend" color="7" fill="1" visible="yes" active="yes"/>
<layer number="150" name="Notes" color="7" fill="1" visible="yes" active="yes"/>
<layer number="151" name="HeatSink" color="7" fill="1" visible="yes" active="yes"/>
<layer number="152" name="_bDocu" color="7" fill="1" visible="yes" active="yes"/>
<layer number="153" name="FabDoc1" color="7" fill="1" visible="yes" active="yes"/>
<layer number="154" name="FabDoc2" color="7" fill="1" visible="yes" active="yes"/>
<layer number="155" name="FabDoc3" color="7" fill="1" visible="yes" active="yes"/>
<layer number="199" name="Contour" color="7" fill="1" visible="yes" active="yes"/>
<layer number="200" name="200bmp" color="1" fill="10" visible="yes" active="yes"/>
<layer number="201" name="201bmp" color="2" fill="10" visible="yes" active="yes"/>
<layer number="202" name="202bmp" color="3" fill="10" visible="yes" active="yes"/>
<layer number="203" name="203bmp" color="4" fill="10" visible="yes" active="yes"/>
<layer number="204" name="204bmp" color="5" fill="10" visible="yes" active="yes"/>
<layer number="205" name="205bmp" color="6" fill="10" visible="yes" active="yes"/>
<layer number="206" name="206bmp" color="7" fill="10" visible="yes" active="yes"/>
<layer number="207" name="207bmp" color="8" fill="10" visible="yes" active="yes"/>
<layer number="208" name="208bmp" color="9" fill="10" visible="yes" active="yes"/>
<layer number="209" name="209bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="210" name="210bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="211" name="211bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="212" name="212bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="213" name="213bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="214" name="214bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="215" name="215bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="216" name="216bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="217" name="217bmp" color="18" fill="1" visible="no" active="no"/>
<layer number="218" name="218bmp" color="19" fill="1" visible="no" active="no"/>
<layer number="219" name="219bmp" color="20" fill="1" visible="no" active="no"/>
<layer number="220" name="220bmp" color="21" fill="1" visible="no" active="no"/>
<layer number="221" name="221bmp" color="22" fill="1" visible="no" active="no"/>
<layer number="222" name="222bmp" color="23" fill="1" visible="no" active="no"/>
<layer number="223" name="223bmp" color="24" fill="1" visible="no" active="no"/>
<layer number="224" name="224bmp" color="25" fill="1" visible="no" active="no"/>
<layer number="225" name="225bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="226" name="226bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="227" name="227bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="228" name="228bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="229" name="229bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="230" name="230bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="231" name="231bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="248" name="Housing" color="7" fill="1" visible="yes" active="yes"/>
<layer number="249" name="Edge" color="7" fill="1" visible="yes" active="yes"/>
<layer number="250" name="Descript" color="3" fill="1" visible="no" active="no"/>
<layer number="251" name="SMDround" color="12" fill="11" visible="no" active="no"/>
<layer number="254" name="cooling" color="7" fill="1" visible="yes" active="yes"/>
<layer number="255" name="routoute" color="7" fill="1" visible="yes" active="yes"/>
</layers>
<schematic xreflabel="%F%N/%S.%C%R" xrefpart="/%S.%C%R">
<libraries>
<library name="memory-micron">
<description>&lt;b&gt;MICRON Flash Memory&lt;/b&gt;&lt;p&gt;
www.micron.com&lt;br&gt;
&lt;author&gt;Created by librarian@cadsoft.de&lt;/author&gt;&lt;p&gt;</description>
<packages>
<package name="TSOP48">
<description>&lt;b&gt;48-Pin TSOP&lt;/b&gt; OCPL Type 1 (WC Package Code)&lt;p&gt;
Source: http://download.micron.com/pdf/datasheets/flash/nand/4gb_nand_m40a.pdf</description>
<wire x1="-9.1" y1="5.9" x2="9.1" y2="5.9" width="0.2032" layer="21"/>
<wire x1="9.1" y1="5.9" x2="9.1" y2="-5.9" width="0.2032" layer="21"/>
<wire x1="9.1" y1="-5.9" x2="-9.1" y2="-5.9" width="0.2032" layer="21"/>
<wire x1="-9.1" y1="-5.9" x2="-9.1" y2="5.9" width="0.2032" layer="21"/>
<circle x="-7.5" y="4.5" radius="0.7071" width="0.27" layer="21"/>
<smd name="1" x="-9.75" y="5.75" dx="0.9" dy="0.3" layer="1"/>
<smd name="2" x="-9.75" y="5.25" dx="0.9" dy="0.3" layer="1"/>
<smd name="3" x="-9.75" y="4.75" dx="0.9" dy="0.3" layer="1"/>
<smd name="4" x="-9.75" y="4.25" dx="0.9" dy="0.3" layer="1"/>
<smd name="5" x="-9.75" y="3.75" dx="0.9" dy="0.3" layer="1"/>
<smd name="6" x="-9.75" y="3.25" dx="0.9" dy="0.3" layer="1"/>
<smd name="7" x="-9.75" y="2.75" dx="0.9" dy="0.3" layer="1"/>
<smd name="8" x="-9.75" y="2.25" dx="0.9" dy="0.3" layer="1"/>
<smd name="9" x="-9.75" y="1.75" dx="0.9" dy="0.3" layer="1"/>
<smd name="10" x="-9.75" y="1.25" dx="0.9" dy="0.3" layer="1"/>
<smd name="11" x="-9.75" y="0.75" dx="0.9" dy="0.3" layer="1"/>
<smd name="12" x="-9.75" y="0.25" dx="0.9" dy="0.3" layer="1"/>
<smd name="13" x="-9.75" y="-0.25" dx="0.9" dy="0.3" layer="1"/>
<smd name="14" x="-9.75" y="-0.75" dx="0.9" dy="0.3" layer="1"/>
<smd name="15" x="-9.75" y="-1.25" dx="0.9" dy="0.3" layer="1"/>
<smd name="16" x="-9.75" y="-1.75" dx="0.9" dy="0.3" layer="1"/>
<smd name="17" x="-9.75" y="-2.25" dx="0.9" dy="0.3" layer="1"/>
<smd name="18" x="-9.75" y="-2.75" dx="0.9" dy="0.3" layer="1"/>
<smd name="19" x="-9.75" y="-3.25" dx="0.9" dy="0.3" layer="1"/>
<smd name="20" x="-9.75" y="-3.75" dx="0.9" dy="0.3" layer="1"/>
<smd name="21" x="-9.75" y="-4.25" dx="0.9" dy="0.3" layer="1"/>
<smd name="22" x="-9.75" y="-4.75" dx="0.9" dy="0.3" layer="1"/>
<smd name="23" x="-9.75" y="-5.25" dx="0.9" dy="0.3" layer="1"/>
<smd name="24" x="-9.75" y="-5.75" dx="0.9" dy="0.3" layer="1"/>
<smd name="25" x="9.75" y="-5.75" dx="0.9" dy="0.3" layer="1" rot="R180"/>
<smd name="26" x="9.75" y="-5.25" dx="0.9" dy="0.3" layer="1" rot="R180"/>
<smd name="27" x="9.75" y="-4.75" dx="0.9" dy="0.3" layer="1" rot="R180"/>
<smd name="28" x="9.75" y="-4.25" dx="0.9" dy="0.3" layer="1" rot="R180"/>
<smd name="29" x="9.75" y="-3.75" dx="0.9" dy="0.3" layer="1" rot="R180"/>
<smd name="30" x="9.75" y="-3.25" dx="0.9" dy="0.3" layer="1" rot="R180"/>
<smd name="31" x="9.75" y="-2.75" dx="0.9" dy="0.3" layer="1" rot="R180"/>
<smd name="32" x="9.75" y="-2.25" dx="0.9" dy="0.3" layer="1" rot="R180"/>
<smd name="33" x="9.75" y="-1.75" dx="0.9" dy="0.3" layer="1" rot="R180"/>
<smd name="34" x="9.75" y="-1.25" dx="0.9" dy="0.3" layer="1" rot="R180"/>
<smd name="35" x="9.75" y="-0.75" dx="0.9" dy="0.3" layer="1" rot="R180"/>
<smd name="36" x="9.75" y="-0.25" dx="0.9" dy="0.3" layer="1" rot="R180"/>
<smd name="37" x="9.75" y="0.25" dx="0.9" dy="0.3" layer="1" rot="R180"/>
<smd name="38" x="9.75" y="0.75" dx="0.9" dy="0.3" layer="1" rot="R180"/>
<smd name="39" x="9.75" y="1.25" dx="0.9" dy="0.3" layer="1" rot="R180"/>
<smd name="40" x="9.75" y="1.75" dx="0.9" dy="0.3" layer="1" rot="R180"/>
<smd name="41" x="9.75" y="2.25" dx="0.9" dy="0.3" layer="1" rot="R180"/>
<smd name="42" x="9.75" y="2.75" dx="0.9" dy="0.3" layer="1" rot="R180"/>
<smd name="43" x="9.75" y="3.25" dx="0.9" dy="0.3" layer="1" rot="R180"/>
<smd name="44" x="9.75" y="3.75" dx="0.9" dy="0.3" layer="1" rot="R180"/>
<smd name="45" x="9.75" y="4.25" dx="0.9" dy="0.3" layer="1" rot="R180"/>
<smd name="46" x="9.75" y="4.75" dx="0.9" dy="0.3" layer="1" rot="R180"/>
<smd name="47" x="9.75" y="5.25" dx="0.9" dy="0.3" layer="1" rot="R180"/>
<smd name="48" x="9.75" y="5.75" dx="0.9" dy="0.3" layer="1" rot="R180"/>
<text x="-9" y="6.54" size="1.27" layer="25">&gt;NAME</text>
<text x="-4.5" y="-0.54" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-10" y1="5.625" x2="-9.175" y2="5.875" layer="51"/>
<rectangle x1="-10" y1="5.125" x2="-9.175" y2="5.375" layer="51"/>
<rectangle x1="-10" y1="4.625" x2="-9.175" y2="4.875" layer="51"/>
<rectangle x1="-10" y1="4.125" x2="-9.175" y2="4.375" layer="51"/>
<rectangle x1="-10" y1="3.625" x2="-9.175" y2="3.875" layer="51"/>
<rectangle x1="-10" y1="3.125" x2="-9.175" y2="3.375" layer="51"/>
<rectangle x1="-10" y1="2.625" x2="-9.175" y2="2.875" layer="51"/>
<rectangle x1="-10" y1="2.125" x2="-9.175" y2="2.375" layer="51"/>
<rectangle x1="-10" y1="1.625" x2="-9.175" y2="1.875" layer="51"/>
<rectangle x1="-10" y1="1.125" x2="-9.175" y2="1.375" layer="51"/>
<rectangle x1="-10" y1="0.625" x2="-9.175" y2="0.875" layer="51"/>
<rectangle x1="-10" y1="0.125" x2="-9.175" y2="0.375" layer="51"/>
<rectangle x1="-10" y1="-0.375" x2="-9.175" y2="-0.125" layer="51"/>
<rectangle x1="-10" y1="-0.875" x2="-9.175" y2="-0.625" layer="51"/>
<rectangle x1="-10" y1="-1.375" x2="-9.175" y2="-1.125" layer="51"/>
<rectangle x1="-10" y1="-1.875" x2="-9.175" y2="-1.625" layer="51"/>
<rectangle x1="-10" y1="-2.375" x2="-9.175" y2="-2.125" layer="51"/>
<rectangle x1="-10" y1="-2.875" x2="-9.175" y2="-2.625" layer="51"/>
<rectangle x1="-10" y1="-3.375" x2="-9.175" y2="-3.125" layer="51"/>
<rectangle x1="-10" y1="-3.875" x2="-9.175" y2="-3.625" layer="51"/>
<rectangle x1="-10" y1="-4.375" x2="-9.175" y2="-4.125" layer="51"/>
<rectangle x1="-10" y1="-4.875" x2="-9.175" y2="-4.625" layer="51"/>
<rectangle x1="-10" y1="-5.375" x2="-9.175" y2="-5.125" layer="51"/>
<rectangle x1="-10" y1="-5.875" x2="-9.175" y2="-5.625" layer="51"/>
<rectangle x1="9.175" y1="-5.875" x2="10" y2="-5.625" layer="51" rot="R180"/>
<rectangle x1="9.175" y1="-5.375" x2="10" y2="-5.125" layer="51" rot="R180"/>
<rectangle x1="9.175" y1="-4.875" x2="10" y2="-4.625" layer="51" rot="R180"/>
<rectangle x1="9.175" y1="-4.375" x2="10" y2="-4.125" layer="51" rot="R180"/>
<rectangle x1="9.175" y1="-3.875" x2="10" y2="-3.625" layer="51" rot="R180"/>
<rectangle x1="9.175" y1="-3.375" x2="10" y2="-3.125" layer="51" rot="R180"/>
<rectangle x1="9.175" y1="-2.875" x2="10" y2="-2.625" layer="51" rot="R180"/>
<rectangle x1="9.175" y1="-2.375" x2="10" y2="-2.125" layer="51" rot="R180"/>
<rectangle x1="9.175" y1="-1.875" x2="10" y2="-1.625" layer="51" rot="R180"/>
<rectangle x1="9.175" y1="-1.375" x2="10" y2="-1.125" layer="51" rot="R180"/>
<rectangle x1="9.175" y1="-0.875" x2="10" y2="-0.625" layer="51" rot="R180"/>
<rectangle x1="9.175" y1="-0.375" x2="10" y2="-0.125" layer="51" rot="R180"/>
<rectangle x1="9.175" y1="0.125" x2="10" y2="0.375" layer="51" rot="R180"/>
<rectangle x1="9.175" y1="0.625" x2="10" y2="0.875" layer="51" rot="R180"/>
<rectangle x1="9.175" y1="1.125" x2="10" y2="1.375" layer="51" rot="R180"/>
<rectangle x1="9.175" y1="1.625" x2="10" y2="1.875" layer="51" rot="R180"/>
<rectangle x1="9.175" y1="2.125" x2="10" y2="2.375" layer="51" rot="R180"/>
<rectangle x1="9.175" y1="2.625" x2="10" y2="2.875" layer="51" rot="R180"/>
<rectangle x1="9.175" y1="3.125" x2="10" y2="3.375" layer="51" rot="R180"/>
<rectangle x1="9.175" y1="3.625" x2="10" y2="3.875" layer="51" rot="R180"/>
<rectangle x1="9.175" y1="4.125" x2="10" y2="4.375" layer="51" rot="R180"/>
<rectangle x1="9.175" y1="4.625" x2="10" y2="4.875" layer="51" rot="R180"/>
<rectangle x1="9.175" y1="5.125" x2="10" y2="5.375" layer="51" rot="R180"/>
<rectangle x1="9.175" y1="5.625" x2="10" y2="5.875" layer="51" rot="R180"/>
</package>
</packages>
<symbols>
<symbol name="MT29F8G08DAA">
<wire x1="-12.7" y1="25.4" x2="-12.7" y2="15.24" width="0.254" layer="94"/>
<wire x1="-12.7" y1="15.24" x2="-12.7" y2="5.08" width="0.254" layer="94"/>
<wire x1="-12.7" y1="5.08" x2="-12.7" y2="-22.86" width="0.254" layer="94"/>
<wire x1="-12.7" y1="-22.86" x2="12.7" y2="-22.86" width="0.254" layer="94"/>
<wire x1="12.7" y1="-22.86" x2="12.7" y2="25.4" width="0.254" layer="94"/>
<wire x1="12.7" y1="25.4" x2="-12.7" y2="25.4" width="0.254" layer="94"/>
<wire x1="-12.7" y1="5.08" x2="2.54" y2="5.08" width="0.254" layer="94"/>
<wire x1="2.54" y1="5.08" x2="2.54" y2="15.24" width="0.254" layer="94"/>
<wire x1="-12.7" y1="15.24" x2="2.54" y2="15.24" width="0.254" layer="94"/>
<text x="0" y="6.35" size="1.778" layer="94" rot="R90">8/16GB</text>
<text x="-12.7" y="26.67" size="1.778" layer="95">&gt;NAME</text>
<text x="-12.7" y="-25.4" size="1.778" layer="96">&gt;VALUE</text>
<pin name="R/!B2" x="-15.24" y="12.7" length="short" direction="pas"/>
<pin name="R/!B" x="-15.24" y="2.54" length="short" direction="in"/>
<pin name="!RE" x="-15.24" y="0" length="short" direction="in"/>
<pin name="!CE" x="-15.24" y="-2.54" length="short" direction="in"/>
<pin name="!CE2" x="-15.24" y="7.62" length="short" direction="pas"/>
<pin name="VCC" x="-15.24" y="22.86" length="short" direction="pwr"/>
<pin name="VSS" x="-15.24" y="-20.32" length="short" direction="pwr"/>
<pin name="CLE" x="-15.24" y="-7.62" length="short" direction="in"/>
<pin name="ALE" x="-15.24" y="-10.16" length="short" direction="in"/>
<pin name="!WE" x="15.24" y="-2.54" length="short" direction="in" rot="R180"/>
<pin name="!WP" x="15.24" y="-7.62" length="short" direction="in" rot="R180"/>
<pin name="DNU@1" x="12.7" y="-12.7" visible="off" length="point" direction="nc" rot="R180"/>
<pin name="DNU@2" x="12.7" y="-15.24" visible="off" length="point" direction="nc" rot="R180"/>
<pin name="DNU@3" x="12.7" y="-17.78" visible="off" length="point" direction="nc" rot="R180"/>
<pin name="VCC@1" x="-15.24" y="20.32" length="short" direction="pwr"/>
<pin name="VSS@1" x="-15.24" y="-17.78" length="short" direction="pwr"/>
<pin name="I/O0" x="15.24" y="20.32" length="short" rot="R180"/>
<pin name="I/O1" x="15.24" y="17.78" length="short" rot="R180"/>
<pin name="I/O2" x="15.24" y="15.24" length="short" rot="R180"/>
<pin name="I/O3" x="15.24" y="12.7" length="short" rot="R180"/>
<pin name="I/O4" x="15.24" y="10.16" length="short" rot="R180"/>
<pin name="I/O5" x="15.24" y="7.62" length="short" rot="R180"/>
<pin name="I/O6" x="15.24" y="5.08" length="short" rot="R180"/>
<pin name="I/O7" x="15.24" y="2.54" length="short" rot="R180"/>
<pin name="DNU/VSS" x="15.24" y="-20.32" length="short" direction="pas" rot="R180"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="MT29F8G08DAA" prefix="IC">
<description>&lt;b&gt;4Gb, 8Gb, and 16Gb x8 NAND Flash Memory&lt;/b&gt;&lt;p&gt;
Source: http://download.micron.com/pdf/datasheets/flash/nand/4gb_nand_m40a.pdf</description>
<gates>
<gate name="G$1" symbol="MT29F8G08DAA" x="0" y="0"/>
</gates>
<devices>
<device name="" package="TSOP48">
<connects>
<connect gate="G$1" pin="!CE" pad="9"/>
<connect gate="G$1" pin="!CE2" pad="10"/>
<connect gate="G$1" pin="!RE" pad="8"/>
<connect gate="G$1" pin="!WE" pad="18"/>
<connect gate="G$1" pin="!WP" pad="19"/>
<connect gate="G$1" pin="ALE" pad="17"/>
<connect gate="G$1" pin="CLE" pad="16"/>
<connect gate="G$1" pin="DNU/VSS" pad="38"/>
<connect gate="G$1" pin="DNU@1" pad="25"/>
<connect gate="G$1" pin="DNU@2" pad="26"/>
<connect gate="G$1" pin="DNU@3" pad="48"/>
<connect gate="G$1" pin="I/O0" pad="29"/>
<connect gate="G$1" pin="I/O1" pad="30"/>
<connect gate="G$1" pin="I/O2" pad="31"/>
<connect gate="G$1" pin="I/O3" pad="32"/>
<connect gate="G$1" pin="I/O4" pad="41"/>
<connect gate="G$1" pin="I/O5" pad="42"/>
<connect gate="G$1" pin="I/O6" pad="43"/>
<connect gate="G$1" pin="I/O7" pad="44"/>
<connect gate="G$1" pin="R/!B" pad="7"/>
<connect gate="G$1" pin="R/!B2" pad="6"/>
<connect gate="G$1" pin="VCC" pad="12"/>
<connect gate="G$1" pin="VCC@1" pad="37"/>
<connect gate="G$1" pin="VSS" pad="13"/>
<connect gate="G$1" pin="VSS@1" pad="36"/>
</connects>
<technologies>
<technology name="">
<attribute name="MF" value="" constant="no"/>
<attribute name="MPN" value="" constant="no"/>
<attribute name="OC_FARNELL" value="unknown" constant="no"/>
<attribute name="OC_NEWARK" value="unknown" constant="no"/>
</technology>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="con-lsta">
<description>&lt;b&gt;Female Headers etc.&lt;/b&gt;&lt;p&gt;
Naming:&lt;p&gt;
FE = female&lt;p&gt;
# contacts - # rows&lt;p&gt;
W = angled&lt;p&gt;
&lt;author&gt;Created by librarian@cadsoft.de&lt;/author&gt;</description>
<packages>
<package name="FE10-2">
<description>&lt;b&gt;FEMALE HEADER&lt;/b&gt;</description>
<wire x1="-12.7" y1="-2.413" x2="-12.7" y2="2.413" width="0.1524" layer="21"/>
<wire x1="-12.7" y1="2.413" x2="-12.065" y2="3.048" width="0.1524" layer="21" curve="-90"/>
<wire x1="-12.7" y1="-2.413" x2="-12.065" y2="-3.048" width="0.1524" layer="21" curve="90"/>
<wire x1="-12.065" y1="3.048" x2="12.065" y2="3.048" width="0.1524" layer="21"/>
<wire x1="12.7" y1="-2.413" x2="12.7" y2="2.413" width="0.1524" layer="21"/>
<wire x1="-12.065" y1="-3.048" x2="12.065" y2="-3.048" width="0.1524" layer="21"/>
<wire x1="12.065" y1="3.048" x2="12.7" y2="2.413" width="0.1524" layer="21" curve="-90"/>
<wire x1="12.065" y1="-3.048" x2="12.7" y2="-2.413" width="0.1524" layer="21" curve="90"/>
<circle x="-11.43" y="-1.27" radius="0.127" width="0.4064" layer="51"/>
<circle x="-11.43" y="1.27" radius="0.127" width="0.4064" layer="51"/>
<circle x="-8.89" y="-1.27" radius="0.127" width="0.4064" layer="51"/>
<circle x="-8.89" y="1.27" radius="0.127" width="0.4064" layer="51"/>
<circle x="-6.35" y="-1.27" radius="0.127" width="0.4064" layer="51"/>
<circle x="-6.35" y="1.27" radius="0.127" width="0.4064" layer="51"/>
<circle x="-3.81" y="-1.27" radius="0.127" width="0.4064" layer="51"/>
<circle x="-3.81" y="1.27" radius="0.127" width="0.4064" layer="51"/>
<circle x="-1.27" y="-1.27" radius="0.127" width="0.4064" layer="51"/>
<circle x="-1.27" y="1.27" radius="0.127" width="0.4064" layer="51"/>
<circle x="-11.43" y="-1.27" radius="0.889" width="0.1524" layer="51"/>
<circle x="-11.43" y="1.27" radius="0.889" width="0.1524" layer="51"/>
<circle x="-8.89" y="1.27" radius="0.889" width="0.1524" layer="51"/>
<circle x="-6.35" y="1.27" radius="0.889" width="0.1524" layer="51"/>
<circle x="-3.81" y="1.27" radius="0.889" width="0.1524" layer="51"/>
<circle x="-1.27" y="1.27" radius="0.889" width="0.1524" layer="51"/>
<circle x="-8.89" y="-1.27" radius="0.889" width="0.1524" layer="51"/>
<circle x="-6.35" y="-1.27" radius="0.889" width="0.1524" layer="51"/>
<circle x="-3.81" y="-1.27" radius="0.889" width="0.1524" layer="51"/>
<circle x="-1.27" y="-1.27" radius="0.889" width="0.1524" layer="51"/>
<circle x="1.27" y="-1.27" radius="0.127" width="0.4064" layer="51"/>
<circle x="1.27" y="1.27" radius="0.127" width="0.4064" layer="51"/>
<circle x="3.81" y="-1.27" radius="0.127" width="0.4064" layer="51"/>
<circle x="3.81" y="1.27" radius="0.127" width="0.4064" layer="51"/>
<circle x="6.35" y="-1.27" radius="0.127" width="0.4064" layer="51"/>
<circle x="6.35" y="1.27" radius="0.127" width="0.4064" layer="51"/>
<circle x="8.89" y="-1.27" radius="0.127" width="0.4064" layer="51"/>
<circle x="8.89" y="1.27" radius="0.127" width="0.4064" layer="51"/>
<circle x="11.43" y="-1.27" radius="0.127" width="0.4064" layer="51"/>
<circle x="11.43" y="1.27" radius="0.127" width="0.4064" layer="51"/>
<circle x="1.27" y="-1.27" radius="0.889" width="0.1524" layer="51"/>
<circle x="1.27" y="1.27" radius="0.889" width="0.1524" layer="51"/>
<circle x="3.81" y="1.27" radius="0.889" width="0.1524" layer="51"/>
<circle x="6.35" y="1.27" radius="0.889" width="0.1524" layer="51"/>
<circle x="8.89" y="1.27" radius="0.889" width="0.1524" layer="51"/>
<circle x="11.43" y="1.27" radius="0.889" width="0.1524" layer="51"/>
<circle x="3.81" y="-1.27" radius="0.889" width="0.1524" layer="51"/>
<circle x="6.35" y="-1.27" radius="0.889" width="0.1524" layer="51"/>
<circle x="8.89" y="-1.27" radius="0.889" width="0.1524" layer="51"/>
<circle x="11.43" y="-1.27" radius="0.889" width="0.1524" layer="51"/>
<pad name="1" x="-11.43" y="1.27" drill="0.9144" shape="octagon"/>
<pad name="2" x="-11.43" y="-1.27" drill="0.9144" shape="octagon"/>
<pad name="3" x="-8.89" y="1.27" drill="0.9144" shape="octagon"/>
<pad name="4" x="-8.89" y="-1.27" drill="0.9144" shape="octagon"/>
<pad name="5" x="-6.35" y="1.27" drill="0.9144" shape="octagon"/>
<pad name="6" x="-6.35" y="-1.27" drill="0.9144" shape="octagon"/>
<pad name="7" x="-3.81" y="1.27" drill="0.9144" shape="octagon"/>
<pad name="8" x="-3.81" y="-1.27" drill="0.9144" shape="octagon"/>
<pad name="9" x="-1.27" y="1.27" drill="0.9144" shape="octagon"/>
<pad name="10" x="-1.27" y="-1.27" drill="0.9144" shape="octagon"/>
<pad name="11" x="1.27" y="1.27" drill="0.9144" shape="octagon"/>
<pad name="12" x="1.27" y="-1.27" drill="0.9144" shape="octagon"/>
<pad name="13" x="3.81" y="1.27" drill="0.9144" shape="octagon"/>
<pad name="14" x="3.81" y="-1.27" drill="0.9144" shape="octagon"/>
<pad name="15" x="6.35" y="1.27" drill="0.9144" shape="octagon"/>
<pad name="16" x="6.35" y="-1.27" drill="0.9144" shape="octagon"/>
<pad name="17" x="8.89" y="1.27" drill="0.9144" shape="octagon"/>
<pad name="18" x="8.89" y="-1.27" drill="0.9144" shape="octagon"/>
<pad name="19" x="11.43" y="1.27" drill="0.9144" shape="octagon"/>
<pad name="20" x="11.43" y="-1.27" drill="0.9144" shape="octagon"/>
<text x="-7.62" y="3.429" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-11.811" y="3.429" size="1.27" layer="21" ratio="10">1</text>
<text x="10.541" y="-4.699" size="1.27" layer="21" ratio="10">20</text>
<text x="-12.7" y="-4.699" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="FE05-1">
<description>&lt;b&gt;FEMALE HEADER&lt;/b&gt;</description>
<wire x1="-6.35" y1="1.27" x2="-6.35" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="-6.35" y1="-1.27" x2="-4.064" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="-4.064" y1="-1.27" x2="-3.81" y2="-1.016" width="0.1524" layer="21"/>
<wire x1="-3.81" y1="-1.016" x2="-3.556" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="-3.556" y1="-1.27" x2="-1.524" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="-1.524" y1="-1.27" x2="-1.27" y2="-1.016" width="0.1524" layer="21"/>
<wire x1="-1.27" y1="-1.016" x2="-1.016" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="-1.016" y1="-1.27" x2="1.016" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="1.016" y1="-1.27" x2="1.27" y2="-1.016" width="0.1524" layer="21"/>
<wire x1="1.27" y1="-1.016" x2="1.524" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="1.524" y1="-1.27" x2="3.556" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="3.556" y1="-1.27" x2="3.81" y2="-1.016" width="0.1524" layer="21"/>
<wire x1="3.81" y1="-1.016" x2="4.064" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="4.064" y1="-1.27" x2="6.35" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="6.35" y1="-1.27" x2="6.35" y2="1.27" width="0.1524" layer="21"/>
<wire x1="6.35" y1="1.27" x2="4.064" y2="1.27" width="0.1524" layer="21"/>
<wire x1="4.064" y1="1.27" x2="3.81" y2="1.016" width="0.1524" layer="21"/>
<wire x1="3.81" y1="1.016" x2="3.556" y2="1.27" width="0.1524" layer="21"/>
<wire x1="3.556" y1="1.27" x2="1.524" y2="1.27" width="0.1524" layer="21"/>
<wire x1="1.524" y1="1.27" x2="1.27" y2="1.016" width="0.1524" layer="21"/>
<wire x1="1.27" y1="1.016" x2="1.016" y2="1.27" width="0.1524" layer="21"/>
<wire x1="1.016" y1="1.27" x2="-1.016" y2="1.27" width="0.1524" layer="21"/>
<wire x1="-1.016" y1="1.27" x2="-1.27" y2="1.016" width="0.1524" layer="21"/>
<wire x1="-1.27" y1="1.016" x2="-1.524" y2="1.27" width="0.1524" layer="21"/>
<wire x1="-1.524" y1="1.27" x2="-3.556" y2="1.27" width="0.1524" layer="21"/>
<wire x1="-3.556" y1="1.27" x2="-3.81" y2="1.016" width="0.1524" layer="21"/>
<wire x1="-3.81" y1="1.016" x2="-4.064" y2="1.27" width="0.1524" layer="21"/>
<wire x1="-4.064" y1="1.27" x2="-6.35" y2="1.27" width="0.1524" layer="21"/>
<wire x1="-5.334" y1="0.762" x2="-5.334" y2="0.508" width="0.1524" layer="51"/>
<wire x1="-5.334" y1="0.508" x2="-5.588" y2="0.508" width="0.1524" layer="51"/>
<wire x1="-5.588" y1="0.508" x2="-5.588" y2="-0.508" width="0.1524" layer="51"/>
<wire x1="-5.588" y1="-0.508" x2="-5.334" y2="-0.508" width="0.1524" layer="51"/>
<wire x1="-5.334" y1="-0.508" x2="-5.334" y2="-0.762" width="0.1524" layer="51"/>
<wire x1="-5.334" y1="-0.762" x2="-4.826" y2="-0.762" width="0.1524" layer="51"/>
<wire x1="-4.826" y1="-0.762" x2="-4.826" y2="-0.508" width="0.1524" layer="51"/>
<wire x1="-4.826" y1="-0.508" x2="-4.572" y2="-0.508" width="0.1524" layer="51"/>
<wire x1="-4.572" y1="-0.508" x2="-4.572" y2="0.508" width="0.1524" layer="51"/>
<wire x1="-4.572" y1="0.508" x2="-4.826" y2="0.508" width="0.1524" layer="51"/>
<wire x1="-4.826" y1="0.508" x2="-4.826" y2="0.762" width="0.1524" layer="51"/>
<wire x1="-4.826" y1="0.762" x2="-5.334" y2="0.762" width="0.1524" layer="51"/>
<wire x1="-2.794" y1="0.762" x2="-2.794" y2="0.508" width="0.1524" layer="51"/>
<wire x1="-2.794" y1="0.508" x2="-3.048" y2="0.508" width="0.1524" layer="51"/>
<wire x1="-3.048" y1="0.508" x2="-3.048" y2="-0.508" width="0.1524" layer="51"/>
<wire x1="-3.048" y1="-0.508" x2="-2.794" y2="-0.508" width="0.1524" layer="51"/>
<wire x1="-2.794" y1="-0.508" x2="-2.794" y2="-0.762" width="0.1524" layer="51"/>
<wire x1="-2.794" y1="-0.762" x2="-2.286" y2="-0.762" width="0.1524" layer="51"/>
<wire x1="-2.286" y1="-0.762" x2="-2.286" y2="-0.508" width="0.1524" layer="51"/>
<wire x1="-2.286" y1="-0.508" x2="-2.032" y2="-0.508" width="0.1524" layer="51"/>
<wire x1="-2.032" y1="-0.508" x2="-2.032" y2="0.508" width="0.1524" layer="51"/>
<wire x1="-2.032" y1="0.508" x2="-2.286" y2="0.508" width="0.1524" layer="51"/>
<wire x1="-2.286" y1="0.508" x2="-2.286" y2="0.762" width="0.1524" layer="51"/>
<wire x1="-2.286" y1="0.762" x2="-2.794" y2="0.762" width="0.1524" layer="51"/>
<wire x1="-0.254" y1="0.762" x2="-0.254" y2="0.508" width="0.1524" layer="51"/>
<wire x1="-0.254" y1="0.508" x2="-0.508" y2="0.508" width="0.1524" layer="51"/>
<wire x1="-0.508" y1="0.508" x2="-0.508" y2="-0.508" width="0.1524" layer="51"/>
<wire x1="-0.508" y1="-0.508" x2="-0.254" y2="-0.508" width="0.1524" layer="51"/>
<wire x1="-0.254" y1="-0.508" x2="-0.254" y2="-0.762" width="0.1524" layer="51"/>
<wire x1="-0.254" y1="-0.762" x2="0.254" y2="-0.762" width="0.1524" layer="51"/>
<wire x1="0.254" y1="-0.762" x2="0.254" y2="-0.508" width="0.1524" layer="51"/>
<wire x1="0.254" y1="-0.508" x2="0.508" y2="-0.508" width="0.1524" layer="51"/>
<wire x1="0.508" y1="-0.508" x2="0.508" y2="0.508" width="0.1524" layer="51"/>
<wire x1="0.508" y1="0.508" x2="0.254" y2="0.508" width="0.1524" layer="51"/>
<wire x1="0.254" y1="0.508" x2="0.254" y2="0.762" width="0.1524" layer="51"/>
<wire x1="0.254" y1="0.762" x2="-0.254" y2="0.762" width="0.1524" layer="51"/>
<wire x1="2.286" y1="0.762" x2="2.286" y2="0.508" width="0.1524" layer="51"/>
<wire x1="2.286" y1="0.508" x2="2.032" y2="0.508" width="0.1524" layer="51"/>
<wire x1="2.032" y1="0.508" x2="2.032" y2="-0.508" width="0.1524" layer="51"/>
<wire x1="2.032" y1="-0.508" x2="2.286" y2="-0.508" width="0.1524" layer="51"/>
<wire x1="2.286" y1="-0.508" x2="2.286" y2="-0.762" width="0.1524" layer="51"/>
<wire x1="2.286" y1="-0.762" x2="2.794" y2="-0.762" width="0.1524" layer="51"/>
<wire x1="2.794" y1="-0.762" x2="2.794" y2="-0.508" width="0.1524" layer="51"/>
<wire x1="2.794" y1="-0.508" x2="3.048" y2="-0.508" width="0.1524" layer="51"/>
<wire x1="3.048" y1="-0.508" x2="3.048" y2="0.508" width="0.1524" layer="51"/>
<wire x1="3.048" y1="0.508" x2="2.794" y2="0.508" width="0.1524" layer="51"/>
<wire x1="2.794" y1="0.508" x2="2.794" y2="0.762" width="0.1524" layer="51"/>
<wire x1="2.794" y1="0.762" x2="2.286" y2="0.762" width="0.1524" layer="51"/>
<wire x1="4.826" y1="0.762" x2="4.826" y2="0.508" width="0.1524" layer="51"/>
<wire x1="4.826" y1="0.508" x2="4.572" y2="0.508" width="0.1524" layer="51"/>
<wire x1="4.572" y1="0.508" x2="4.572" y2="-0.508" width="0.1524" layer="51"/>
<wire x1="4.572" y1="-0.508" x2="4.826" y2="-0.508" width="0.1524" layer="51"/>
<wire x1="4.826" y1="-0.508" x2="4.826" y2="-0.762" width="0.1524" layer="51"/>
<wire x1="4.826" y1="-0.762" x2="5.334" y2="-0.762" width="0.1524" layer="51"/>
<wire x1="5.334" y1="-0.762" x2="5.334" y2="-0.508" width="0.1524" layer="51"/>
<wire x1="5.334" y1="-0.508" x2="5.588" y2="-0.508" width="0.1524" layer="51"/>
<wire x1="5.588" y1="-0.508" x2="5.588" y2="0.508" width="0.1524" layer="51"/>
<wire x1="5.588" y1="0.508" x2="5.334" y2="0.508" width="0.1524" layer="51"/>
<wire x1="5.334" y1="0.508" x2="5.334" y2="0.762" width="0.1524" layer="51"/>
<wire x1="5.334" y1="0.762" x2="4.826" y2="0.762" width="0.1524" layer="51"/>
<pad name="1" x="-5.08" y="0" drill="0.8128" shape="long" rot="R90"/>
<pad name="2" x="-2.54" y="0" drill="0.8128" shape="long" rot="R90"/>
<pad name="3" x="0" y="0" drill="0.8128" shape="long" rot="R90"/>
<pad name="4" x="2.54" y="0" drill="0.8128" shape="long" rot="R90"/>
<pad name="5" x="5.08" y="0" drill="0.8128" shape="long" rot="R90"/>
<text x="-6.35" y="1.651" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-7.493" y="-0.635" size="1.27" layer="21" ratio="10">1</text>
<text x="6.604" y="-0.635" size="1.27" layer="21" ratio="10">5</text>
<text x="0" y="1.651" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="-5.207" y1="0.254" x2="-4.953" y2="0.762" layer="51"/>
<rectangle x1="-5.207" y1="-0.762" x2="-4.953" y2="-0.254" layer="51"/>
<rectangle x1="-2.667" y1="0.254" x2="-2.413" y2="0.762" layer="51"/>
<rectangle x1="-2.667" y1="-0.762" x2="-2.413" y2="-0.254" layer="51"/>
<rectangle x1="-0.127" y1="0.254" x2="0.127" y2="0.762" layer="51"/>
<rectangle x1="-0.127" y1="-0.762" x2="0.127" y2="-0.254" layer="51"/>
<rectangle x1="2.413" y1="0.254" x2="2.667" y2="0.762" layer="51"/>
<rectangle x1="2.413" y1="-0.762" x2="2.667" y2="-0.254" layer="51"/>
<rectangle x1="4.953" y1="0.254" x2="5.207" y2="0.762" layer="51"/>
<rectangle x1="4.953" y1="-0.762" x2="5.207" y2="-0.254" layer="51"/>
</package>
<package name="FE06-2">
<description>&lt;b&gt;FEMALE HEADER&lt;/b&gt;</description>
<wire x1="-6.985" y1="3.048" x2="6.985" y2="3.048" width="0.1524" layer="21"/>
<wire x1="7.62" y1="-2.413" x2="7.62" y2="2.413" width="0.1524" layer="21"/>
<wire x1="-7.62" y1="-2.413" x2="-7.62" y2="2.413" width="0.1524" layer="21"/>
<wire x1="-6.985" y1="-3.048" x2="6.985" y2="-3.048" width="0.1524" layer="21"/>
<wire x1="-7.62" y1="2.413" x2="-6.985" y2="3.048" width="0.1524" layer="21" curve="-90"/>
<wire x1="6.985" y1="3.048" x2="7.62" y2="2.413" width="0.1524" layer="21" curve="-90"/>
<wire x1="6.985" y1="-3.048" x2="7.62" y2="-2.413" width="0.1524" layer="21" curve="90"/>
<wire x1="-7.62" y1="-2.413" x2="-6.985" y2="-3.048" width="0.1524" layer="21" curve="90"/>
<circle x="-6.35" y="-1.27" radius="0.127" width="0.4064" layer="51"/>
<circle x="-6.35" y="1.27" radius="0.127" width="0.4064" layer="51"/>
<circle x="-3.81" y="-1.27" radius="0.127" width="0.4064" layer="51"/>
<circle x="-3.81" y="1.27" radius="0.127" width="0.4064" layer="51"/>
<circle x="-1.27" y="-1.27" radius="0.127" width="0.4064" layer="51"/>
<circle x="-1.27" y="1.27" radius="0.127" width="0.4064" layer="51"/>
<circle x="1.27" y="-1.27" radius="0.127" width="0.4064" layer="51"/>
<circle x="1.27" y="1.27" radius="0.127" width="0.4064" layer="51"/>
<circle x="3.81" y="-1.27" radius="0.127" width="0.4064" layer="51"/>
<circle x="3.81" y="1.27" radius="0.127" width="0.4064" layer="51"/>
<circle x="-6.35" y="-1.27" radius="0.889" width="0.1524" layer="51"/>
<circle x="-6.35" y="1.27" radius="0.889" width="0.1524" layer="51"/>
<circle x="-3.81" y="1.27" radius="0.889" width="0.1524" layer="51"/>
<circle x="-1.27" y="1.27" radius="0.889" width="0.1524" layer="51"/>
<circle x="1.27" y="1.27" radius="0.889" width="0.1524" layer="51"/>
<circle x="3.81" y="1.27" radius="0.889" width="0.1524" layer="51"/>
<circle x="-3.81" y="-1.27" radius="0.889" width="0.1524" layer="51"/>
<circle x="-1.27" y="-1.27" radius="0.889" width="0.1524" layer="51"/>
<circle x="1.27" y="-1.27" radius="0.889" width="0.1524" layer="51"/>
<circle x="3.81" y="-1.27" radius="0.889" width="0.1524" layer="51"/>
<circle x="6.35" y="-1.27" radius="0.127" width="0.4064" layer="51"/>
<circle x="6.35" y="1.27" radius="0.127" width="0.4064" layer="51"/>
<circle x="6.35" y="-1.27" radius="0.889" width="0.1524" layer="51"/>
<circle x="6.35" y="1.27" radius="0.889" width="0.1524" layer="51"/>
<pad name="1" x="-6.35" y="1.27" drill="0.9144"/>
<pad name="2" x="-6.35" y="-1.27" drill="0.9144" shape="octagon"/>
<pad name="3" x="-3.81" y="1.27" drill="0.9144" shape="octagon"/>
<pad name="4" x="-3.81" y="-1.27" drill="0.9144" shape="octagon"/>
<pad name="5" x="-1.27" y="1.27" drill="0.9144" shape="octagon"/>
<pad name="6" x="-1.27" y="-1.27" drill="0.9144" shape="octagon"/>
<pad name="7" x="1.27" y="1.27" drill="0.9144" shape="octagon"/>
<pad name="8" x="1.27" y="-1.27" drill="0.9144" shape="octagon"/>
<pad name="9" x="3.81" y="1.27" drill="0.9144" shape="octagon"/>
<pad name="10" x="3.81" y="-1.27" drill="0.9144" shape="octagon"/>
<pad name="11" x="6.35" y="1.27" drill="0.9144" shape="octagon"/>
<pad name="12" x="6.35" y="-1.27" drill="0.9144" shape="octagon"/>
<text x="-2.54" y="3.429" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-6.858" y="3.429" size="1.27" layer="21" ratio="10">1</text>
<text x="5.334" y="-4.699" size="1.27" layer="21" ratio="10">12</text>
<text x="-7.62" y="-4.699" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="FE05-2">
<description>&lt;b&gt;FEMALE HEADER&lt;/b&gt;</description>
<wire x1="-5.715" y1="3.048" x2="5.715" y2="3.048" width="0.1524" layer="21"/>
<wire x1="6.35" y1="-2.413" x2="6.35" y2="2.413" width="0.1524" layer="21"/>
<wire x1="-6.35" y1="-2.413" x2="-6.35" y2="2.413" width="0.1524" layer="21"/>
<wire x1="-5.715" y1="-3.048" x2="5.715" y2="-3.048" width="0.1524" layer="21"/>
<wire x1="-6.35" y1="2.413" x2="-5.715" y2="3.048" width="0.1524" layer="21" curve="-90"/>
<wire x1="5.715" y1="3.048" x2="6.35" y2="2.413" width="0.1524" layer="21" curve="-90"/>
<wire x1="5.715" y1="-3.048" x2="6.35" y2="-2.413" width="0.1524" layer="21" curve="90"/>
<wire x1="-6.35" y1="-2.413" x2="-5.715" y2="-3.048" width="0.1524" layer="21" curve="90"/>
<circle x="-5.08" y="-1.27" radius="0.127" width="0.4064" layer="51"/>
<circle x="-5.08" y="1.27" radius="0.127" width="0.4064" layer="51"/>
<circle x="-2.54" y="-1.27" radius="0.127" width="0.4064" layer="51"/>
<circle x="-2.54" y="1.27" radius="0.127" width="0.4064" layer="51"/>
<circle x="0" y="-1.27" radius="0.127" width="0.4064" layer="51"/>
<circle x="0" y="1.27" radius="0.127" width="0.4064" layer="51"/>
<circle x="2.54" y="-1.27" radius="0.127" width="0.4064" layer="51"/>
<circle x="2.54" y="1.27" radius="0.127" width="0.4064" layer="51"/>
<circle x="5.08" y="-1.27" radius="0.127" width="0.4064" layer="51"/>
<circle x="5.08" y="1.27" radius="0.127" width="0.4064" layer="51"/>
<circle x="-5.08" y="-1.27" radius="0.889" width="0.1524" layer="51"/>
<circle x="-5.08" y="1.27" radius="0.889" width="0.1524" layer="51"/>
<circle x="-2.54" y="1.27" radius="0.889" width="0.1524" layer="51"/>
<circle x="0" y="1.27" radius="0.889" width="0.1524" layer="51"/>
<circle x="2.54" y="1.27" radius="0.889" width="0.1524" layer="51"/>
<circle x="5.08" y="1.27" radius="0.889" width="0.1524" layer="51"/>
<circle x="-2.54" y="-1.27" radius="0.889" width="0.1524" layer="51"/>
<circle x="0" y="-1.27" radius="0.889" width="0.1524" layer="51"/>
<circle x="2.54" y="-1.27" radius="0.889" width="0.1524" layer="51"/>
<circle x="5.08" y="-1.27" radius="0.889" width="0.1524" layer="51"/>
<pad name="1" x="-5.08" y="1.27" drill="0.9144"/>
<pad name="2" x="-5.08" y="-1.27" drill="0.9144" shape="octagon"/>
<pad name="3" x="-2.54" y="1.27" drill="0.9144" shape="octagon"/>
<pad name="4" x="-2.54" y="-1.27" drill="0.9144" shape="octagon"/>
<pad name="5" x="0" y="1.27" drill="0.9144" shape="octagon"/>
<pad name="6" x="0" y="-1.27" drill="0.9144" shape="octagon"/>
<pad name="7" x="2.54" y="1.27" drill="0.9144" shape="octagon"/>
<pad name="8" x="2.54" y="-1.27" drill="0.9144" shape="octagon"/>
<pad name="9" x="5.08" y="1.27" drill="0.9144" shape="octagon"/>
<pad name="10" x="5.08" y="-1.27" drill="0.9144" shape="octagon"/>
<text x="-2.54" y="3.429" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-5.461" y="3.429" size="1.27" layer="21" ratio="10">1</text>
<text x="-6.35" y="-4.699" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
<text x="4.191" y="-4.699" size="1.27" layer="21" ratio="10">10</text>
</package>
</packages>
<symbols>
<symbol name="FE10-2">
<wire x1="3.81" y1="-12.7" x2="-3.81" y2="-12.7" width="0.4064" layer="94"/>
<wire x1="-1.905" y1="-5.715" x2="-1.905" y2="-4.445" width="0.254" layer="94" curve="-180" cap="flat"/>
<wire x1="-1.905" y1="-8.255" x2="-1.905" y2="-6.985" width="0.254" layer="94" curve="-180" cap="flat"/>
<wire x1="-1.905" y1="-10.795" x2="-1.905" y2="-9.525" width="0.254" layer="94" curve="-180" cap="flat"/>
<wire x1="1.905" y1="-4.445" x2="1.905" y2="-5.715" width="0.254" layer="94" curve="-180" cap="flat"/>
<wire x1="1.905" y1="-6.985" x2="1.905" y2="-8.255" width="0.254" layer="94" curve="-180" cap="flat"/>
<wire x1="1.905" y1="-9.525" x2="1.905" y2="-10.795" width="0.254" layer="94" curve="-180" cap="flat"/>
<wire x1="-1.905" y1="-0.635" x2="-1.905" y2="0.635" width="0.254" layer="94" curve="-180" cap="flat"/>
<wire x1="-1.905" y1="-3.175" x2="-1.905" y2="-1.905" width="0.254" layer="94" curve="-180" cap="flat"/>
<wire x1="1.905" y1="0.635" x2="1.905" y2="-0.635" width="0.254" layer="94" curve="-180" cap="flat"/>
<wire x1="1.905" y1="-1.905" x2="1.905" y2="-3.175" width="0.254" layer="94" curve="-180" cap="flat"/>
<wire x1="-1.905" y1="6.985" x2="-1.905" y2="8.255" width="0.254" layer="94" curve="-180" cap="flat"/>
<wire x1="-1.905" y1="4.445" x2="-1.905" y2="5.715" width="0.254" layer="94" curve="-180" cap="flat"/>
<wire x1="-1.905" y1="1.905" x2="-1.905" y2="3.175" width="0.254" layer="94" curve="-180" cap="flat"/>
<wire x1="1.905" y1="8.255" x2="1.905" y2="6.985" width="0.254" layer="94" curve="-180" cap="flat"/>
<wire x1="1.905" y1="5.715" x2="1.905" y2="4.445" width="0.254" layer="94" curve="-180" cap="flat"/>
<wire x1="1.905" y1="3.175" x2="1.905" y2="1.905" width="0.254" layer="94" curve="-180" cap="flat"/>
<wire x1="-3.81" y1="15.24" x2="-3.81" y2="-12.7" width="0.4064" layer="94"/>
<wire x1="3.81" y1="-12.7" x2="3.81" y2="15.24" width="0.4064" layer="94"/>
<wire x1="-3.81" y1="15.24" x2="3.81" y2="15.24" width="0.4064" layer="94"/>
<wire x1="-1.905" y1="12.065" x2="-1.905" y2="13.335" width="0.254" layer="94" curve="-180" cap="flat"/>
<wire x1="-1.905" y1="9.525" x2="-1.905" y2="10.795" width="0.254" layer="94" curve="-180" cap="flat"/>
<wire x1="1.905" y1="13.335" x2="1.905" y2="12.065" width="0.254" layer="94" curve="-180" cap="flat"/>
<wire x1="1.905" y1="10.795" x2="1.905" y2="9.525" width="0.254" layer="94" curve="-180" cap="flat"/>
<text x="-3.81" y="-15.24" size="1.778" layer="96">&gt;VALUE</text>
<text x="-3.81" y="16.002" size="1.778" layer="95">&gt;NAME</text>
<pin name="1" x="-7.62" y="-10.16" visible="pad" length="middle" direction="pas" swaplevel="1"/>
<pin name="3" x="-7.62" y="-7.62" visible="pad" length="middle" direction="pas" swaplevel="1"/>
<pin name="5" x="-7.62" y="-5.08" visible="pad" length="middle" direction="pas" swaplevel="1"/>
<pin name="2" x="7.62" y="-10.16" visible="pad" length="middle" direction="pas" swaplevel="1" rot="R180"/>
<pin name="4" x="7.62" y="-7.62" visible="pad" length="middle" direction="pas" swaplevel="1" rot="R180"/>
<pin name="6" x="7.62" y="-5.08" visible="pad" length="middle" direction="pas" swaplevel="1" rot="R180"/>
<pin name="7" x="-7.62" y="-2.54" visible="pad" length="middle" direction="pas" swaplevel="1"/>
<pin name="9" x="-7.62" y="0" visible="pad" length="middle" direction="pas" swaplevel="1"/>
<pin name="8" x="7.62" y="-2.54" visible="pad" length="middle" direction="pas" swaplevel="1" rot="R180"/>
<pin name="10" x="7.62" y="0" visible="pad" length="middle" direction="pas" swaplevel="1" rot="R180"/>
<pin name="11" x="-7.62" y="2.54" visible="pad" length="middle" direction="pas" swaplevel="1"/>
<pin name="13" x="-7.62" y="5.08" visible="pad" length="middle" direction="pas" swaplevel="1"/>
<pin name="15" x="-7.62" y="7.62" visible="pad" length="middle" direction="pas" swaplevel="1"/>
<pin name="12" x="7.62" y="2.54" visible="pad" length="middle" direction="pas" swaplevel="1" rot="R180"/>
<pin name="14" x="7.62" y="5.08" visible="pad" length="middle" direction="pas" swaplevel="1" rot="R180"/>
<pin name="16" x="7.62" y="7.62" visible="pad" length="middle" direction="pas" swaplevel="1" rot="R180"/>
<pin name="17" x="-7.62" y="10.16" visible="pad" length="middle" direction="pas" swaplevel="1"/>
<pin name="19" x="-7.62" y="12.7" visible="pad" length="middle" direction="pas" swaplevel="1"/>
<pin name="18" x="7.62" y="10.16" visible="pad" length="middle" direction="pas" swaplevel="1" rot="R180"/>
<pin name="20" x="7.62" y="12.7" visible="pad" length="middle" direction="pas" swaplevel="1" rot="R180"/>
</symbol>
<symbol name="FE05-1">
<wire x1="3.81" y1="-7.62" x2="-1.27" y2="-7.62" width="0.4064" layer="94"/>
<wire x1="1.905" y1="0.635" x2="1.905" y2="-0.635" width="0.254" layer="94" curve="-180" cap="flat"/>
<wire x1="1.905" y1="-1.905" x2="1.905" y2="-3.175" width="0.254" layer="94" curve="-180" cap="flat"/>
<wire x1="1.905" y1="-4.445" x2="1.905" y2="-5.715" width="0.254" layer="94" curve="-180" cap="flat"/>
<wire x1="-1.27" y1="7.62" x2="-1.27" y2="-7.62" width="0.4064" layer="94"/>
<wire x1="3.81" y1="-7.62" x2="3.81" y2="7.62" width="0.4064" layer="94"/>
<wire x1="-1.27" y1="7.62" x2="3.81" y2="7.62" width="0.4064" layer="94"/>
<wire x1="1.905" y1="5.715" x2="1.905" y2="4.445" width="0.254" layer="94" curve="-180" cap="flat"/>
<wire x1="1.905" y1="3.175" x2="1.905" y2="1.905" width="0.254" layer="94" curve="-180" cap="flat"/>
<text x="-1.27" y="-10.16" size="1.778" layer="96">&gt;VALUE</text>
<text x="-1.27" y="8.382" size="1.778" layer="95">&gt;NAME</text>
<pin name="1" x="7.62" y="-5.08" visible="pad" length="middle" direction="pas" swaplevel="1" rot="R180"/>
<pin name="2" x="7.62" y="-2.54" visible="pad" length="middle" direction="pas" swaplevel="1" rot="R180"/>
<pin name="3" x="7.62" y="0" visible="pad" length="middle" direction="pas" swaplevel="1" rot="R180"/>
<pin name="4" x="7.62" y="2.54" visible="pad" length="middle" direction="pas" swaplevel="1" rot="R180"/>
<pin name="5" x="7.62" y="5.08" visible="pad" length="middle" direction="pas" swaplevel="1" rot="R180"/>
</symbol>
<symbol name="FE06-2">
<wire x1="3.81" y1="-7.62" x2="-3.81" y2="-7.62" width="0.4064" layer="94"/>
<wire x1="-1.905" y1="-0.635" x2="-1.905" y2="0.635" width="0.254" layer="94" curve="-180" cap="flat"/>
<wire x1="-1.905" y1="-3.175" x2="-1.905" y2="-1.905" width="0.254" layer="94" curve="-180" cap="flat"/>
<wire x1="-1.905" y1="-5.715" x2="-1.905" y2="-4.445" width="0.254" layer="94" curve="-180" cap="flat"/>
<wire x1="1.905" y1="0.635" x2="1.905" y2="-0.635" width="0.254" layer="94" curve="-180" cap="flat"/>
<wire x1="1.905" y1="-1.905" x2="1.905" y2="-3.175" width="0.254" layer="94" curve="-180" cap="flat"/>
<wire x1="1.905" y1="-4.445" x2="1.905" y2="-5.715" width="0.254" layer="94" curve="-180" cap="flat"/>
<wire x1="-1.905" y1="4.445" x2="-1.905" y2="5.715" width="0.254" layer="94" curve="-180" cap="flat"/>
<wire x1="-1.905" y1="1.905" x2="-1.905" y2="3.175" width="0.254" layer="94" curve="-180" cap="flat"/>
<wire x1="1.905" y1="5.715" x2="1.905" y2="4.445" width="0.254" layer="94" curve="-180" cap="flat"/>
<wire x1="1.905" y1="3.175" x2="1.905" y2="1.905" width="0.254" layer="94" curve="-180" cap="flat"/>
<wire x1="-1.905" y1="6.985" x2="-1.905" y2="8.255" width="0.254" layer="94" curve="-180" cap="flat"/>
<wire x1="1.905" y1="8.255" x2="1.905" y2="6.985" width="0.254" layer="94" curve="-180" cap="flat"/>
<wire x1="-3.81" y1="10.16" x2="-3.81" y2="-7.62" width="0.4064" layer="94"/>
<wire x1="3.81" y1="-7.62" x2="3.81" y2="10.16" width="0.4064" layer="94"/>
<wire x1="-3.81" y1="10.16" x2="3.81" y2="10.16" width="0.4064" layer="94"/>
<text x="-3.81" y="-10.16" size="1.778" layer="96">&gt;VALUE</text>
<text x="-3.81" y="10.922" size="1.778" layer="95">&gt;NAME</text>
<pin name="1" x="-7.62" y="-5.08" visible="pad" length="middle" direction="pas" swaplevel="1"/>
<pin name="3" x="-7.62" y="-2.54" visible="pad" length="middle" direction="pas" swaplevel="1"/>
<pin name="5" x="-7.62" y="0" visible="pad" length="middle" direction="pas" swaplevel="1"/>
<pin name="2" x="7.62" y="-5.08" visible="pad" length="middle" direction="pas" swaplevel="1" rot="R180"/>
<pin name="4" x="7.62" y="-2.54" visible="pad" length="middle" direction="pas" swaplevel="1" rot="R180"/>
<pin name="6" x="7.62" y="0" visible="pad" length="middle" direction="pas" swaplevel="1" rot="R180"/>
<pin name="7" x="-7.62" y="2.54" visible="pad" length="middle" direction="pas" swaplevel="1"/>
<pin name="9" x="-7.62" y="5.08" visible="pad" length="middle" direction="pas" swaplevel="1"/>
<pin name="8" x="7.62" y="2.54" visible="pad" length="middle" direction="pas" swaplevel="1" rot="R180"/>
<pin name="10" x="7.62" y="5.08" visible="pad" length="middle" direction="pas" swaplevel="1" rot="R180"/>
<pin name="11" x="-7.62" y="7.62" visible="pad" length="middle" direction="pas" swaplevel="1"/>
<pin name="12" x="7.62" y="7.62" visible="pad" length="middle" direction="pas" swaplevel="1" rot="R180"/>
</symbol>
<symbol name="FE05-2">
<wire x1="3.81" y1="-7.62" x2="-3.81" y2="-7.62" width="0.4064" layer="94"/>
<wire x1="-1.905" y1="-0.635" x2="-1.905" y2="0.635" width="0.254" layer="94" curve="-180" cap="flat"/>
<wire x1="-1.905" y1="-3.175" x2="-1.905" y2="-1.905" width="0.254" layer="94" curve="-180" cap="flat"/>
<wire x1="-1.905" y1="-5.715" x2="-1.905" y2="-4.445" width="0.254" layer="94" curve="-180" cap="flat"/>
<wire x1="1.905" y1="0.635" x2="1.905" y2="-0.635" width="0.254" layer="94" curve="-180" cap="flat"/>
<wire x1="1.905" y1="-1.905" x2="1.905" y2="-3.175" width="0.254" layer="94" curve="-180" cap="flat"/>
<wire x1="1.905" y1="-4.445" x2="1.905" y2="-5.715" width="0.254" layer="94" curve="-180" cap="flat"/>
<wire x1="-3.81" y1="7.62" x2="-3.81" y2="-7.62" width="0.4064" layer="94"/>
<wire x1="3.81" y1="-7.62" x2="3.81" y2="7.62" width="0.4064" layer="94"/>
<wire x1="-3.81" y1="7.62" x2="3.81" y2="7.62" width="0.4064" layer="94"/>
<wire x1="-1.905" y1="4.445" x2="-1.905" y2="5.715" width="0.254" layer="94" curve="-180" cap="flat"/>
<wire x1="-1.905" y1="1.905" x2="-1.905" y2="3.175" width="0.254" layer="94" curve="-180" cap="flat"/>
<wire x1="1.905" y1="5.715" x2="1.905" y2="4.445" width="0.254" layer="94" curve="-180" cap="flat"/>
<wire x1="1.905" y1="3.175" x2="1.905" y2="1.905" width="0.254" layer="94" curve="-180" cap="flat"/>
<text x="-3.81" y="-10.16" size="1.778" layer="96">&gt;VALUE</text>
<text x="-3.81" y="8.382" size="1.778" layer="95">&gt;NAME</text>
<pin name="1" x="-7.62" y="-5.08" visible="pad" length="middle" direction="pas" swaplevel="1"/>
<pin name="3" x="-7.62" y="-2.54" visible="pad" length="middle" direction="pas" swaplevel="1"/>
<pin name="5" x="-7.62" y="0" visible="pad" length="middle" direction="pas" swaplevel="1"/>
<pin name="2" x="7.62" y="-5.08" visible="pad" length="middle" direction="pas" swaplevel="1" rot="R180"/>
<pin name="4" x="7.62" y="-2.54" visible="pad" length="middle" direction="pas" swaplevel="1" rot="R180"/>
<pin name="6" x="7.62" y="0" visible="pad" length="middle" direction="pas" swaplevel="1" rot="R180"/>
<pin name="7" x="-7.62" y="2.54" visible="pad" length="middle" direction="pas" swaplevel="1"/>
<pin name="9" x="-7.62" y="5.08" visible="pad" length="middle" direction="pas" swaplevel="1"/>
<pin name="8" x="7.62" y="2.54" visible="pad" length="middle" direction="pas" swaplevel="1" rot="R180"/>
<pin name="10" x="7.62" y="5.08" visible="pad" length="middle" direction="pas" swaplevel="1" rot="R180"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="FE10-2" prefix="SV" uservalue="yes">
<description>&lt;b&gt;FEMALE HEADER&lt;/b&gt;</description>
<gates>
<gate name="G$1" symbol="FE10-2" x="0" y="0"/>
</gates>
<devices>
<device name="" package="FE10-2">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="10" pad="10"/>
<connect gate="G$1" pin="11" pad="11"/>
<connect gate="G$1" pin="12" pad="12"/>
<connect gate="G$1" pin="13" pad="13"/>
<connect gate="G$1" pin="14" pad="14"/>
<connect gate="G$1" pin="15" pad="15"/>
<connect gate="G$1" pin="16" pad="16"/>
<connect gate="G$1" pin="17" pad="17"/>
<connect gate="G$1" pin="18" pad="18"/>
<connect gate="G$1" pin="19" pad="19"/>
<connect gate="G$1" pin="2" pad="2"/>
<connect gate="G$1" pin="20" pad="20"/>
<connect gate="G$1" pin="3" pad="3"/>
<connect gate="G$1" pin="4" pad="4"/>
<connect gate="G$1" pin="5" pad="5"/>
<connect gate="G$1" pin="6" pad="6"/>
<connect gate="G$1" pin="7" pad="7"/>
<connect gate="G$1" pin="8" pad="8"/>
<connect gate="G$1" pin="9" pad="9"/>
</connects>
<technologies>
<technology name="">
<attribute name="MF" value="" constant="no"/>
<attribute name="MPN" value="" constant="no"/>
<attribute name="OC_FARNELL" value="unknown" constant="no"/>
<attribute name="OC_NEWARK" value="unknown" constant="no"/>
</technology>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="FE05-1" prefix="SV" uservalue="yes">
<description>&lt;b&gt;FEMALE HEADER&lt;/b&gt;</description>
<gates>
<gate name="G$1" symbol="FE05-1" x="0" y="0"/>
</gates>
<devices>
<device name="" package="FE05-1">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
<connect gate="G$1" pin="3" pad="3"/>
<connect gate="G$1" pin="4" pad="4"/>
<connect gate="G$1" pin="5" pad="5"/>
</connects>
<technologies>
<technology name="">
<attribute name="MF" value="" constant="no"/>
<attribute name="MPN" value="" constant="no"/>
<attribute name="OC_FARNELL" value="unknown" constant="no"/>
<attribute name="OC_NEWARK" value="unknown" constant="no"/>
</technology>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="FE06-2" prefix="SV" uservalue="yes">
<description>&lt;b&gt;FEMALE HEADER&lt;/b&gt;</description>
<gates>
<gate name="G$1" symbol="FE06-2" x="0" y="0"/>
</gates>
<devices>
<device name="" package="FE06-2">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="10" pad="10"/>
<connect gate="G$1" pin="11" pad="11"/>
<connect gate="G$1" pin="12" pad="12"/>
<connect gate="G$1" pin="2" pad="2"/>
<connect gate="G$1" pin="3" pad="3"/>
<connect gate="G$1" pin="4" pad="4"/>
<connect gate="G$1" pin="5" pad="5"/>
<connect gate="G$1" pin="6" pad="6"/>
<connect gate="G$1" pin="7" pad="7"/>
<connect gate="G$1" pin="8" pad="8"/>
<connect gate="G$1" pin="9" pad="9"/>
</connects>
<technologies>
<technology name="">
<attribute name="MF" value="" constant="no"/>
<attribute name="MPN" value="" constant="no"/>
<attribute name="OC_FARNELL" value="unknown" constant="no"/>
<attribute name="OC_NEWARK" value="unknown" constant="no"/>
</technology>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="FE05-2" prefix="SV" uservalue="yes">
<description>&lt;b&gt;FEMALE HEADER&lt;/b&gt;</description>
<gates>
<gate name="G$1" symbol="FE05-2" x="0" y="0"/>
</gates>
<devices>
<device name="" package="FE05-2">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="10" pad="10"/>
<connect gate="G$1" pin="2" pad="2"/>
<connect gate="G$1" pin="3" pad="3"/>
<connect gate="G$1" pin="4" pad="4"/>
<connect gate="G$1" pin="5" pad="5"/>
<connect gate="G$1" pin="6" pad="6"/>
<connect gate="G$1" pin="7" pad="7"/>
<connect gate="G$1" pin="8" pad="8"/>
<connect gate="G$1" pin="9" pad="9"/>
</connects>
<technologies>
<technology name="">
<attribute name="MF" value="" constant="no"/>
<attribute name="MPN" value="" constant="no"/>
<attribute name="OC_FARNELL" value="unknown" constant="no"/>
<attribute name="OC_NEWARK" value="unknown" constant="no"/>
</technology>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="con-cypressindustries">
<description>&lt;b&gt;Connectors from Cypress Industries&lt;/b&gt;&lt;p&gt;
www.cypressindustries.com&lt;br&gt;
&lt;author&gt;Created by librarian@cadsoft.de&lt;/author&gt;</description>
<packages>
<package name="85-32004-40X">
<description>&lt;b&gt;USB B Type Single Solder&lt;/b&gt;&lt;p&gt;
Source: http://www.cypressindustries.com/pdf/85-32004-40x-1.pdf</description>
<wire x1="-5.25" y1="5.3" x2="5.3" y2="5.3" width="0.1016" layer="21"/>
<wire x1="6" y1="4.6" x2="6" y2="-4.65" width="0.1016" layer="21"/>
<wire x1="5.3" y1="-5.35" x2="-5.3" y2="-5.35" width="0.1016" layer="21"/>
<wire x1="-6" y1="-4.65" x2="-6" y2="4.6" width="0.1016" layer="21"/>
<wire x1="-6" y1="4.6" x2="-5.3" y2="5.3" width="0.1016" layer="21" curve="-90"/>
<wire x1="5.3" y1="5.3" x2="6" y2="4.6" width="0.1016" layer="21" curve="-90"/>
<wire x1="6" y1="-4.65" x2="5.3" y2="-5.35" width="0.1016" layer="21" curve="-90"/>
<wire x1="-5.3" y1="-5.35" x2="-6" y2="-4.65" width="0.1016" layer="21" curve="-90"/>
<wire x1="-4.6" y1="1.55" x2="-4.6" y2="2.35" width="0.1016" layer="21"/>
<wire x1="-4.6" y1="2.35" x2="-2.6" y2="4.35" width="0.1016" layer="21"/>
<wire x1="-2.6" y1="4.35" x2="2.6" y2="4.35" width="0.1016" layer="21"/>
<wire x1="2.6" y1="4.35" x2="4.6" y2="2.35" width="0.1016" layer="21"/>
<wire x1="4.6" y1="2.35" x2="4.6" y2="1.55" width="0.1016" layer="21"/>
<wire x1="4.6" y1="1.55" x2="4.45" y2="1.4" width="0.1016" layer="21"/>
<wire x1="4.45" y1="1.4" x2="4.45" y2="-2.5" width="0.1016" layer="21"/>
<wire x1="4.45" y1="-2.5" x2="4.6" y2="-2.65" width="0.1016" layer="21"/>
<wire x1="4.6" y1="-2.65" x2="4.6" y2="-3.6" width="0.1016" layer="21"/>
<wire x1="4.6" y1="-3.6" x2="3.85" y2="-4.35" width="0.1016" layer="21"/>
<wire x1="3.85" y1="-4.35" x2="-3.85" y2="-4.35" width="0.1016" layer="21"/>
<wire x1="-3.85" y1="-4.35" x2="-4.6" y2="-3.6" width="0.1016" layer="21"/>
<wire x1="-4.6" y1="-3.6" x2="-4.6" y2="-2.65" width="0.1016" layer="21"/>
<wire x1="-4.6" y1="-2.65" x2="-4.45" y2="-2.5" width="0.1016" layer="21"/>
<wire x1="-4.45" y1="-2.5" x2="-4.45" y2="1.4" width="0.1016" layer="21"/>
<wire x1="-4.45" y1="1.4" x2="-4.6" y2="1.55" width="0.1016" layer="21"/>
<wire x1="-4.15" y1="0.85" x2="-4.15" y2="2.25" width="0.1016" layer="21"/>
<wire x1="-4.15" y1="2.25" x2="-2.5" y2="3.9" width="0.1016" layer="21"/>
<wire x1="-2.5" y1="3.9" x2="2.5" y2="3.9" width="0.1016" layer="21"/>
<wire x1="2.5" y1="3.9" x2="4.15" y2="2.25" width="0.1016" layer="21"/>
<wire x1="3.85" y1="-2.1" x2="4.15" y2="-2.4" width="0.1016" layer="21"/>
<wire x1="4.15" y1="-2.4" x2="4.15" y2="-3.55" width="0.1016" layer="21"/>
<wire x1="4.15" y1="-3.55" x2="3.8" y2="-3.9" width="0.1016" layer="21"/>
<wire x1="3.8" y1="-3.9" x2="-3.8" y2="-3.9" width="0.1016" layer="21"/>
<wire x1="-3.8" y1="-3.9" x2="-4.15" y2="-3.55" width="0.1016" layer="21"/>
<wire x1="-4.15" y1="-3.55" x2="-4.15" y2="-2.4" width="0.1016" layer="21"/>
<wire x1="-4.15" y1="-2.4" x2="-3.85" y2="-2.1" width="0.1016" layer="21"/>
<wire x1="-3.85" y1="-2.1" x2="-3.85" y2="0.55" width="0.1016" layer="21"/>
<wire x1="-3.85" y1="0.55" x2="-4.15" y2="0.85" width="0.1016" layer="21"/>
<wire x1="3.85" y1="0.55" x2="3.85" y2="-2.1" width="0.1016" layer="21"/>
<wire x1="3.85" y1="0.55" x2="4.15" y2="0.85" width="0.1016" layer="21"/>
<wire x1="4.15" y1="0.85" x2="4.15" y2="2.25" width="0.1016" layer="21"/>
<wire x1="2.3" y1="1.15" x2="2.3" y2="-1.15" width="0.1016" layer="21"/>
<wire x1="2.3" y1="-1.15" x2="-2.3" y2="-1.15" width="0.1016" layer="51"/>
<wire x1="-2.8" y1="1.55" x2="2.8" y2="1.55" width="0.1016" layer="51"/>
<wire x1="2.8" y1="1.55" x2="2.8" y2="-1.55" width="0.1016" layer="21"/>
<wire x1="2.8" y1="-1.55" x2="-2.8" y2="-1.55" width="0.1016" layer="51"/>
<wire x1="-2.8" y1="-1.55" x2="-2.8" y2="1.55" width="0.1016" layer="21"/>
<wire x1="-2.3" y1="-1.15" x2="-2.3" y2="1.15" width="0.1016" layer="21"/>
<wire x1="-2.3" y1="1.15" x2="2.3" y2="1.15" width="0.1016" layer="51"/>
<wire x1="-1.75" y1="1.6" x2="-1.75" y2="2.25" width="0.1016" layer="51"/>
<wire x1="-1.75" y1="2.25" x2="-0.75" y2="2.25" width="0.1016" layer="51"/>
<wire x1="-0.75" y1="2.25" x2="-0.75" y2="1.6" width="0.1016" layer="51"/>
<wire x1="0.75" y1="1.6" x2="0.75" y2="2.25" width="0.1016" layer="51"/>
<wire x1="0.75" y1="2.25" x2="1.75" y2="2.25" width="0.1016" layer="51"/>
<wire x1="1.75" y1="2.25" x2="1.75" y2="1.6" width="0.1016" layer="51"/>
<wire x1="-0.75" y1="-1.6" x2="-0.75" y2="-2.25" width="0.1016" layer="51"/>
<wire x1="-0.75" y1="-2.25" x2="-1.75" y2="-2.25" width="0.1016" layer="51"/>
<wire x1="-1.75" y1="-2.25" x2="-1.75" y2="-1.6" width="0.1016" layer="51"/>
<wire x1="1.75" y1="-1.6" x2="1.75" y2="-2.25" width="0.1016" layer="51"/>
<wire x1="1.75" y1="-2.25" x2="0.75" y2="-2.25" width="0.1016" layer="51"/>
<wire x1="0.75" y1="-2.25" x2="0.75" y2="-1.6" width="0.1016" layer="51"/>
<pad name="1" x="1.25" y="1.8" drill="0.7" diameter="1.27"/>
<pad name="4" x="1.25" y="-1.8" drill="0.7" diameter="1.27"/>
<pad name="2" x="-1.25" y="1.8" drill="0.7" diameter="1.27"/>
<pad name="3" x="-1.25" y="-1.8" drill="0.7" diameter="1.27"/>
<text x="-4.5" y="5.5" size="1.27" layer="25">&gt;NAME</text>
<text x="-4.5" y="-7" size="1.27" layer="27">&gt;VALUE</text>
</package>
<package name="32005-101">
<description>&lt;b&gt;MINI USB 4P R/A SMT&lt;/b&gt; Two Salient Point&lt;p&gt;
Source: http://www.cypressindustries.com/pdf/32005-101.pdf</description>
<wire x1="-3.5464" y1="3.6429" x2="-1.8857" y2="3.6429" width="0.1016" layer="21"/>
<wire x1="-1.8857" y1="3.6429" x2="-1.8857" y2="3.1125" width="0.1016" layer="21"/>
<wire x1="-1.8857" y1="-3.2125" x2="-1.8857" y2="-3.6428" width="0.1016" layer="21"/>
<wire x1="-1.8857" y1="-3.6428" x2="-3.5464" y2="-3.6428" width="0.1016" layer="21"/>
<wire x1="-3.5464" y1="-3.6428" x2="-3.5464" y2="3.6429" width="0.1016" layer="21"/>
<wire x1="-1.8321" y1="3.1072" x2="-0.4794" y2="3.1072" width="0.1016" layer="51"/>
<wire x1="-0.4794" y1="3.1072" x2="-0.4794" y2="4.4465" width="0.1016" layer="51"/>
<wire x1="-0.4794" y1="4.4465" x2="2.2661" y2="4.4465" width="0.1016" layer="51"/>
<wire x1="2.2661" y1="4.4465" x2="2.2661" y2="3.1072" width="0.1016" layer="51"/>
<wire x1="2.4269" y1="3.1072" x2="2.4269" y2="-3.2072" width="0.1016" layer="51"/>
<wire x1="2.4269" y1="-3.2072" x2="2.2661" y2="-3.2072" width="0.1016" layer="51"/>
<wire x1="2.2661" y1="-3.2072" x2="2.2661" y2="-4.4465" width="0.1016" layer="51"/>
<wire x1="2.2661" y1="-4.4465" x2="-0.466" y2="-4.4465" width="0.1016" layer="51"/>
<wire x1="-0.466" y1="-4.4465" x2="-0.466" y2="-3.2143" width="0.1016" layer="51"/>
<wire x1="-1.8321" y1="-3.2143" x2="-0.466" y2="-3.2143" width="0.1016" layer="51"/>
<wire x1="1.4626" y1="-3.234" x2="1.4626" y2="-3.9108" width="0.1016" layer="51"/>
<wire x1="1.4626" y1="-3.9108" x2="0.2304" y2="-3.9108" width="0.1016" layer="51"/>
<wire x1="0.2304" y1="-3.9108" x2="0.2304" y2="-3.234" width="0.1016" layer="51"/>
<wire x1="1.4626" y1="3.9108" x2="0.2304" y2="3.9108" width="0.1016" layer="51"/>
<wire x1="0.2304" y1="3.9108" x2="0.2304" y2="3.134" width="0.1016" layer="51"/>
<wire x1="1.4626" y1="3.1339" x2="1.4626" y2="3.9108" width="0.1016" layer="51"/>
<wire x1="-0.4794" y1="3.1072" x2="2.2661" y2="3.1072" width="0.1016" layer="51"/>
<wire x1="2.2661" y1="3.1072" x2="2.4269" y2="3.1072" width="0.1016" layer="51"/>
<wire x1="-0.466" y1="-3.2143" x2="2.429" y2="-3.2143" width="0.1016" layer="51"/>
<smd name="M1" x="0.85" y="3.875" dx="2.25" dy="3.8" layer="1" rot="R270"/>
<smd name="M2" x="0.85" y="-3.875" dx="2.25" dy="3.8" layer="1" rot="R270"/>
<smd name="1" x="3.15" y="1.2" dx="0.55" dy="2.5" layer="1" rot="R270"/>
<smd name="2" x="3.15" y="0.4" dx="0.55" dy="2.5" layer="1" rot="R270"/>
<smd name="3" x="3.15" y="-0.4" dx="0.55" dy="2.5" layer="1" rot="R270"/>
<smd name="4" x="3.15" y="-1.2" dx="0.55" dy="2.5" layer="1" rot="R270"/>
<text x="-2" y="5.5" size="1.27" layer="25">&gt;NAME</text>
<text x="-2" y="-6.5" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="3.0125" y1="0.4125" x2="3.4125" y2="1.9875" layer="51" rot="R270"/>
<rectangle x1="3.025" y1="-0.375" x2="3.425" y2="1.175" layer="51" rot="R270"/>
<rectangle x1="3.025" y1="-1.175" x2="3.425" y2="0.375" layer="51" rot="R270"/>
<rectangle x1="3.0375" y1="-1.9625" x2="3.4375" y2="-0.4375" layer="51" rot="R270"/>
<hole x="0" y="1.5" drill="1"/>
<hole x="0" y="-1.5" drill="1"/>
</package>
<package name="85-32004-10X">
<description>&lt;b&gt;USB B TYPE SINGLE R/A DIP&lt;/b&gt; With Rear Shell, Without Rear Shell&lt;p&gt;
Source: http://www.cypressindustries.com/pdf/85-32004-10x.pdf</description>
<wire x1="-10.225" y1="5.9625" x2="-10.225" y2="-5.9625" width="0.1016" layer="21"/>
<wire x1="-10.225" y1="-5.9625" x2="6.1625" y2="-5.9625" width="0.1016" layer="51"/>
<wire x1="6.1625" y1="-5.9625" x2="6.1625" y2="5.9625" width="0.1016" layer="21"/>
<wire x1="6.1625" y1="5.9625" x2="-10.225" y2="5.9625" width="0.1016" layer="51"/>
<wire x1="1.15" y1="6.0125" x2="0.975" y2="7.175" width="0.1016" layer="51"/>
<wire x1="0.975" y1="7.175" x2="-0.975" y2="7.175" width="0.1016" layer="51"/>
<wire x1="-0.975" y1="7.175" x2="-1.1375" y2="6.025" width="0.1016" layer="51"/>
<wire x1="-1.15" y1="-6.0125" x2="-0.975" y2="-7.175" width="0.1016" layer="51"/>
<wire x1="-0.975" y1="-7.175" x2="0.975" y2="-7.175" width="0.1016" layer="51"/>
<wire x1="0.975" y1="-7.175" x2="1.1375" y2="-6.025" width="0.1016" layer="51"/>
<wire x1="-1.8375" y1="5.9625" x2="-10.225" y2="5.9625" width="0.1016" layer="21"/>
<wire x1="-10.225" y1="-5.9625" x2="-1.8375" y2="-5.9625" width="0.1016" layer="21"/>
<wire x1="6.1625" y1="5.9625" x2="1.775" y2="5.9625" width="0.1016" layer="21"/>
<wire x1="1.775" y1="-5.9625" x2="6.1625" y2="-5.9625" width="0.1016" layer="21"/>
<pad name="M1" x="0" y="6.02" drill="2.3" diameter="3" rot="R270"/>
<pad name="M2" x="0" y="-6.02" drill="2.3" diameter="3" rot="R270"/>
<pad name="2" x="4.71" y="1.25" drill="0.92" diameter="1.27" rot="R270"/>
<pad name="3" x="2.71" y="1.25" drill="0.92" diameter="1.27" rot="R270"/>
<pad name="1" x="4.71" y="-1.25" drill="0.92" diameter="1.27" rot="R270"/>
<pad name="4" x="2.71" y="-1.25" drill="0.92" diameter="1.27" rot="R270"/>
<text x="-7.62" y="6.35" size="1.27" layer="25">&gt;NAME</text>
<text x="-8.255" y="1.27" size="1.27" layer="27">&gt;VALUE</text>
</package>
<package name="85-32006-601-1">
<description>&lt;b&gt;1394-4P R/A SMT W/ SIDE FLANGE&lt;/b&gt;&lt;p&gt;
IEEE1394 4 Pin Firewire Connector&lt;br&gt;
Source: http://www.cypressindustries.com/pdf/85-32006-601-1.pdf</description>
<wire x1="4.2155" y1="-3.5433" x2="3.7635" y2="-3.9953" width="0.1016" layer="21" curve="-90"/>
<wire x1="-4.2215" y1="4.4416" x2="-4.2215" y2="7.5301" width="0.1016" layer="51"/>
<wire x1="-4.2215" y1="7.5301" x2="-5.9541" y2="7.5301" width="0.1016" layer="51"/>
<wire x1="-5.9541" y1="7.5301" x2="-6.2554" y2="7.2288" width="0.1016" layer="51" curve="90"/>
<wire x1="-6.2554" y1="7.2288" x2="-6.2554" y2="6.7768" width="0.1016" layer="51"/>
<wire x1="-4.2215" y1="2.5584" x2="-8.2139" y2="2.5584" width="0.1016" layer="21"/>
<wire x1="-8.2139" y1="2.5584" x2="-8.5153" y2="2.257" width="0.1016" layer="21" curve="90"/>
<wire x1="-8.5153" y1="2.257" x2="-8.5153" y2="-2.1121" width="0.1016" layer="21"/>
<wire x1="-8.5153" y1="-2.1121" x2="-8.214" y2="-2.4134" width="0.1016" layer="21" curve="89.923986"/>
<wire x1="-8.214" y1="-2.4134" x2="-4.2215" y2="-2.4134" width="0.1016" layer="21"/>
<wire x1="-6.2553" y1="6.7768" x2="-5.502" y2="6.0235" width="0.1016" layer="51" curve="-90.015214"/>
<wire x1="-5.502" y1="6.0235" x2="-6.2553" y2="5.2702" width="0.1016" layer="51" curve="-90"/>
<wire x1="-6.2553" y1="5.2702" x2="-6.2553" y2="4.7429" width="0.1016" layer="51"/>
<wire x1="-6.2553" y1="4.7429" x2="-5.954" y2="4.4416" width="0.1016" layer="51" curve="90.076115"/>
<wire x1="-5.954" y1="4.4416" x2="-4.2214" y2="4.4416" width="0.1016" layer="51"/>
<wire x1="-4.2214" y1="4.4416" x2="-4.2214" y2="3.0103" width="0.1016" layer="51"/>
<wire x1="-4.2214" y1="3.0103" x2="-4.2214" y2="1.3531" width="0.1016" layer="21"/>
<wire x1="-4.2214" y1="1.3531" x2="-4.2214" y2="-1.9614" width="0.1016" layer="21"/>
<wire x1="-4.2214" y1="-1.9614" x2="-4.2214" y2="-3.5433" width="0.1016" layer="21"/>
<wire x1="-4.2214" y1="-3.5433" x2="-3.7694" y2="-3.9953" width="0.1016" layer="21" curve="90"/>
<wire x1="-3.7694" y1="-3.9953" x2="3.7636" y2="-3.9953" width="0.1016" layer="21"/>
<wire x1="4.2156" y1="-3.5433" x2="4.2156" y2="-2.4134" width="0.1016" layer="21"/>
<wire x1="4.2156" y1="-2.4134" x2="4.2156" y2="2.5584" width="0.1016" layer="21"/>
<wire x1="4.2156" y1="2.5584" x2="4.2156" y2="3.0103" width="0.1016" layer="21"/>
<wire x1="4.2156" y1="3.0103" x2="4.2156" y2="4.4416" width="0.1016" layer="51"/>
<wire x1="4.2156" y1="4.4416" x2="4.2156" y2="7.5301" width="0.1016" layer="51"/>
<wire x1="4.2156" y1="7.5301" x2="5.9482" y2="7.5301" width="0.1016" layer="51"/>
<wire x1="5.9482" y1="7.5301" x2="6.2495" y2="7.2288" width="0.1016" layer="51" curve="-90"/>
<wire x1="6.2495" y1="7.2288" x2="6.2495" y2="6.7768" width="0.1016" layer="51"/>
<wire x1="6.2495" y1="6.7768" x2="5.4962" y2="6.0235" width="0.1016" layer="51" curve="89.98479"/>
<wire x1="5.4962" y1="6.0235" x2="6.2495" y2="5.2702" width="0.1016" layer="51" curve="90"/>
<wire x1="6.2495" y1="5.2702" x2="6.2495" y2="4.7429" width="0.1016" layer="51"/>
<wire x1="6.2495" y1="4.7429" x2="5.9482" y2="4.4416" width="0.1016" layer="51" curve="-90"/>
<wire x1="5.9482" y1="4.4416" x2="4.2156" y2="4.4416" width="0.1016" layer="51"/>
<wire x1="4.2156" y1="2.5584" x2="8.208" y2="2.5584" width="0.1016" layer="21"/>
<wire x1="8.208" y1="2.5584" x2="8.5094" y2="2.257" width="0.1016" layer="21" curve="-90"/>
<wire x1="8.5094" y1="2.257" x2="8.5094" y2="-2.1121" width="0.1016" layer="21"/>
<wire x1="8.5094" y1="-2.1121" x2="8.2081" y2="-2.4134" width="0.1016" layer="21" curve="-89.923986"/>
<wire x1="8.2081" y1="-2.4134" x2="4.2156" y2="-2.4134" width="0.1016" layer="21"/>
<wire x1="-3.2421" y1="-4.0707" x2="-3.2421" y2="-5.8786" width="0.1016" layer="21"/>
<wire x1="-3.2421" y1="-5.8786" x2="3.2363" y2="-5.8786" width="0.1016" layer="21"/>
<wire x1="3.2363" y1="-5.8786" x2="3.2363" y2="-4.0707" width="0.1016" layer="21"/>
<wire x1="2.5583" y1="7.5301" x2="3.4623" y2="7.5301" width="0.1016" layer="51"/>
<wire x1="3.4623" y1="7.5301" x2="3.7636" y2="7.2288" width="0.1016" layer="51" curve="-90"/>
<wire x1="3.7636" y1="7.2288" x2="3.7636" y2="3.4623" width="0.1016" layer="51"/>
<wire x1="2.5583" y1="7.5301" x2="2.5583" y2="3.4623" width="0.1016" layer="21"/>
<wire x1="2.5583" y1="3.4623" x2="2.5583" y2="3.0103" width="0.1016" layer="21"/>
<wire x1="2.6336" y1="3.4623" x2="3.7636" y2="3.4623" width="0.1016" layer="21"/>
<wire x1="3.7636" y1="3.4623" x2="4.2156" y2="3.0103" width="0.1016" layer="21" curve="-90"/>
<wire x1="2.5583" y1="3.0103" x2="3.5377" y2="3.0103" width="0.1016" layer="21"/>
<wire x1="3.5377" y1="3.0103" x2="3.7636" y2="2.7844" width="0.1016" layer="21" curve="-90"/>
<wire x1="3.7636" y1="2.7844" x2="3.7636" y2="1.6544" width="0.1016" layer="21"/>
<wire x1="3.7636" y1="1.6544" x2="3.387" y2="1.2777" width="0.1016" layer="21"/>
<wire x1="3.387" y1="1.2777" x2="3.387" y2="-1.8861" width="0.1016" layer="21"/>
<wire x1="3.387" y1="-1.8861" x2="3.7636" y2="-2.2628" width="0.1016" layer="21"/>
<wire x1="3.7636" y1="-2.2628" x2="3.7636" y2="-3.3174" width="0.1016" layer="21"/>
<wire x1="3.7636" y1="-3.3174" x2="3.613" y2="-3.468" width="0.1016" layer="21" curve="-90"/>
<wire x1="3.613" y1="-3.468" x2="-3.6187" y2="-3.468" width="0.1016" layer="21"/>
<wire x1="-3.6187" y1="-3.468" x2="-3.7694" y2="-3.3173" width="0.1016" layer="21" curve="-89.924011"/>
<wire x1="-3.7694" y1="-3.3173" x2="-3.7694" y2="-2.2627" width="0.1016" layer="21"/>
<wire x1="-3.7694" y1="-2.2627" x2="-3.3928" y2="-1.8861" width="0.1016" layer="21"/>
<wire x1="-3.3928" y1="-1.8861" x2="-3.3928" y2="1.2778" width="0.1016" layer="21"/>
<wire x1="-3.3928" y1="1.2778" x2="-3.7694" y2="1.6544" width="0.1016" layer="21"/>
<wire x1="-3.7694" y1="1.6544" x2="-3.7694" y2="2.8597" width="0.1016" layer="21"/>
<wire x1="-3.7694" y1="2.8597" x2="-3.6188" y2="3.0103" width="0.1016" layer="21" curve="-90"/>
<wire x1="-3.6188" y1="3.0103" x2="-2.5642" y2="3.0103" width="0.1016" layer="21"/>
<wire x1="-2.5642" y1="3.0103" x2="2.5583" y2="3.0103" width="0.1016" layer="21"/>
<wire x1="-2.5641" y1="7.5301" x2="-2.5641" y2="3.4623" width="0.1016" layer="21"/>
<wire x1="-2.5641" y1="3.4623" x2="-2.5642" y2="3.0103" width="0.1016" layer="21"/>
<wire x1="-2.5641" y1="7.5301" x2="-3.4681" y2="7.5301" width="0.1016" layer="51"/>
<wire x1="-3.4681" y1="7.5301" x2="-3.7694" y2="7.2288" width="0.1016" layer="51" curve="89.923986"/>
<wire x1="-3.7694" y1="7.2288" x2="-3.7694" y2="3.5377" width="0.1016" layer="51"/>
<wire x1="-2.5641" y1="3.4623" x2="-3.7694" y2="3.4623" width="0.1016" layer="21"/>
<wire x1="-3.7694" y1="3.4623" x2="-4.2214" y2="3.0103" width="0.1016" layer="21" curve="90"/>
<wire x1="-4.2214" y1="1.3531" x2="-3.8447" y2="0.9764" width="0.1016" layer="21"/>
<wire x1="-3.8447" y1="0.9764" x2="-3.8447" y2="-1.5847" width="0.1016" layer="21"/>
<wire x1="-3.8447" y1="-1.5847" x2="-4.2214" y2="-1.9614" width="0.1016" layer="21"/>
<wire x1="-2.5641" y1="3.4623" x2="-1.8862" y2="3.4623" width="0.1016" layer="21"/>
<wire x1="-1.8862" y1="3.4623" x2="-1.8862" y2="4.2156" width="0.1016" layer="21"/>
<wire x1="-1.8862" y1="4.2156" x2="-1.6602" y2="4.4416" width="0.1016" layer="21"/>
<wire x1="-1.6602" y1="4.4416" x2="1.6544" y2="4.4416" width="0.1016" layer="51"/>
<wire x1="1.6544" y1="4.4416" x2="1.8804" y2="4.2157" width="0.1016" layer="21"/>
<wire x1="1.8804" y1="4.2157" x2="1.8804" y2="3.4623" width="0.1016" layer="21"/>
<wire x1="1.8804" y1="3.4623" x2="2.5583" y2="3.4623" width="0.1016" layer="21"/>
<wire x1="4.2155" y1="-1.9614" x2="3.8388" y2="-1.5847" width="0.1016" layer="21"/>
<wire x1="3.8388" y1="-1.5847" x2="3.8388" y2="0.9764" width="0.1016" layer="21"/>
<wire x1="3.8388" y1="0.9764" x2="4.2155" y2="1.3531" width="0.1016" layer="21"/>
<wire x1="-4.2214" y1="7.5301" x2="-3.4681" y2="7.5301" width="0.1016" layer="51"/>
<wire x1="3.4622" y1="7.5301" x2="4.2155" y2="7.5301" width="0.1016" layer="51"/>
<wire x1="-2.5641" y1="7.5301" x2="-3.2681" y2="7.5301" width="0.1016" layer="21"/>
<wire x1="2.5583" y1="7.5301" x2="3.2623" y2="7.5301" width="0.1016" layer="21"/>
<smd name="M1" x="-5.05" y="6" dx="3.4" dy="3.4" layer="1"/>
<smd name="M2" x="5.05" y="6" dx="3.4" dy="3.4" layer="1"/>
<smd name="1" x="-1.2" y="4.775" dx="0.5" dy="1.95" layer="1"/>
<smd name="2" x="-0.4" y="4.775" dx="0.5" dy="1.95" layer="1"/>
<smd name="3" x="0.4" y="4.775" dx="0.5" dy="1.95" layer="1"/>
<smd name="4" x="1.2" y="4.775" dx="0.5" dy="1.95" layer="1"/>
<text x="-2.2" y="6.4" size="1.27" layer="25">&gt;NAME</text>
<text x="-3" y="1.4" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-1.4" y1="4.45" x2="-1" y2="5.7" layer="51"/>
<rectangle x1="-0.6" y1="4.45" x2="-0.2" y2="5.7" layer="51"/>
<rectangle x1="0.2" y1="4.45" x2="0.6" y2="5.7" layer="51"/>
<rectangle x1="1" y1="4.45" x2="1.4" y2="5.7" layer="51"/>
<hole x="-6.5" y="0" drill="2.8"/>
<hole x="6.5" y="0" drill="2.8"/>
</package>
<package name="85-32006-201-1">
<description>&lt;b&gt;1394-4P R/A DIP TWO Legs 1.2 PITCH&lt;/b&gt;&lt;p&gt;
IEEE1394 4 Pin Firewire Connector&lt;br&gt;
Source: http://www.cypressindustries.com/pdf/85-32006-201-1.pdf</description>
<wire x1="3.3" y1="3.4" x2="3.8" y2="3.9" width="0" layer="46" curve="-90"/>
<wire x1="3.8" y1="3.9" x2="4.3" y2="3.4" width="0" layer="46" curve="-90"/>
<wire x1="4.3" y1="3.4" x2="4.3" y2="2.4" width="0" layer="46"/>
<wire x1="4.3" y1="2.4" x2="3.8" y2="1.9" width="0" layer="46" curve="-90"/>
<wire x1="3.8" y1="1.9" x2="3.3" y2="2.4" width="0" layer="46" curve="-90"/>
<wire x1="3.3" y1="2.4" x2="3.3" y2="3.4" width="0" layer="46"/>
<wire x1="-4.3" y1="3.4" x2="-3.8" y2="3.9" width="0" layer="46" curve="-90"/>
<wire x1="-3.8" y1="3.9" x2="-3.3" y2="3.4" width="0" layer="46" curve="-90"/>
<wire x1="-3.3" y1="3.4" x2="-3.3" y2="2.4" width="0" layer="46"/>
<wire x1="-3.3" y1="2.4" x2="-3.8" y2="1.9" width="0" layer="46" curve="-90"/>
<wire x1="-3.8" y1="1.9" x2="-4.3" y2="2.4" width="0" layer="46" curve="-90"/>
<wire x1="-4.3" y1="2.4" x2="-4.3" y2="3.4" width="0" layer="46"/>
<wire x1="3.3" y1="8.15" x2="3.8" y2="8.65" width="0" layer="46" curve="-90"/>
<wire x1="3.8" y1="8.65" x2="4.3" y2="8.15" width="0" layer="46" curve="-90"/>
<wire x1="4.3" y1="8.15" x2="4.3" y2="7.15" width="0" layer="46"/>
<wire x1="4.3" y1="7.15" x2="3.8" y2="6.65" width="0" layer="46" curve="-90"/>
<wire x1="3.8" y1="6.65" x2="3.3" y2="7.15" width="0" layer="46" curve="-90"/>
<wire x1="3.3" y1="7.15" x2="3.3" y2="8.15" width="0" layer="46"/>
<wire x1="-4.3" y1="8.15" x2="-3.8" y2="8.65" width="0" layer="46" curve="-90"/>
<wire x1="-3.8" y1="8.65" x2="-3.3" y2="8.15" width="0" layer="46" curve="-90"/>
<wire x1="-3.3" y1="8.15" x2="-3.3" y2="7.15" width="0" layer="46"/>
<wire x1="-3.3" y1="7.15" x2="-3.8" y2="6.65" width="0" layer="46" curve="-90"/>
<wire x1="-3.8" y1="6.65" x2="-4.3" y2="7.15" width="0" layer="46" curve="-90"/>
<wire x1="-4.3" y1="7.15" x2="-4.3" y2="8.15" width="0" layer="46"/>
<wire x1="-4.0064" y1="8.666" x2="-3.5273" y2="8.666" width="0.1016" layer="51"/>
<wire x1="-3.5273" y1="8.666" x2="-3.5273" y2="7.6209" width="0.1016" layer="51"/>
<wire x1="-3.5273" y1="7.6209" x2="-2.5257" y2="7.6209" width="0.1016" layer="51"/>
<wire x1="-2.5257" y1="7.6209" x2="-2.5257" y2="6.5322" width="0.1016" layer="21"/>
<wire x1="-3.5273" y1="7.6209" x2="-3.5273" y2="6.4887" width="0.1016" layer="51"/>
<wire x1="-4.0064" y1="8.666" x2="-4.0064" y2="6.4451" width="0.1016" layer="51"/>
<wire x1="-4.0064" y1="6.4451" x2="-4.0064" y2="0.0688" width="0.1016" layer="51"/>
<wire x1="-4.0064" y1="0.0688" x2="-3.3714" y2="-0.5662" width="0.1016" layer="21" curve="90"/>
<wire x1="-3.3714" y1="-0.5662" x2="3.3714" y2="-0.5662" width="0.1016" layer="21"/>
<wire x1="3.3714" y1="-0.5662" x2="4.0064" y2="0.0688" width="0.1016" layer="21" curve="90"/>
<wire x1="4.0064" y1="0.0688" x2="4.0064" y2="6.4451" width="0.1016" layer="51"/>
<wire x1="4.0064" y1="6.4451" x2="4.0064" y2="8.666" width="0.1016" layer="51"/>
<wire x1="4.0064" y1="8.666" x2="3.571" y2="8.666" width="0.1016" layer="51"/>
<wire x1="3.5274" y1="7.6209" x2="2.5258" y2="7.6209" width="0.1016" layer="51"/>
<wire x1="2.5258" y1="7.6209" x2="2.5258" y2="6.4886" width="0.1016" layer="21"/>
<wire x1="3.5274" y1="6.4886" x2="-3.5273" y2="6.4886" width="0.1016" layer="51"/>
<wire x1="-3.5273" y1="6.4886" x2="-3.5273" y2="0.2304" width="0.1016" layer="51"/>
<wire x1="-3.5273" y1="0.2304" x2="-3.2098" y2="-0.0871" width="0.1016" layer="21" curve="90.036103"/>
<wire x1="-3.2098" y1="-0.0871" x2="3.2099" y2="-0.0871" width="0.1016" layer="21"/>
<wire x1="3.2099" y1="-0.0871" x2="3.5274" y2="0.2304" width="0.1016" layer="21" curve="90.036103"/>
<wire x1="3.5274" y1="0.2304" x2="3.5274" y2="6.9241" width="0.1016" layer="51"/>
<wire x1="3.5274" y1="6.9241" x2="3.5274" y2="8.666" width="0.1016" layer="51"/>
<wire x1="-3.4838" y1="5.6177" x2="-3.266" y2="5.6177" width="0.1016" layer="21"/>
<wire x1="-3.266" y1="5.6177" x2="-3.266" y2="5.1822" width="0.1016" layer="21" curve="-180"/>
<wire x1="-3.266" y1="5.1822" x2="-3.4838" y2="5.1822" width="0.1016" layer="21"/>
<wire x1="-3.4838" y1="0.8274" x2="-3.266" y2="0.8274" width="0.1016" layer="21"/>
<wire x1="-3.266" y1="0.8274" x2="-3.266" y2="0.3919" width="0.1016" layer="21" curve="-180"/>
<wire x1="-3.266" y1="0.3919" x2="-3.4838" y2="0.3919" width="0.1016" layer="21"/>
<wire x1="3.4404" y1="0.3919" x2="3.2226" y2="0.3919" width="0.1016" layer="21"/>
<wire x1="3.2226" y1="0.3919" x2="3.2226" y2="0.8274" width="0.1016" layer="21" curve="-180"/>
<wire x1="3.2226" y1="0.8274" x2="3.4404" y2="0.8274" width="0.1016" layer="21"/>
<wire x1="3.4839" y1="5.1822" x2="3.2661" y2="5.1822" width="0.1016" layer="21"/>
<wire x1="3.2661" y1="5.1822" x2="3.2661" y2="5.6177" width="0.1016" layer="21" curve="-180"/>
<wire x1="3.2661" y1="5.6177" x2="3.4839" y2="5.6177" width="0.1016" layer="21"/>
<wire x1="-2.5693" y1="6.9241" x2="-3.5274" y2="6.9241" width="0.1016" layer="51"/>
<wire x1="-3.5274" y1="6.9241" x2="-4.0064" y2="6.4451" width="0.1016" layer="51" curve="90"/>
<wire x1="2.5693" y1="6.9241" x2="3.5274" y2="6.9241" width="0.1016" layer="51"/>
<wire x1="3.5274" y1="6.9241" x2="4.0064" y2="6.4451" width="0.1016" layer="51" curve="-90"/>
<wire x1="-3.1354" y1="-0.6097" x2="-3.1354" y2="-2.4387" width="0.1016" layer="21"/>
<wire x1="-3.1354" y1="-2.4387" x2="3.1355" y2="-2.4387" width="0.1016" layer="21"/>
<wire x1="3.1355" y1="-2.4387" x2="3.1355" y2="-0.6097" width="0.1016" layer="21"/>
<wire x1="-4.0064" y1="5.9451" x2="-4.0064" y2="4.5688" width="0.1016" layer="21"/>
<wire x1="4.0064" y1="0.0688" x2="4.0064" y2="0.9451" width="0.1016" layer="21"/>
<wire x1="-4.0064" y1="0.9451" x2="-4.0064" y2="0.0688" width="0.1016" layer="21"/>
<wire x1="4.0064" y1="4.5688" x2="4.0064" y2="5.9451" width="0.1016" layer="21"/>
<wire x1="-3.5273" y1="5.9886" x2="-3.5273" y2="4.7304" width="0.1016" layer="21"/>
<wire x1="3.5274" y1="4.7304" x2="3.5274" y2="5.9241" width="0.1016" layer="21"/>
<wire x1="-3.5273" y1="0.9886" x2="-3.5273" y2="0.2304" width="0.1016" layer="21"/>
<wire x1="3.5274" y1="0.2304" x2="3.5274" y2="0.9241" width="0.1016" layer="21"/>
<wire x1="3.0274" y1="6.4886" x2="-3.0273" y2="6.4886" width="0.1016" layer="21"/>
<pad name="M1" x="-3.8" y="2.9" drill="1" diameter="1.4" shape="long" rot="R90"/>
<pad name="M2" x="3.8" y="2.9" drill="1" diameter="1.4" shape="long" rot="R90"/>
<pad name="M3" x="-3.8" y="7.65" drill="1" diameter="1.4" shape="long" rot="R90"/>
<pad name="M4" x="3.8" y="7.65" drill="1" diameter="1.4" shape="long" rot="R90"/>
<pad name="1" x="-1.2" y="8.1" drill="0.7" diameter="1.1"/>
<pad name="2" x="-0.4" y="6.9" drill="0.7" diameter="1.1"/>
<pad name="3" x="0.4" y="8.1" drill="0.7" diameter="1.1"/>
<pad name="4" x="1.2" y="6.9" drill="0.7" diameter="1.1"/>
<text x="-5" y="0.5" size="1.27" layer="25" rot="R90">&gt;NAME</text>
<text x="6.5" y="0.5" size="1.27" layer="27" rot="R90">&gt;VALUE</text>
</package>
<package name="32005-601">
<description>&lt;b&gt;MINI USB-B R/A DIP&lt;/b&gt;&lt;p&gt;
Source: http://www.cypressindustries.com/pdf/32005-601.pdf</description>
<wire x1="-5.9228" y1="3.8473" x2="2.9098" y2="3.8473" width="0.1016" layer="51"/>
<wire x1="2.9404" y1="3.7967" x2="2.9404" y2="2.5986" width="0.1016" layer="21"/>
<wire x1="2.9404" y1="2.5986" x2="1.8098" y2="2.5986" width="0.1016" layer="21"/>
<wire x1="2.9097" y1="-3.8473" x2="-5.9228" y2="-3.8473" width="0.1016" layer="51"/>
<wire x1="-5.9228" y1="-3.8473" x2="-5.9228" y2="3.8473" width="0.1016" layer="21"/>
<wire x1="2.9573" y1="-3.8217" x2="2.9573" y2="-2.6998" width="0.1016" layer="21"/>
<wire x1="2.9573" y1="-2.6998" x2="1.8098" y2="-2.6998" width="0.1016" layer="21"/>
<wire x1="-5.9182" y1="3.8416" x2="-3.8879" y2="3.8416" width="0.1016" layer="51"/>
<wire x1="-5.9182" y1="-3.8415" x2="-5.9182" y2="-3.8414" width="0.1016" layer="21"/>
<wire x1="-5.9182" y1="-3.8414" x2="-5.9182" y2="3.8416" width="0.1016" layer="21"/>
<wire x1="-1.8171" y1="2.9591" x2="-4.5685" y2="2.7514" width="0.1016" layer="21"/>
<wire x1="-4.5685" y1="2.7514" x2="-4.828" y2="2.5438" width="0.1016" layer="21" curve="68.629849"/>
<wire x1="-4.828" y1="2.5438" x2="-4.828" y2="1.9727" width="0.1016" layer="21" curve="34.099487"/>
<wire x1="-4.828" y1="1.9727" x2="-4.5685" y2="1.7651" width="0.1016" layer="21" curve="68.629849"/>
<wire x1="-4.5685" y1="1.7651" x2="-1.8171" y2="1.5055" width="0.1016" layer="21"/>
<wire x1="-1.8171" y1="1.5055" x2="-1.8171" y2="1.7132" width="0.1016" layer="21"/>
<wire x1="-1.8171" y1="1.7132" x2="-4.2051" y2="1.9727" width="0.1016" layer="21"/>
<wire x1="-4.2051" y1="1.9727" x2="-4.2051" y2="2.4919" width="0.1016" layer="21"/>
<wire x1="-4.2051" y1="2.4919" x2="-1.8171" y2="2.7514" width="0.1016" layer="21"/>
<wire x1="-1.8171" y1="2.7514" x2="-1.8171" y2="2.9591" width="0.1016" layer="21"/>
<wire x1="-4.5684" y1="1.2459" x2="-0.5192" y2="1.0383" width="0.1016" layer="21"/>
<wire x1="-0.5192" y1="1.0383" x2="-0.3116" y2="0.8306" width="0.1016" layer="21" curve="-83.771817"/>
<wire x1="-4.5685" y1="1.2459" x2="-4.7761" y2="1.0383" width="0.1016" layer="21" curve="90"/>
<wire x1="-4.7761" y1="1.0383" x2="-4.7761" y2="1.0382" width="0.1016" layer="21"/>
<wire x1="-4.7761" y1="1.0382" x2="-4.5685" y2="0.8306" width="0.1016" layer="21" curve="90"/>
<wire x1="-4.5685" y1="0.8306" x2="-1.1422" y2="0.623" width="0.1016" layer="21"/>
<wire x1="-5.9182" y1="-3.8414" x2="-3.8879" y2="-3.8414" width="0.1016" layer="51"/>
<wire x1="-1.8171" y1="-2.9589" x2="-4.5685" y2="-2.7512" width="0.1016" layer="21"/>
<wire x1="-4.5685" y1="-2.7512" x2="-4.828" y2="-2.5436" width="0.1016" layer="21" curve="-68.629849"/>
<wire x1="-4.828" y1="-2.5436" x2="-4.828" y2="-1.9725" width="0.1016" layer="21" curve="-34.099487"/>
<wire x1="-4.828" y1="-1.9725" x2="-4.5685" y2="-1.7649" width="0.1016" layer="21" curve="-68.629849"/>
<wire x1="-4.5685" y1="-1.7649" x2="-1.8171" y2="-1.5053" width="0.1016" layer="21"/>
<wire x1="-1.8171" y1="-1.5053" x2="-1.8171" y2="-1.713" width="0.1016" layer="21"/>
<wire x1="-1.8171" y1="-1.713" x2="-4.2051" y2="-1.9725" width="0.1016" layer="21"/>
<wire x1="-4.2051" y1="-1.9725" x2="-4.2051" y2="-2.4917" width="0.1016" layer="21"/>
<wire x1="-4.2051" y1="-2.4917" x2="-1.8171" y2="-2.7512" width="0.1016" layer="21"/>
<wire x1="-1.8171" y1="-2.7512" x2="-1.8171" y2="-2.9589" width="0.1016" layer="21"/>
<wire x1="-4.5684" y1="-1.2457" x2="-0.5192" y2="-1.0381" width="0.1016" layer="21"/>
<wire x1="-0.5192" y1="-1.0381" x2="-0.3116" y2="-0.8304" width="0.1016" layer="21" curve="83.722654"/>
<wire x1="-0.3116" y1="-0.8304" x2="-0.3116" y2="0.8307" width="0.1016" layer="21"/>
<wire x1="-4.5685" y1="-1.2457" x2="-4.7761" y2="-1.0381" width="0.1016" layer="21" curve="-90"/>
<wire x1="-4.7761" y1="-1.038" x2="-4.5685" y2="-0.8304" width="0.1016" layer="21" curve="-90"/>
<wire x1="-4.5685" y1="-0.8304" x2="-1.1422" y2="-0.6228" width="0.1016" layer="21"/>
<wire x1="-1.1422" y1="-0.6228" x2="-1.1422" y2="0.6232" width="0.1016" layer="21"/>
<wire x1="-5.9182" y1="-3.8414" x2="-3.8146" y2="-3.8414" width="0.1016" layer="21"/>
<wire x1="-5.9182" y1="3.8416" x2="-3.8647" y2="3.8415" width="0.1016" layer="21"/>
<wire x1="2.8842" y1="-3.8472" x2="-0.6281" y2="-3.8472" width="0.1016" layer="21"/>
<wire x1="-0.6523" y1="3.8472" x2="2.8331" y2="3.8473" width="0.1016" layer="21"/>
<wire x1="-1.725" y1="4.15" x2="-1.225" y2="3.65" width="0" layer="46" curve="-90"/>
<wire x1="-1.225" y1="3.65" x2="-1.725" y2="3.15" width="0" layer="46" curve="-90"/>
<wire x1="-1.725" y1="3.15" x2="-2.725" y2="3.15" width="0" layer="46"/>
<wire x1="-2.725" y1="3.15" x2="-3.225" y2="3.65" width="0" layer="46" curve="-90"/>
<wire x1="-3.225" y1="3.65" x2="-2.725" y2="4.15" width="0" layer="46" curve="-90"/>
<wire x1="-2.725" y1="4.15" x2="-1.725" y2="4.15" width="0" layer="46"/>
<wire x1="-1.525" y1="-3.15" x2="-1.225" y2="-3.65" width="0" layer="46" curve="-90"/>
<wire x1="-1.225" y1="-3.65" x2="-1.725" y2="-4.15" width="0" layer="46" curve="-90"/>
<wire x1="-1.725" y1="-4.15" x2="-2.725" y2="-4.15" width="0" layer="46"/>
<wire x1="-2.725" y1="-4.15" x2="-3.225" y2="-3.65" width="0" layer="46" curve="-90"/>
<wire x1="-3.225" y1="-3.65" x2="-2.725" y2="-3.15" width="0" layer="46" curve="-90"/>
<wire x1="-2.725" y1="-3.15" x2="-1.525" y2="-3.15" width="0" layer="46"/>
<wire x1="1.8098" y1="2.6048" x2="1.8098" y2="-2.6973" width="0.1016" layer="51"/>
<pad name="M1" x="-2.225" y="3.65" drill="1" diameter="1.4224" shape="long"/>
<pad name="M2" x="-2.225" y="-3.65" drill="1" diameter="1.4224" shape="long"/>
<pad name="4" x="1.625" y="-0.8" drill="0.7" diameter="1.016"/>
<pad name="3" x="2.825" y="0" drill="0.7" diameter="1.016"/>
<pad name="1" x="2.825" y="1.6" drill="0.7" diameter="1.016"/>
<pad name="5" x="2.825" y="-1.6" drill="0.7" diameter="1.016"/>
<pad name="2" x="1.625" y="0.8" drill="0.7" diameter="1.016"/>
<text x="-4.1155" y="6.4859" size="1.27" layer="25">&gt;NAME</text>
<text x="-4.3979" y="-7.8527" size="1.27" layer="27">&gt;VALUE</text>
</package>
</packages>
<symbols>
<symbol name="MINI-USB-4P-">
<wire x1="-2.54" y1="6.35" x2="-2.54" y2="-3.81" width="0.254" layer="94"/>
<wire x1="-2.54" y1="-3.81" x2="-1.27" y2="-5.08" width="0.254" layer="94" curve="90"/>
<wire x1="-1.27" y1="-5.08" x2="5.08" y2="-5.08" width="0.254" layer="94"/>
<wire x1="5.08" y1="-5.08" x2="6.35" y2="-3.81" width="0.254" layer="94" curve="90"/>
<wire x1="6.35" y1="-3.81" x2="6.35" y2="6.35" width="0.254" layer="94"/>
<wire x1="-2.54" y1="6.35" x2="-1.27" y2="7.62" width="0.254" layer="94" curve="-90"/>
<wire x1="-1.27" y1="7.62" x2="5.08" y2="7.62" width="0.254" layer="94"/>
<wire x1="5.08" y1="7.62" x2="6.35" y2="6.35" width="0.254" layer="94" curve="-90"/>
<wire x1="0" y1="5.08" x2="0" y2="-2.54" width="0.254" layer="94"/>
<wire x1="0" y1="-2.54" x2="1.27" y2="-3.81" width="0.254" layer="94"/>
<wire x1="1.27" y1="-3.81" x2="2.54" y2="-3.81" width="0.254" layer="94"/>
<wire x1="3.81" y1="-2.54" x2="3.81" y2="5.08" width="0.254" layer="94"/>
<wire x1="2.54" y1="6.35" x2="1.27" y2="6.35" width="0.254" layer="94"/>
<wire x1="1.27" y1="6.35" x2="0" y2="5.08" width="0.254" layer="94"/>
<wire x1="2.54" y1="6.35" x2="3.81" y2="5.08" width="0.254" layer="94"/>
<wire x1="2.54" y1="-3.81" x2="3.81" y2="-2.54" width="0.254" layer="94"/>
<text x="-2.54" y="10.16" size="1.778" layer="95" font="vector">&gt;NAME</text>
<text x="10.16" y="-5.08" size="1.778" layer="96" font="vector" rot="R90">&gt;VALUE</text>
<pin name="1" x="-5.08" y="5.08" visible="pin" direction="pas"/>
<pin name="2" x="-5.08" y="2.54" visible="pin" direction="pas"/>
<pin name="3" x="-5.08" y="0" visible="pin" direction="pas"/>
<pin name="4" x="-5.08" y="-2.54" visible="pin" direction="pas"/>
</symbol>
<symbol name="MINI-USB-5">
<wire x1="-2.54" y1="6.35" x2="-2.54" y2="-6.35" width="0.254" layer="94"/>
<wire x1="-2.54" y1="-6.35" x2="-1.27" y2="-7.62" width="0.254" layer="94" curve="90"/>
<wire x1="-1.27" y1="-7.62" x2="0" y2="-7.62" width="0.254" layer="94"/>
<wire x1="0" y1="-7.62" x2="1.016" y2="-8.128" width="0.254" layer="94" curve="-53.130102"/>
<wire x1="1.016" y1="-8.128" x2="2.54" y2="-8.89" width="0.254" layer="94" curve="53.130102"/>
<wire x1="2.54" y1="-8.89" x2="5.08" y2="-8.89" width="0.254" layer="94"/>
<wire x1="5.08" y1="-8.89" x2="6.35" y2="-7.62" width="0.254" layer="94" curve="90"/>
<wire x1="6.35" y1="-7.62" x2="6.35" y2="7.62" width="0.254" layer="94"/>
<wire x1="-2.54" y1="6.35" x2="-1.27" y2="7.62" width="0.254" layer="94" curve="-90"/>
<wire x1="-1.27" y1="7.62" x2="0" y2="7.62" width="0.254" layer="94"/>
<wire x1="0" y1="7.62" x2="1.016" y2="8.128" width="0.254" layer="94" curve="53.130102"/>
<wire x1="1.016" y1="8.128" x2="2.54" y2="8.89" width="0.254" layer="94" curve="-53.130102"/>
<wire x1="2.54" y1="8.89" x2="5.08" y2="8.89" width="0.254" layer="94"/>
<wire x1="5.08" y1="8.89" x2="6.35" y2="7.62" width="0.254" layer="94" curve="-90"/>
<wire x1="0" y1="5.08" x2="0" y2="-5.08" width="0.254" layer="94"/>
<wire x1="0" y1="-5.08" x2="1.27" y2="-6.35" width="0.254" layer="94"/>
<wire x1="1.27" y1="-6.35" x2="3.81" y2="-6.35" width="0.254" layer="94"/>
<wire x1="3.81" y1="-6.35" x2="3.81" y2="6.35" width="0.254" layer="94"/>
<wire x1="3.81" y1="6.35" x2="1.27" y2="6.35" width="0.254" layer="94"/>
<wire x1="1.27" y1="6.35" x2="0" y2="5.08" width="0.254" layer="94"/>
<text x="-2.54" y="11.43" size="1.778" layer="95" font="vector">&gt;NAME</text>
<text x="10.16" y="-7.62" size="1.778" layer="96" font="vector" rot="R90">&gt;VALUE</text>
<pin name="1" x="-5.08" y="5.08" visible="pin" direction="pas"/>
<pin name="2" x="-5.08" y="2.54" visible="pin" direction="pas"/>
<pin name="3" x="-5.08" y="0" visible="pin" direction="pas"/>
<pin name="4" x="-5.08" y="-2.54" visible="pin" direction="pas"/>
<pin name="5" x="-5.08" y="-5.08" visible="pin" direction="pas"/>
</symbol>
<symbol name="SHIELD2">
<wire x1="-2.54" y1="1.27" x2="0" y2="1.27" width="0.254" layer="94" style="shortdash"/>
<wire x1="-2.54" y1="0" x2="-2.54" y2="1.27" width="0.1524" layer="94"/>
<wire x1="0" y1="0" x2="0" y2="1.27" width="0.1524" layer="94"/>
<text x="1.27" y="-1.27" size="1.778" layer="95" font="vector">&gt;NAME</text>
<pin name="S1" x="-2.54" y="-2.54" visible="off" length="short" direction="pas" rot="R90"/>
<pin name="S2" x="0" y="-2.54" visible="off" length="short" direction="pas" rot="R90"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="MINI-USB_4P-" prefix="X">
<description>&lt;b&gt;MINI USB 4 Pol.&lt;/b&gt;&lt;p&gt;
Source: www.cypressindustries.com</description>
<gates>
<gate name="G$1" symbol="MINI-USB-4P-" x="0" y="0"/>
</gates>
<devices>
<device name="85-32004-40X" package="85-32004-40X">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
<connect gate="G$1" pin="3" pad="3"/>
<connect gate="G$1" pin="4" pad="4"/>
</connects>
<technologies>
<technology name="">
<attribute name="MF" value="" constant="no"/>
<attribute name="MPN" value="" constant="no"/>
<attribute name="OC_FARNELL" value="unknown" constant="no"/>
<attribute name="OC_NEWARK" value="unknown" constant="no"/>
</technology>
</technologies>
</device>
<device name="32005-101" package="32005-101">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
<connect gate="G$1" pin="3" pad="3"/>
<connect gate="G$1" pin="4" pad="4"/>
</connects>
<technologies>
<technology name="">
<attribute name="MF" value="" constant="no"/>
<attribute name="MPN" value="" constant="no"/>
<attribute name="OC_FARNELL" value="unknown" constant="no"/>
<attribute name="OC_NEWARK" value="unknown" constant="no"/>
</technology>
</technologies>
</device>
<device name="85-32004-10X" package="85-32004-10X">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
<connect gate="G$1" pin="3" pad="3"/>
<connect gate="G$1" pin="4" pad="4"/>
</connects>
<technologies>
<technology name="">
<attribute name="MF" value="" constant="no"/>
<attribute name="MPN" value="" constant="no"/>
<attribute name="OC_FARNELL" value="unknown" constant="no"/>
<attribute name="OC_NEWARK" value="unknown" constant="no"/>
</technology>
</technologies>
</device>
<device name="85-32006-601-1" package="85-32006-601-1">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
<connect gate="G$1" pin="3" pad="3"/>
<connect gate="G$1" pin="4" pad="4"/>
</connects>
<technologies>
<technology name="">
<attribute name="MF" value="" constant="no"/>
<attribute name="MPN" value="" constant="no"/>
<attribute name="OC_FARNELL" value="unknown" constant="no"/>
<attribute name="OC_NEWARK" value="unknown" constant="no"/>
</technology>
</technologies>
</device>
<device name="85-32006-201-1" package="85-32006-201-1">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
<connect gate="G$1" pin="3" pad="3"/>
<connect gate="G$1" pin="4" pad="4"/>
</connects>
<technologies>
<technology name="">
<attribute name="MF" value="" constant="no"/>
<attribute name="MPN" value="" constant="no"/>
<attribute name="OC_FARNELL" value="unknown" constant="no"/>
<attribute name="OC_NEWARK" value="unknown" constant="no"/>
</technology>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="MINI-USB_SHIELD5P2-" prefix="X">
<description>MINI USB-B R/A DIP&lt;/b&gt; 5pol.&lt;p&gt;
Source: www.cypressindustries.com</description>
<gates>
<gate name="G41" symbol="MINI-USB-5" x="0" y="0"/>
<gate name="S" symbol="SHIELD2" x="2.54" y="-12.7" addlevel="request"/>
</gates>
<devices>
<device name="32005-601" package="32005-601">
<connects>
<connect gate="G41" pin="1" pad="1"/>
<connect gate="G41" pin="2" pad="2"/>
<connect gate="G41" pin="3" pad="3"/>
<connect gate="G41" pin="4" pad="4"/>
<connect gate="G41" pin="5" pad="5"/>
<connect gate="S" pin="S1" pad="M1"/>
<connect gate="S" pin="S2" pad="M2"/>
</connects>
<technologies>
<technology name="">
<attribute name="MF" value="" constant="no"/>
<attribute name="MPN" value="" constant="no"/>
<attribute name="OC_FARNELL" value="unknown" constant="no"/>
<attribute name="OC_NEWARK" value="unknown" constant="no"/>
</technology>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="ftdichip">
<description>&lt;b&gt;FTDI (TM) CHIP&lt;/b&gt; Future Technology Devices International Ltd.&lt;p&gt;
http://www.ftdichip.com</description>
<packages>
<package name="SSOP28">
<description>&lt;b&gt;Shrink Small Outline Package&lt;/b&gt; SSOP-28&lt;p&gt;
http://www.ftdichip.com/Documents/DataSheets/DS_FT232R_v104.pdf</description>
<wire x1="-5.1" y1="-2.6" x2="5.1" y2="-2.6" width="0.2032" layer="21"/>
<wire x1="5.1" y1="-2.6" x2="5.1" y2="2.6" width="0.2032" layer="21"/>
<wire x1="5.1" y1="2.6" x2="-5.1" y2="2.6" width="0.2032" layer="21"/>
<wire x1="-5.1" y1="2.6" x2="-5.1" y2="-2.6" width="0.2032" layer="21"/>
<circle x="-4.2" y="-1.625" radius="0.4422" width="0" layer="21"/>
<smd name="1" x="-4.225" y="-3.625" dx="0.4" dy="1.5" layer="1"/>
<smd name="2" x="-3.575" y="-3.625" dx="0.4" dy="1.5" layer="1"/>
<smd name="3" x="-2.925" y="-3.625" dx="0.4" dy="1.5" layer="1"/>
<smd name="4" x="-2.275" y="-3.625" dx="0.4" dy="1.5" layer="1"/>
<smd name="5" x="-1.625" y="-3.625" dx="0.4" dy="1.5" layer="1"/>
<smd name="6" x="-0.975" y="-3.625" dx="0.4" dy="1.5" layer="1"/>
<smd name="7" x="-0.325" y="-3.625" dx="0.4" dy="1.5" layer="1"/>
<smd name="8" x="0.325" y="-3.625" dx="0.4" dy="1.5" layer="1"/>
<smd name="9" x="0.975" y="-3.625" dx="0.4" dy="1.5" layer="1"/>
<smd name="10" x="1.625" y="-3.625" dx="0.4" dy="1.5" layer="1"/>
<smd name="11" x="2.275" y="-3.625" dx="0.4" dy="1.5" layer="1"/>
<smd name="12" x="2.925" y="-3.625" dx="0.4" dy="1.5" layer="1"/>
<smd name="13" x="3.575" y="-3.625" dx="0.4" dy="1.5" layer="1"/>
<smd name="14" x="4.225" y="-3.625" dx="0.4" dy="1.5" layer="1"/>
<smd name="15" x="4.225" y="3.625" dx="0.4" dy="1.5" layer="1"/>
<smd name="16" x="3.575" y="3.625" dx="0.4" dy="1.5" layer="1"/>
<smd name="17" x="2.925" y="3.625" dx="0.4" dy="1.5" layer="1"/>
<smd name="18" x="2.275" y="3.625" dx="0.4" dy="1.5" layer="1"/>
<smd name="19" x="1.625" y="3.625" dx="0.4" dy="1.5" layer="1"/>
<smd name="20" x="0.975" y="3.625" dx="0.4" dy="1.5" layer="1"/>
<smd name="21" x="0.325" y="3.625" dx="0.4" dy="1.5" layer="1"/>
<smd name="22" x="-0.325" y="3.625" dx="0.4" dy="1.5" layer="1"/>
<smd name="23" x="-0.975" y="3.625" dx="0.4" dy="1.5" layer="1"/>
<smd name="24" x="-1.625" y="3.625" dx="0.4" dy="1.5" layer="1"/>
<smd name="25" x="-2.275" y="3.625" dx="0.4" dy="1.5" layer="1"/>
<smd name="26" x="-2.925" y="3.625" dx="0.4" dy="1.5" layer="1"/>
<smd name="27" x="-3.575" y="3.625" dx="0.4" dy="1.5" layer="1"/>
<smd name="28" x="-4.225" y="3.625" dx="0.4" dy="1.5" layer="1"/>
<text x="-5.476" y="-2.6299" size="1.27" layer="25" rot="R90">&gt;NAME</text>
<text x="-3.8999" y="-0.68" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-4.4028" y1="-3.937" x2="-4.0472" y2="-2.6416" layer="51"/>
<rectangle x1="-3.7529" y1="-3.937" x2="-3.3973" y2="-2.6416" layer="51"/>
<rectangle x1="-3.1029" y1="-3.937" x2="-2.7473" y2="-2.6416" layer="51"/>
<rectangle x1="-2.4529" y1="-3.937" x2="-2.0973" y2="-2.6416" layer="51"/>
<rectangle x1="-1.8029" y1="-3.937" x2="-1.4473" y2="-2.6416" layer="51"/>
<rectangle x1="-1.1529" y1="-3.937" x2="-0.7973" y2="-2.6416" layer="51"/>
<rectangle x1="-0.5029" y1="-3.937" x2="-0.1473" y2="-2.6416" layer="51"/>
<rectangle x1="0.1473" y1="-3.937" x2="0.5029" y2="-2.6416" layer="51"/>
<rectangle x1="0.7973" y1="-3.937" x2="1.1529" y2="-2.6416" layer="51"/>
<rectangle x1="1.4473" y1="-3.937" x2="1.8029" y2="-2.6416" layer="51"/>
<rectangle x1="2.0973" y1="-3.937" x2="2.4529" y2="-2.6416" layer="51"/>
<rectangle x1="2.7473" y1="-3.937" x2="3.1029" y2="-2.6416" layer="51"/>
<rectangle x1="3.3973" y1="-3.937" x2="3.7529" y2="-2.6416" layer="51"/>
<rectangle x1="4.0472" y1="-3.937" x2="4.4028" y2="-2.6416" layer="51"/>
<rectangle x1="4.0472" y1="2.6416" x2="4.4028" y2="3.937" layer="51"/>
<rectangle x1="3.3973" y1="2.6416" x2="3.7529" y2="3.937" layer="51"/>
<rectangle x1="2.7473" y1="2.6416" x2="3.1029" y2="3.937" layer="51"/>
<rectangle x1="2.0973" y1="2.6416" x2="2.4529" y2="3.937" layer="51"/>
<rectangle x1="1.4473" y1="2.6416" x2="1.8029" y2="3.937" layer="51"/>
<rectangle x1="0.7973" y1="2.6416" x2="1.1529" y2="3.937" layer="51"/>
<rectangle x1="0.1473" y1="2.6416" x2="0.5029" y2="3.937" layer="51"/>
<rectangle x1="-0.5029" y1="2.6416" x2="-0.1473" y2="3.937" layer="51"/>
<rectangle x1="-1.1529" y1="2.6416" x2="-0.7973" y2="3.937" layer="51"/>
<rectangle x1="-1.8029" y1="2.6416" x2="-1.4473" y2="3.937" layer="51"/>
<rectangle x1="-2.4529" y1="2.6416" x2="-2.0973" y2="3.937" layer="51"/>
<rectangle x1="-3.1029" y1="2.6416" x2="-2.7473" y2="3.937" layer="51"/>
<rectangle x1="-3.7529" y1="2.6416" x2="-3.3973" y2="3.937" layer="51"/>
<rectangle x1="-4.4028" y1="2.6416" x2="-4.0472" y2="3.937" layer="51"/>
</package>
<package name="QFN32">
<description>&lt;b&gt;QFN 32&lt;/b&gt;&lt;p&gt;
Source: http://www.ftdichip.com/Documents/DataSheets/DS_FT232R_v104.pdf</description>
<wire x1="-2.45" y1="2.45" x2="2.45" y2="2.45" width="0.1016" layer="51"/>
<wire x1="2.45" y1="2.45" x2="2.45" y2="-2.45" width="0.1016" layer="51"/>
<wire x1="2.45" y1="-2.45" x2="-2.45" y2="-2.45" width="0.1016" layer="51"/>
<wire x1="-2.45" y1="-2.45" x2="-2.45" y2="2.45" width="0.1016" layer="51"/>
<wire x1="-2.45" y1="2.05" x2="-2.45" y2="2.45" width="0.1016" layer="21"/>
<wire x1="-2.45" y1="2.45" x2="-2.05" y2="2.45" width="0.1016" layer="21"/>
<wire x1="2.05" y1="2.45" x2="2.45" y2="2.45" width="0.1016" layer="21"/>
<wire x1="2.45" y1="2.45" x2="2.45" y2="2.05" width="0.1016" layer="21"/>
<wire x1="2.45" y1="-2.05" x2="2.45" y2="-2.45" width="0.1016" layer="21"/>
<wire x1="2.45" y1="-2.45" x2="2.05" y2="-2.45" width="0.1016" layer="21"/>
<wire x1="-2.05" y1="-2.45" x2="-2.45" y2="-2.45" width="0.1016" layer="21"/>
<wire x1="-2.45" y1="-2.45" x2="-2.45" y2="-2.05" width="0.1016" layer="21"/>
<circle x="-2.175" y="2.175" radius="0.15" width="0" layer="21"/>
<smd name="EXP" x="0" y="0" dx="3.2" dy="3.2" layer="1" roundness="20" stop="no" cream="no"/>
<smd name="1" x="-2.3" y="1.75" dx="0.6" dy="0.3" layer="1" roundness="30" stop="no" cream="no"/>
<smd name="2" x="-2.3" y="1.25" dx="0.6" dy="0.3" layer="1" roundness="30" stop="no" cream="no"/>
<smd name="3" x="-2.3" y="0.75" dx="0.6" dy="0.3" layer="1" roundness="30" stop="no" cream="no"/>
<smd name="4" x="-2.3" y="0.25" dx="0.6" dy="0.3" layer="1" roundness="30" stop="no" cream="no"/>
<smd name="5" x="-2.3" y="-0.25" dx="0.6" dy="0.3" layer="1" roundness="30" stop="no" cream="no"/>
<smd name="6" x="-2.3" y="-0.75" dx="0.6" dy="0.3" layer="1" roundness="30" stop="no" cream="no"/>
<smd name="7" x="-2.3" y="-1.25" dx="0.6" dy="0.3" layer="1" roundness="30" stop="no" cream="no"/>
<smd name="8" x="-2.3" y="-1.75" dx="0.6" dy="0.3" layer="1" roundness="30" stop="no" cream="no"/>
<smd name="9" x="-1.75" y="-2.3" dx="0.6" dy="0.3" layer="1" roundness="30" rot="R90" stop="no" cream="no"/>
<smd name="10" x="-1.25" y="-2.3" dx="0.6" dy="0.3" layer="1" roundness="30" rot="R90" stop="no" cream="no"/>
<smd name="11" x="-0.75" y="-2.3" dx="0.6" dy="0.3" layer="1" roundness="30" rot="R90" stop="no" cream="no"/>
<smd name="12" x="-0.25" y="-2.3" dx="0.6" dy="0.3" layer="1" roundness="30" rot="R90" stop="no" cream="no"/>
<smd name="13" x="0.25" y="-2.3" dx="0.6" dy="0.3" layer="1" roundness="30" rot="R90" stop="no" cream="no"/>
<smd name="14" x="0.75" y="-2.3" dx="0.6" dy="0.3" layer="1" roundness="30" rot="R90" stop="no" cream="no"/>
<smd name="15" x="1.25" y="-2.3" dx="0.6" dy="0.3" layer="1" roundness="30" rot="R90" stop="no" cream="no"/>
<smd name="16" x="1.75" y="-2.3" dx="0.6" dy="0.3" layer="1" roundness="30" rot="R90" stop="no" cream="no"/>
<smd name="17" x="2.3" y="-1.75" dx="0.6" dy="0.3" layer="1" roundness="30" rot="R180" stop="no" cream="no"/>
<smd name="18" x="2.3" y="-1.25" dx="0.6" dy="0.3" layer="1" roundness="30" rot="R180" stop="no" cream="no"/>
<smd name="19" x="2.3" y="-0.75" dx="0.6" dy="0.3" layer="1" roundness="30" rot="R180" stop="no" cream="no"/>
<smd name="20" x="2.3" y="-0.25" dx="0.6" dy="0.3" layer="1" roundness="30" rot="R180" stop="no" cream="no"/>
<smd name="21" x="2.3" y="0.25" dx="0.6" dy="0.3" layer="1" roundness="30" rot="R180" stop="no" cream="no"/>
<smd name="22" x="2.3" y="0.75" dx="0.6" dy="0.3" layer="1" roundness="30" rot="R180" stop="no" cream="no"/>
<smd name="23" x="2.3" y="1.25" dx="0.6" dy="0.3" layer="1" roundness="30" rot="R180" stop="no" cream="no"/>
<smd name="24" x="2.3" y="1.75" dx="0.6" dy="0.3" layer="1" roundness="30" rot="R180" stop="no" cream="no"/>
<smd name="25" x="1.75" y="2.3" dx="0.6" dy="0.3" layer="1" roundness="30" rot="R270" stop="no" cream="no"/>
<smd name="26" x="1.25" y="2.3" dx="0.6" dy="0.3" layer="1" roundness="30" rot="R270" stop="no" cream="no"/>
<smd name="27" x="0.75" y="2.3" dx="0.6" dy="0.3" layer="1" roundness="30" rot="R270" stop="no" cream="no"/>
<smd name="28" x="0.25" y="2.3" dx="0.6" dy="0.3" layer="1" roundness="30" rot="R270" stop="no" cream="no"/>
<smd name="29" x="-0.25" y="2.3" dx="0.6" dy="0.3" layer="1" roundness="30" rot="R270" stop="no" cream="no"/>
<smd name="30" x="-0.75" y="2.3" dx="0.6" dy="0.3" layer="1" roundness="30" rot="R270" stop="no" cream="no"/>
<smd name="31" x="-1.25" y="2.3" dx="0.6" dy="0.3" layer="1" roundness="30" rot="R270" stop="no" cream="no"/>
<smd name="32" x="-1.75" y="2.3" dx="0.6" dy="0.3" layer="1" roundness="30" rot="R270" stop="no" cream="no"/>
<text x="-4.05" y="-4.35" size="1.27" layer="27">&gt;VALUE</text>
<text x="-3.8" y="3.25" size="1.27" layer="25">&gt;NAME</text>
<rectangle x1="-0.3" y1="1.1" x2="0.3" y2="1.4" layer="31"/>
<rectangle x1="-0.3" y1="0.6" x2="0.3" y2="0.9" layer="31"/>
<rectangle x1="-0.3" y1="0.1" x2="0.3" y2="0.4" layer="31"/>
<rectangle x1="-0.3" y1="-0.4" x2="0.3" y2="-0.1" layer="31"/>
<rectangle x1="-0.3" y1="-0.9" x2="0.3" y2="-0.6" layer="31"/>
<rectangle x1="-0.3" y1="-1.4" x2="0.3" y2="-1.1" layer="31"/>
<rectangle x1="-1.3" y1="1.1" x2="-0.7" y2="1.4" layer="31"/>
<rectangle x1="-1.3" y1="0.6" x2="-0.7" y2="0.9" layer="31"/>
<rectangle x1="-1.3" y1="0.1" x2="-0.7" y2="0.4" layer="31"/>
<rectangle x1="-1.3" y1="-0.4" x2="-0.7" y2="-0.1" layer="31"/>
<rectangle x1="-1.3" y1="-0.9" x2="-0.7" y2="-0.6" layer="31"/>
<rectangle x1="-1.3" y1="-1.4" x2="-0.7" y2="-1.1" layer="31"/>
<rectangle x1="0.7" y1="1.1" x2="1.3" y2="1.4" layer="31"/>
<rectangle x1="0.7" y1="0.6" x2="1.3" y2="0.9" layer="31"/>
<rectangle x1="0.7" y1="0.1" x2="1.3" y2="0.4" layer="31"/>
<rectangle x1="0.7" y1="-0.4" x2="1.3" y2="-0.1" layer="31"/>
<rectangle x1="0.7" y1="-0.9" x2="1.3" y2="-0.6" layer="31"/>
<rectangle x1="0.7" y1="-1.4" x2="1.3" y2="-1.1" layer="31"/>
<polygon width="0.5" layer="29">
<vertex x="-1.325" y="1.325"/>
<vertex x="1.325" y="1.325"/>
<vertex x="1.325" y="-1.325"/>
<vertex x="-1.325" y="-1.325"/>
</polygon>
<polygon width="0.1016" layer="29">
<vertex x="-2.55" y="1.85"/>
<vertex x="-2.1" y="1.85"/>
<vertex x="-2.05" y="1.8"/>
<vertex x="-2.05" y="1.65"/>
<vertex x="-2.55" y="1.65"/>
</polygon>
<polygon width="0.1016" layer="31">
<vertex x="-2.525" y="1.825"/>
<vertex x="-2.125" y="1.825"/>
<vertex x="-2.075" y="1.775"/>
<vertex x="-2.075" y="1.675"/>
<vertex x="-2.525" y="1.675"/>
</polygon>
<polygon width="0.1016" layer="29">
<vertex x="-2.55" y="1.35"/>
<vertex x="-2.05" y="1.35"/>
<vertex x="-2.05" y="1.15"/>
<vertex x="-2.55" y="1.15"/>
</polygon>
<polygon width="0.1016" layer="31">
<vertex x="-2.525" y="1.325"/>
<vertex x="-2.075" y="1.325"/>
<vertex x="-2.075" y="1.175"/>
<vertex x="-2.525" y="1.175"/>
</polygon>
<polygon width="0.1016" layer="29">
<vertex x="-2.55" y="0.85"/>
<vertex x="-2.05" y="0.85"/>
<vertex x="-2.05" y="0.65"/>
<vertex x="-2.55" y="0.65"/>
</polygon>
<polygon width="0.1016" layer="31">
<vertex x="-2.525" y="0.825"/>
<vertex x="-2.075" y="0.825"/>
<vertex x="-2.075" y="0.675"/>
<vertex x="-2.525" y="0.675"/>
</polygon>
<polygon width="0.1016" layer="29">
<vertex x="-2.55" y="0.35"/>
<vertex x="-2.05" y="0.35"/>
<vertex x="-2.05" y="0.15"/>
<vertex x="-2.55" y="0.15"/>
</polygon>
<polygon width="0.1016" layer="31">
<vertex x="-2.525" y="0.325"/>
<vertex x="-2.075" y="0.325"/>
<vertex x="-2.075" y="0.175"/>
<vertex x="-2.525" y="0.175"/>
</polygon>
<polygon width="0.1016" layer="29">
<vertex x="-2.55" y="-0.15"/>
<vertex x="-2.05" y="-0.15"/>
<vertex x="-2.05" y="-0.35"/>
<vertex x="-2.55" y="-0.35"/>
</polygon>
<polygon width="0.1016" layer="31">
<vertex x="-2.525" y="-0.175"/>
<vertex x="-2.075" y="-0.175"/>
<vertex x="-2.075" y="-0.325"/>
<vertex x="-2.525" y="-0.325"/>
</polygon>
<polygon width="0.1016" layer="29">
<vertex x="-2.55" y="-0.65"/>
<vertex x="-2.05" y="-0.65"/>
<vertex x="-2.05" y="-0.85"/>
<vertex x="-2.55" y="-0.85"/>
</polygon>
<polygon width="0.1016" layer="31">
<vertex x="-2.525" y="-0.675"/>
<vertex x="-2.075" y="-0.675"/>
<vertex x="-2.075" y="-0.825"/>
<vertex x="-2.525" y="-0.825"/>
</polygon>
<polygon width="0.1016" layer="29">
<vertex x="-2.55" y="-1.15"/>
<vertex x="-2.05" y="-1.15"/>
<vertex x="-2.05" y="-1.35"/>
<vertex x="-2.55" y="-1.35"/>
</polygon>
<polygon width="0.1016" layer="31">
<vertex x="-2.525" y="-1.175"/>
<vertex x="-2.075" y="-1.175"/>
<vertex x="-2.075" y="-1.325"/>
<vertex x="-2.525" y="-1.325"/>
</polygon>
<polygon width="0.1016" layer="29">
<vertex x="-2.55" y="-1.85"/>
<vertex x="-2.1" y="-1.85"/>
<vertex x="-2.05" y="-1.8"/>
<vertex x="-2.05" y="-1.65"/>
<vertex x="-2.55" y="-1.65"/>
</polygon>
<polygon width="0.1016" layer="31">
<vertex x="-2.525" y="-1.825"/>
<vertex x="-2.125" y="-1.825"/>
<vertex x="-2.075" y="-1.775"/>
<vertex x="-2.075" y="-1.675"/>
<vertex x="-2.525" y="-1.675"/>
</polygon>
<polygon width="0.1016" layer="29">
<vertex x="-1.85" y="-2.55"/>
<vertex x="-1.85" y="-2.1"/>
<vertex x="-1.8" y="-2.05"/>
<vertex x="-1.65" y="-2.05"/>
<vertex x="-1.65" y="-2.55"/>
</polygon>
<polygon width="0.1016" layer="31">
<vertex x="-1.825" y="-2.525"/>
<vertex x="-1.825" y="-2.125"/>
<vertex x="-1.775" y="-2.075"/>
<vertex x="-1.675" y="-2.075"/>
<vertex x="-1.675" y="-2.525"/>
</polygon>
<polygon width="0.1016" layer="29">
<vertex x="-1.35" y="-2.55"/>
<vertex x="-1.35" y="-2.05"/>
<vertex x="-1.15" y="-2.05"/>
<vertex x="-1.15" y="-2.55"/>
</polygon>
<polygon width="0.1016" layer="31">
<vertex x="-1.325" y="-2.525"/>
<vertex x="-1.325" y="-2.075"/>
<vertex x="-1.175" y="-2.075"/>
<vertex x="-1.175" y="-2.525"/>
</polygon>
<polygon width="0.1016" layer="29">
<vertex x="-0.85" y="-2.55"/>
<vertex x="-0.85" y="-2.05"/>
<vertex x="-0.65" y="-2.05"/>
<vertex x="-0.65" y="-2.55"/>
</polygon>
<polygon width="0.1016" layer="31">
<vertex x="-0.825" y="-2.525"/>
<vertex x="-0.825" y="-2.075"/>
<vertex x="-0.675" y="-2.075"/>
<vertex x="-0.675" y="-2.525"/>
</polygon>
<polygon width="0.1016" layer="29">
<vertex x="-0.35" y="-2.55"/>
<vertex x="-0.35" y="-2.05"/>
<vertex x="-0.15" y="-2.05"/>
<vertex x="-0.15" y="-2.55"/>
</polygon>
<polygon width="0.1016" layer="31">
<vertex x="-0.325" y="-2.525"/>
<vertex x="-0.325" y="-2.075"/>
<vertex x="-0.175" y="-2.075"/>
<vertex x="-0.175" y="-2.525"/>
</polygon>
<polygon width="0.1016" layer="29">
<vertex x="0.15" y="-2.55"/>
<vertex x="0.15" y="-2.05"/>
<vertex x="0.35" y="-2.05"/>
<vertex x="0.35" y="-2.55"/>
</polygon>
<polygon width="0.1016" layer="31">
<vertex x="0.175" y="-2.525"/>
<vertex x="0.175" y="-2.075"/>
<vertex x="0.325" y="-2.075"/>
<vertex x="0.325" y="-2.525"/>
</polygon>
<polygon width="0.1016" layer="29">
<vertex x="0.65" y="-2.55"/>
<vertex x="0.65" y="-2.05"/>
<vertex x="0.85" y="-2.05"/>
<vertex x="0.85" y="-2.55"/>
</polygon>
<polygon width="0.1016" layer="31">
<vertex x="0.675" y="-2.525"/>
<vertex x="0.675" y="-2.075"/>
<vertex x="0.825" y="-2.075"/>
<vertex x="0.825" y="-2.525"/>
</polygon>
<polygon width="0.1016" layer="29">
<vertex x="1.15" y="-2.55"/>
<vertex x="1.15" y="-2.05"/>
<vertex x="1.35" y="-2.05"/>
<vertex x="1.35" y="-2.55"/>
</polygon>
<polygon width="0.1016" layer="31">
<vertex x="1.175" y="-2.525"/>
<vertex x="1.175" y="-2.075"/>
<vertex x="1.325" y="-2.075"/>
<vertex x="1.325" y="-2.525"/>
</polygon>
<polygon width="0.1016" layer="29">
<vertex x="1.85" y="-2.55"/>
<vertex x="1.85" y="-2.1"/>
<vertex x="1.8" y="-2.05"/>
<vertex x="1.65" y="-2.05"/>
<vertex x="1.65" y="-2.55"/>
</polygon>
<polygon width="0.1016" layer="31">
<vertex x="1.825" y="-2.525"/>
<vertex x="1.825" y="-2.125"/>
<vertex x="1.775" y="-2.075"/>
<vertex x="1.675" y="-2.075"/>
<vertex x="1.675" y="-2.525"/>
</polygon>
<polygon width="0.1016" layer="29">
<vertex x="2.55" y="-1.85"/>
<vertex x="2.1" y="-1.85"/>
<vertex x="2.05" y="-1.8"/>
<vertex x="2.05" y="-1.65"/>
<vertex x="2.55" y="-1.65"/>
</polygon>
<polygon width="0.1016" layer="31">
<vertex x="2.525" y="-1.825"/>
<vertex x="2.125" y="-1.825"/>
<vertex x="2.075" y="-1.775"/>
<vertex x="2.075" y="-1.675"/>
<vertex x="2.525" y="-1.675"/>
</polygon>
<polygon width="0.1016" layer="29">
<vertex x="2.55" y="-1.35"/>
<vertex x="2.05" y="-1.35"/>
<vertex x="2.05" y="-1.15"/>
<vertex x="2.55" y="-1.15"/>
</polygon>
<polygon width="0.1016" layer="31">
<vertex x="2.525" y="-1.325"/>
<vertex x="2.075" y="-1.325"/>
<vertex x="2.075" y="-1.175"/>
<vertex x="2.525" y="-1.175"/>
</polygon>
<polygon width="0.1016" layer="29">
<vertex x="2.55" y="-0.85"/>
<vertex x="2.05" y="-0.85"/>
<vertex x="2.05" y="-0.65"/>
<vertex x="2.55" y="-0.65"/>
</polygon>
<polygon width="0.1016" layer="31">
<vertex x="2.525" y="-0.825"/>
<vertex x="2.075" y="-0.825"/>
<vertex x="2.075" y="-0.675"/>
<vertex x="2.525" y="-0.675"/>
</polygon>
<polygon width="0.1016" layer="29">
<vertex x="2.55" y="-0.35"/>
<vertex x="2.05" y="-0.35"/>
<vertex x="2.05" y="-0.15"/>
<vertex x="2.55" y="-0.15"/>
</polygon>
<polygon width="0.1016" layer="31">
<vertex x="2.525" y="-0.325"/>
<vertex x="2.075" y="-0.325"/>
<vertex x="2.075" y="-0.175"/>
<vertex x="2.525" y="-0.175"/>
</polygon>
<polygon width="0.1016" layer="29">
<vertex x="2.55" y="0.15"/>
<vertex x="2.05" y="0.15"/>
<vertex x="2.05" y="0.35"/>
<vertex x="2.55" y="0.35"/>
</polygon>
<polygon width="0.1016" layer="31">
<vertex x="2.525" y="0.175"/>
<vertex x="2.075" y="0.175"/>
<vertex x="2.075" y="0.325"/>
<vertex x="2.525" y="0.325"/>
</polygon>
<polygon width="0.1016" layer="29">
<vertex x="2.55" y="0.65"/>
<vertex x="2.05" y="0.65"/>
<vertex x="2.05" y="0.85"/>
<vertex x="2.55" y="0.85"/>
</polygon>
<polygon width="0.1016" layer="31">
<vertex x="2.525" y="0.675"/>
<vertex x="2.075" y="0.675"/>
<vertex x="2.075" y="0.825"/>
<vertex x="2.525" y="0.825"/>
</polygon>
<polygon width="0.1016" layer="29">
<vertex x="2.55" y="1.15"/>
<vertex x="2.05" y="1.15"/>
<vertex x="2.05" y="1.35"/>
<vertex x="2.55" y="1.35"/>
</polygon>
<polygon width="0.1016" layer="31">
<vertex x="2.525" y="1.175"/>
<vertex x="2.075" y="1.175"/>
<vertex x="2.075" y="1.325"/>
<vertex x="2.525" y="1.325"/>
</polygon>
<polygon width="0.1016" layer="29">
<vertex x="2.55" y="1.85"/>
<vertex x="2.1" y="1.85"/>
<vertex x="2.05" y="1.8"/>
<vertex x="2.05" y="1.65"/>
<vertex x="2.55" y="1.65"/>
</polygon>
<polygon width="0.1016" layer="31">
<vertex x="2.525" y="1.825"/>
<vertex x="2.125" y="1.825"/>
<vertex x="2.075" y="1.775"/>
<vertex x="2.075" y="1.675"/>
<vertex x="2.525" y="1.675"/>
</polygon>
<polygon width="0.1016" layer="29">
<vertex x="1.85" y="2.55"/>
<vertex x="1.85" y="2.1"/>
<vertex x="1.8" y="2.05"/>
<vertex x="1.65" y="2.05"/>
<vertex x="1.65" y="2.55"/>
</polygon>
<polygon width="0.1016" layer="31">
<vertex x="1.825" y="2.525"/>
<vertex x="1.825" y="2.125"/>
<vertex x="1.775" y="2.075"/>
<vertex x="1.675" y="2.075"/>
<vertex x="1.675" y="2.525"/>
</polygon>
<polygon width="0.1016" layer="29">
<vertex x="1.35" y="2.55"/>
<vertex x="1.35" y="2.05"/>
<vertex x="1.15" y="2.05"/>
<vertex x="1.15" y="2.55"/>
</polygon>
<polygon width="0.1016" layer="31">
<vertex x="1.325" y="2.525"/>
<vertex x="1.325" y="2.075"/>
<vertex x="1.175" y="2.075"/>
<vertex x="1.175" y="2.525"/>
</polygon>
<polygon width="0.1016" layer="29">
<vertex x="0.85" y="2.55"/>
<vertex x="0.85" y="2.05"/>
<vertex x="0.65" y="2.05"/>
<vertex x="0.65" y="2.55"/>
</polygon>
<polygon width="0.1016" layer="31">
<vertex x="0.825" y="2.525"/>
<vertex x="0.825" y="2.075"/>
<vertex x="0.675" y="2.075"/>
<vertex x="0.675" y="2.525"/>
</polygon>
<polygon width="0.1016" layer="29">
<vertex x="0.35" y="2.55"/>
<vertex x="0.35" y="2.05"/>
<vertex x="0.15" y="2.05"/>
<vertex x="0.15" y="2.55"/>
</polygon>
<polygon width="0.1016" layer="31">
<vertex x="0.325" y="2.525"/>
<vertex x="0.325" y="2.075"/>
<vertex x="0.175" y="2.075"/>
<vertex x="0.175" y="2.525"/>
</polygon>
<polygon width="0.1016" layer="29">
<vertex x="-0.15" y="2.55"/>
<vertex x="-0.15" y="2.05"/>
<vertex x="-0.35" y="2.05"/>
<vertex x="-0.35" y="2.55"/>
</polygon>
<polygon width="0.1016" layer="31">
<vertex x="-0.175" y="2.525"/>
<vertex x="-0.175" y="2.075"/>
<vertex x="-0.325" y="2.075"/>
<vertex x="-0.325" y="2.525"/>
</polygon>
<polygon width="0.1016" layer="29">
<vertex x="-0.65" y="2.55"/>
<vertex x="-0.65" y="2.05"/>
<vertex x="-0.85" y="2.05"/>
<vertex x="-0.85" y="2.55"/>
</polygon>
<polygon width="0.1016" layer="31">
<vertex x="-0.675" y="2.525"/>
<vertex x="-0.675" y="2.075"/>
<vertex x="-0.825" y="2.075"/>
<vertex x="-0.825" y="2.525"/>
</polygon>
<polygon width="0.1016" layer="29">
<vertex x="-1.15" y="2.55"/>
<vertex x="-1.15" y="2.05"/>
<vertex x="-1.35" y="2.05"/>
<vertex x="-1.35" y="2.55"/>
</polygon>
<polygon width="0.1016" layer="31">
<vertex x="-1.175" y="2.525"/>
<vertex x="-1.175" y="2.075"/>
<vertex x="-1.325" y="2.075"/>
<vertex x="-1.325" y="2.525"/>
</polygon>
<polygon width="0.1016" layer="29">
<vertex x="-1.85" y="2.55"/>
<vertex x="-1.85" y="2.1"/>
<vertex x="-1.8" y="2.05"/>
<vertex x="-1.65" y="2.05"/>
<vertex x="-1.65" y="2.55"/>
</polygon>
<polygon width="0.1016" layer="31">
<vertex x="-1.825" y="2.525"/>
<vertex x="-1.825" y="2.125"/>
<vertex x="-1.775" y="2.075"/>
<vertex x="-1.675" y="2.075"/>
<vertex x="-1.675" y="2.525"/>
</polygon>
</package>
</packages>
<symbols>
<symbol name="FT232R">
<wire x1="-10.16" y1="25.4" x2="12.7" y2="25.4" width="0.254" layer="94"/>
<wire x1="12.7" y1="25.4" x2="12.7" y2="-27.94" width="0.254" layer="94"/>
<wire x1="12.7" y1="-27.94" x2="-10.16" y2="-27.94" width="0.254" layer="94"/>
<wire x1="-10.16" y1="-27.94" x2="-10.16" y2="25.4" width="0.254" layer="94"/>
<text x="-10.16" y="26.67" size="1.778" layer="95">&gt;NAME</text>
<text x="-10.16" y="-30.48" size="1.778" layer="96">&gt;VALUE</text>
<pin name="VCC" x="-12.7" y="22.86" length="short" direction="pwr"/>
<pin name="3V3OUT" x="-12.7" y="-5.08" length="short" direction="out"/>
<pin name="USBDP" x="-12.7" y="-10.16" length="short"/>
<pin name="USBDM" x="-12.7" y="-12.7" length="short"/>
<pin name="OSCO" x="-12.7" y="7.62" length="short" direction="out"/>
<pin name="OSCI" x="-12.7" y="10.16" length="short" direction="in"/>
<pin name="GND" x="15.24" y="-20.32" length="short" direction="pwr" rot="R180"/>
<pin name="TXD" x="15.24" y="22.86" length="short" direction="out" rot="R180"/>
<pin name="RXD" x="15.24" y="20.32" length="short" direction="in" rot="R180"/>
<pin name="!RTS" x="15.24" y="17.78" length="short" direction="out" rot="R180"/>
<pin name="!CTS" x="15.24" y="15.24" length="short" direction="in" rot="R180"/>
<pin name="!DTR" x="15.24" y="12.7" length="short" direction="out" rot="R180"/>
<pin name="!DSR" x="15.24" y="10.16" length="short" direction="in" rot="R180"/>
<pin name="!DCD" x="15.24" y="7.62" length="short" direction="in" rot="R180"/>
<pin name="!RI" x="15.24" y="5.08" length="short" direction="in" rot="R180"/>
<pin name="CBUS0" x="15.24" y="0" length="short" rot="R180"/>
<pin name="CBUS1" x="15.24" y="-2.54" length="short" rot="R180"/>
<pin name="CBUS2" x="15.24" y="-5.08" length="short" rot="R180"/>
<pin name="CBUS3" x="15.24" y="-7.62" length="short" rot="R180"/>
<pin name="CBUS4" x="15.24" y="-10.16" length="short" rot="R180"/>
<pin name="VCCIO" x="-12.7" y="20.32" length="short" direction="pwr"/>
<pin name="!RESET" x="-12.7" y="15.24" length="short" direction="in"/>
<pin name="GND@A" x="-12.7" y="-17.78" length="short" direction="pwr"/>
<pin name="GND@1" x="15.24" y="-22.86" length="short" direction="pwr" rot="R180"/>
<pin name="TEST" x="15.24" y="-15.24" length="short" direction="in" rot="R180"/>
<pin name="GND@2" x="15.24" y="-25.4" length="short" direction="pwr" rot="R180"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="FT232R" prefix="IC">
<description>Source: http://www.ftdichip.com/Documents/DataSheets/DS_FT232R_v104.pdf</description>
<gates>
<gate name="1" symbol="FT232R" x="0" y="0"/>
</gates>
<devices>
<device name="L" package="SSOP28">
<connects>
<connect gate="1" pin="!CTS" pad="11"/>
<connect gate="1" pin="!DCD" pad="10"/>
<connect gate="1" pin="!DSR" pad="9"/>
<connect gate="1" pin="!DTR" pad="2"/>
<connect gate="1" pin="!RESET" pad="19"/>
<connect gate="1" pin="!RI" pad="6"/>
<connect gate="1" pin="!RTS" pad="3"/>
<connect gate="1" pin="3V3OUT" pad="17"/>
<connect gate="1" pin="CBUS0" pad="23"/>
<connect gate="1" pin="CBUS1" pad="22"/>
<connect gate="1" pin="CBUS2" pad="13"/>
<connect gate="1" pin="CBUS3" pad="14"/>
<connect gate="1" pin="CBUS4" pad="12"/>
<connect gate="1" pin="GND" pad="7"/>
<connect gate="1" pin="GND@1" pad="18"/>
<connect gate="1" pin="GND@2" pad="21"/>
<connect gate="1" pin="GND@A" pad="25"/>
<connect gate="1" pin="OSCI" pad="27"/>
<connect gate="1" pin="OSCO" pad="28"/>
<connect gate="1" pin="RXD" pad="5"/>
<connect gate="1" pin="TEST" pad="26"/>
<connect gate="1" pin="TXD" pad="1"/>
<connect gate="1" pin="USBDM" pad="16"/>
<connect gate="1" pin="USBDP" pad="15"/>
<connect gate="1" pin="VCC" pad="20"/>
<connect gate="1" pin="VCCIO" pad="4"/>
</connects>
<technologies>
<technology name="">
<attribute name="MF" value="" constant="no"/>
<attribute name="MPN" value="FT232RL" constant="no"/>
<attribute name="OC_FARNELL" value="1146032" constant="no"/>
<attribute name="OC_NEWARK" value="91K9918" constant="no"/>
</technology>
</technologies>
</device>
<device name="Q" package="QFN32">
<connects>
<connect gate="1" pin="!CTS" pad="8"/>
<connect gate="1" pin="!DCD" pad="7"/>
<connect gate="1" pin="!DSR" pad="6"/>
<connect gate="1" pin="!DTR" pad="31"/>
<connect gate="1" pin="!RESET" pad="18"/>
<connect gate="1" pin="!RI" pad="3"/>
<connect gate="1" pin="!RTS" pad="32"/>
<connect gate="1" pin="3V3OUT" pad="16"/>
<connect gate="1" pin="CBUS0" pad="22"/>
<connect gate="1" pin="CBUS1" pad="21"/>
<connect gate="1" pin="CBUS2" pad="10"/>
<connect gate="1" pin="CBUS3" pad="11"/>
<connect gate="1" pin="CBUS4" pad="9"/>
<connect gate="1" pin="GND" pad="4"/>
<connect gate="1" pin="GND@1" pad="17"/>
<connect gate="1" pin="GND@2" pad="20"/>
<connect gate="1" pin="GND@A" pad="24"/>
<connect gate="1" pin="OSCI" pad="27"/>
<connect gate="1" pin="OSCO" pad="28"/>
<connect gate="1" pin="RXD" pad="2"/>
<connect gate="1" pin="TEST" pad="26"/>
<connect gate="1" pin="TXD" pad="30"/>
<connect gate="1" pin="USBDM" pad="15"/>
<connect gate="1" pin="USBDP" pad="14"/>
<connect gate="1" pin="VCC" pad="19"/>
<connect gate="1" pin="VCCIO" pad="1"/>
</connects>
<technologies>
<technology name="">
<attribute name="MF" value="" constant="no"/>
<attribute name="MPN" value="FT232RQ" constant="no"/>
<attribute name="OC_FARNELL" value="1146033" constant="no"/>
<attribute name="OC_NEWARK" value="91K9919" constant="no"/>
</technology>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="SparkFun-Sensors">
<description>&lt;h3&gt;SparkFun Electronics' preferred foot prints&lt;/h3&gt;
In this library you'll find sensors- accelerometers, gyros, compasses, magnetometers, light sensors, imagers, temp sensors, etc.&lt;br&gt;&lt;br&gt;
We've spent an enormous amount of time creating and checking these footprints and parts, but it is the end user's responsibility to ensure correctness and suitablity for a given componet or application. If you enjoy using this library, please buy one of our products at www.sparkfun.com.
&lt;br&gt;&lt;br&gt;
&lt;b&gt;Licensing:&lt;/b&gt; Creative Commons ShareAlike 4.0 International - https://creativecommons.org/licenses/by-sa/4.0/ 
&lt;br&gt;&lt;br&gt;
You are welcome to use this library for commercial purposes. For attribution, we ask that when you begin to sell your device using our footprint, you email us with a link to the product being sold. We want bragging rights that we helped (in a very small part) to create your 8th world wonder. We would like the opportunity to feature your device on our homepage.</description>
<packages>
<package name="LGA8">
<description>&lt;h4&gt;LGA8 Package&lt;/h4&gt;
&lt;ul&gt;&lt;li&gt; 5.0x3.0x1.2mm&lt;/li&gt;
&lt;li&gt;8-pad&lt;/li&gt;&lt;br&gt;&lt;/ul&gt;
Used in MPL115A1 -</description>
<wire x1="-1.5" y1="-2.5" x2="1.5" y2="-2.5" width="0.2032" layer="21"/>
<wire x1="1.5" y1="2.5" x2="-1.5" y2="2.5" width="0.2032" layer="21"/>
<wire x1="-1.5" y1="1.1" x2="-1.5" y2="1.4" width="0.2032" layer="21"/>
<wire x1="-1.5" y1="2.5" x2="-1.5" y2="2.4" width="0.2032" layer="21"/>
<wire x1="1.5" y1="2.5" x2="1.5" y2="2.4" width="0.2032" layer="21"/>
<wire x1="1.5" y1="1.1" x2="1.5" y2="1.4" width="0.2032" layer="21"/>
<wire x1="-1.5" y1="-0.1" x2="-1.5" y2="0.1" width="0.2032" layer="21"/>
<wire x1="-1.5" y1="-1.4" x2="-1.5" y2="-1.1" width="0.2032" layer="21"/>
<wire x1="-1.5" y1="-2.4" x2="-1.5" y2="-2.5" width="0.2032" layer="21"/>
<wire x1="1.5" y1="-0.1" x2="1.5" y2="0.1" width="0.2032" layer="21"/>
<wire x1="1.5" y1="-1.4" x2="1.5" y2="-1.1" width="0.2032" layer="21"/>
<wire x1="1.5" y1="-2.4" x2="1.5" y2="-2.5" width="0.2032" layer="21"/>
<circle x="0" y="-1.025" radius="0.7071" width="0.127" layer="51"/>
<smd name="1" x="-1.2" y="1.875" dx="1.2" dy="0.5" layer="1"/>
<smd name="2" x="-1.2" y="0.625" dx="1.2" dy="0.5" layer="1"/>
<smd name="3" x="-1.2" y="-0.625" dx="1.2" dy="0.5" layer="1"/>
<smd name="4" x="-1.2" y="-1.875" dx="1.2" dy="0.5" layer="1"/>
<smd name="8" x="1.2" y="1.875" dx="1.2" dy="0.5" layer="1"/>
<smd name="7" x="1.2" y="0.625" dx="1.2" dy="0.5" layer="1"/>
<smd name="6" x="1.2" y="-0.625" dx="1.2" dy="0.5" layer="1"/>
<smd name="5" x="1.2" y="-1.875" dx="1.2" dy="0.5" layer="1"/>
<text x="-1.524" y="2.667" size="0.4064" layer="25" font="vector" ratio="15">&gt;Name</text>
<text x="-1.524" y="-3.048" size="0.4064" layer="27" font="vector" ratio="15">&gt;Value</text>
<circle x="-2.0574" y="2.4384" radius="0.160640625" width="0.127" layer="21"/>
</package>
<package name="16LPCC">
<wire x1="-1.7" y1="-1.7" x2="-1.7" y2="-1.1" width="0.2032" layer="21"/>
<wire x1="-1.7" y1="-1.7" x2="-1.1" y2="-1.7" width="0.2032" layer="21"/>
<wire x1="1.1" y1="-1.7" x2="1.7" y2="-1.7" width="0.2032" layer="21"/>
<wire x1="1.7" y1="-1.7" x2="1.7" y2="-1.1" width="0.2032" layer="21"/>
<wire x1="1.7" y1="1.1" x2="1.7" y2="1.7" width="0.2032" layer="21"/>
<wire x1="1.7" y1="1.7" x2="1.1" y2="1.7" width="0.2032" layer="21"/>
<wire x1="-1.28" y1="1.635" x2="-1.635" y2="1.26" width="0.2032" layer="21"/>
<smd name="4" x="-1.37" y="-0.75" dx="0.65" dy="0.28" layer="1"/>
<smd name="3" x="-1.37" y="-0.25" dx="0.65" dy="0.28" layer="1"/>
<smd name="2" x="-1.37" y="0.25" dx="0.65" dy="0.28" layer="1"/>
<smd name="1" x="-1.37" y="0.75" dx="0.65" dy="0.28" layer="1"/>
<smd name="5" x="-0.75" y="-1.37" dx="0.65" dy="0.28" layer="1" rot="R270"/>
<smd name="15" x="-0.25" y="1.37" dx="0.65" dy="0.28" layer="1" rot="R90"/>
<smd name="14" x="0.25" y="1.37" dx="0.65" dy="0.28" layer="1" rot="R90"/>
<smd name="13" x="0.75" y="1.37" dx="0.65" dy="0.28" layer="1" rot="R90"/>
<smd name="12" x="1.37" y="0.75" dx="0.65" dy="0.28" layer="1"/>
<smd name="11" x="1.37" y="0.25" dx="0.65" dy="0.28" layer="1"/>
<smd name="10" x="1.37" y="-0.25" dx="0.65" dy="0.28" layer="1"/>
<smd name="9" x="1.37" y="-0.75" dx="0.65" dy="0.28" layer="1"/>
<smd name="6" x="-0.25" y="-1.37" dx="0.65" dy="0.28" layer="1" rot="R90"/>
<smd name="8" x="0.75" y="-1.37" dx="0.65" dy="0.28" layer="1" rot="R90"/>
<smd name="16" x="-0.75" y="1.37" dx="0.65" dy="0.28" layer="1" rot="R90"/>
<smd name="7" x="0.25" y="-1.37" dx="0.65" dy="0.28" layer="1" rot="R90"/>
<text x="-1.27" y="1.905" size="0.6096" layer="25">&gt;NAME</text>
<text x="-1.905" y="-2.54" size="0.6096" layer="27">&gt;VALUE</text>
</package>
</packages>
<symbols>
<symbol name="MPL3115A2">
<wire x1="-10.16" y1="-7.62" x2="10.16" y2="-7.62" width="0.254" layer="94"/>
<wire x1="10.16" y1="-7.62" x2="10.16" y2="5.08" width="0.254" layer="94"/>
<wire x1="10.16" y1="5.08" x2="-10.16" y2="5.08" width="0.254" layer="94"/>
<wire x1="-10.16" y1="5.08" x2="-10.16" y2="-7.62" width="0.254" layer="94"/>
<text x="-10.16" y="5.08" size="1.778" layer="95">&gt;NAME</text>
<text x="-10.16" y="-10.16" size="1.778" layer="96">&gt;VALUE</text>
<pin name="VDD" x="-12.7" y="2.54" visible="pin" length="short" direction="pwr"/>
<pin name="CAP" x="-12.7" y="0" visible="pin" length="short" direction="pwr"/>
<pin name="GND" x="-12.7" y="-2.54" visible="pin" length="short" direction="pwr"/>
<pin name="VDDIO" x="-12.7" y="-5.08" visible="pin" length="short" direction="pwr"/>
<pin name="SCL" x="12.7" y="2.54" visible="pin" length="short" rot="R180"/>
<pin name="SDA" x="12.7" y="0" visible="pin" length="short" rot="R180"/>
<pin name="INT1" x="12.7" y="-2.54" visible="pin" length="short" direction="out" rot="R180"/>
<pin name="INT2" x="12.7" y="-5.08" visible="pin" length="short" direction="out" rot="R180"/>
</symbol>
<symbol name="HMC1052L">
<wire x1="-2.54" y1="-2.54" x2="-2.54" y2="25.4" width="0.254" layer="94"/>
<wire x1="-2.54" y1="25.4" x2="25.4" y2="25.4" width="0.254" layer="94"/>
<wire x1="25.4" y1="25.4" x2="25.4" y2="-2.54" width="0.254" layer="94"/>
<wire x1="25.4" y1="-2.54" x2="-2.54" y2="-2.54" width="0.254" layer="94"/>
<pin name="GND2A" x="-7.62" y="15.24" length="middle"/>
<pin name="OFF+" x="-7.62" y="12.7" length="middle"/>
<pin name="OUT+A" x="-7.62" y="10.16" length="middle"/>
<pin name="VB" x="-7.62" y="7.62" length="middle"/>
<pin name="NC" x="7.62" y="-7.62" length="middle" rot="R90"/>
<pin name="OFF-" x="10.16" y="-7.62" length="middle" rot="R90"/>
<pin name="GND2B" x="12.7" y="-7.62" length="middle" rot="R90"/>
<pin name="S/R+" x="15.24" y="-7.62" length="middle" rot="R90"/>
<pin name="NC2" x="30.48" y="7.62" length="middle" rot="R180"/>
<pin name="OUT-B" x="30.48" y="10.16" length="middle" rot="R180"/>
<pin name="S/R-" x="30.48" y="12.7" length="middle" rot="R180"/>
<pin name="NC3" x="30.48" y="15.24" length="middle" rot="R180"/>
<pin name="GND1A" x="15.24" y="30.48" length="middle" rot="R270"/>
<pin name="OUT-A" x="12.7" y="30.48" length="middle" rot="R270"/>
<pin name="GND1B" x="10.16" y="30.48" length="middle" rot="R270"/>
<pin name="OUT+B" x="7.62" y="30.48" length="middle" rot="R270"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="MPL3115A2" prefix="U">
<description>Altimeter/Pressure Sensor, I2C, 1.95V-3.6V supply, 50 to 110kPa</description>
<gates>
<gate name="G$1" symbol="MPL3115A2" x="0" y="0"/>
</gates>
<devices>
<device name="LGA8" package="LGA8">
<connects>
<connect gate="G$1" pin="CAP" pad="2"/>
<connect gate="G$1" pin="GND" pad="3"/>
<connect gate="G$1" pin="INT1" pad="6"/>
<connect gate="G$1" pin="INT2" pad="5"/>
<connect gate="G$1" pin="SCL" pad="8"/>
<connect gate="G$1" pin="SDA" pad="7"/>
<connect gate="G$1" pin="VDD" pad="1"/>
<connect gate="G$1" pin="VDDIO" pad="4"/>
</connects>
<technologies>
<technology name="">
<attribute name="PROD_ID" value="IC-10869" constant="no"/>
</technology>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="HMC1052L">
<description>&lt;b&gt;HMC1052L&lt;/b&gt;
Dual axis magnetic sensor. Used for compass and magnetometer applications. Schematic and footprint are not tested and probably need fixing. Spark Fun Electronics SKU : COM-00708</description>
<gates>
<gate name="G$1" symbol="HMC1052L" x="-10.16" y="-10.16"/>
</gates>
<devices>
<device name="" package="16LPCC">
<connects>
<connect gate="G$1" pin="GND1A" pad="13"/>
<connect gate="G$1" pin="GND1B" pad="15"/>
<connect gate="G$1" pin="GND2A" pad="1"/>
<connect gate="G$1" pin="GND2B" pad="7"/>
<connect gate="G$1" pin="NC" pad="5"/>
<connect gate="G$1" pin="NC2" pad="9"/>
<connect gate="G$1" pin="NC3" pad="12"/>
<connect gate="G$1" pin="OFF+" pad="2"/>
<connect gate="G$1" pin="OFF-" pad="6"/>
<connect gate="G$1" pin="OUT+A" pad="3"/>
<connect gate="G$1" pin="OUT+B" pad="16"/>
<connect gate="G$1" pin="OUT-A" pad="14"/>
<connect gate="G$1" pin="OUT-B" pad="10"/>
<connect gate="G$1" pin="S/R+" pad="8"/>
<connect gate="G$1" pin="S/R-" pad="11"/>
<connect gate="G$1" pin="VB" pad="4"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="Ava">
<packages>
<package name="UBLOX_LEA-6T">
<smd name="15" x="-8.4" y="8.35" dx="1.8" dy="0.8" layer="1"/>
<wire x1="8.5" y1="11.2" x2="-8.5" y2="11.2" width="0.127" layer="21"/>
<wire x1="8.5" y1="-11.2" x2="-8.5" y2="-11.2" width="0.127" layer="21"/>
<smd name="16" x="-8.4" y="7.25" dx="1.8" dy="0.8" layer="1"/>
<smd name="17" x="-8.4" y="6.15" dx="1.8" dy="0.8" layer="1"/>
<smd name="18" x="-8.4" y="5.05" dx="1.8" dy="0.8" layer="1"/>
<smd name="19" x="-8.4" y="3.95" dx="1.8" dy="0.8" layer="1"/>
<smd name="20" x="-8.4" y="2.85" dx="1.8" dy="0.8" layer="1"/>
<smd name="21" x="-8.4" y="-0.95" dx="1.8" dy="0.8" layer="1"/>
<smd name="22" x="-8.4" y="-2.05" dx="1.8" dy="0.8" layer="1"/>
<smd name="23" x="-8.4" y="-3.15" dx="1.8" dy="0.8" layer="1"/>
<smd name="24" x="-8.4" y="-4.25" dx="1.8" dy="0.8" layer="1"/>
<smd name="25" x="-8.4" y="-5.35" dx="1.8" dy="0.8" layer="1"/>
<smd name="26" x="-8.4" y="-6.45" dx="1.8" dy="0.8" layer="1"/>
<smd name="27" x="-8.4" y="-7.55" dx="1.8" dy="0.8" layer="1"/>
<smd name="28" x="-8.4" y="-8.65" dx="1.8" dy="0.8" layer="1"/>
<smd name="14" x="8.4" y="8.35" dx="1.8" dy="0.8" layer="1"/>
<smd name="13" x="8.4" y="7.25" dx="1.8" dy="0.8" layer="1"/>
<smd name="12" x="8.4" y="6.15" dx="1.8" dy="0.8" layer="1"/>
<smd name="11" x="8.4" y="5.05" dx="1.8" dy="0.8" layer="1"/>
<smd name="10" x="8.4" y="3.95" dx="1.8" dy="0.8" layer="1"/>
<smd name="9" x="8.4" y="2.85" dx="1.8" dy="0.8" layer="1"/>
<smd name="8" x="8.4" y="-0.95" dx="1.8" dy="0.8" layer="1"/>
<smd name="7" x="8.4" y="-2.05" dx="1.8" dy="0.8" layer="1"/>
<smd name="6" x="8.4" y="-3.15" dx="1.8" dy="0.8" layer="1"/>
<smd name="5" x="8.4" y="-4.25" dx="1.8" dy="0.8" layer="1"/>
<smd name="4" x="8.4" y="-5.35" dx="1.8" dy="0.8" layer="1"/>
<smd name="3" x="8.4" y="-6.45" dx="1.8" dy="0.8" layer="1"/>
<smd name="2" x="8.4" y="-7.55" dx="1.8" dy="0.8" layer="1"/>
<smd name="1" x="8.4" y="-8.65" dx="1.8" dy="0.8" layer="1"/>
<circle x="5.3975" y="-8.5725" radius="0.898025" width="0" layer="21"/>
</package>
</packages>
<symbols>
<symbol name="UBLOX_LEA-6">
<wire x1="-30.48" y1="22.86" x2="30.48" y2="22.86" width="0.254" layer="94"/>
<wire x1="30.48" y1="22.86" x2="30.48" y2="-25.4" width="0.254" layer="94"/>
<wire x1="30.48" y1="-25.4" x2="-30.48" y2="-25.4" width="0.254" layer="94"/>
<wire x1="-30.48" y1="-25.4" x2="-30.48" y2="22.86" width="0.254" layer="94"/>
<pin name="15_GND" x="-35.56" y="17.78" visible="pin" length="middle"/>
<pin name="16_RF_IN" x="-35.56" y="15.24" length="middle"/>
<pin name="17_GND" x="-35.56" y="12.7" length="middle"/>
<pin name="18_VCC_RF" x="-35.56" y="10.16" length="middle"/>
<pin name="19_V_ANT" x="-35.56" y="7.62" length="middle"/>
<pin name="20_AADET" x="-35.56" y="5.08" length="middle"/>
<pin name="28_TIMEPULSE" x="-35.56" y="-20.32" length="middle"/>
<pin name="27_EXTINT0/SPEED" x="-35.56" y="-17.78" length="middle"/>
<pin name="26_USB_DP" x="-35.56" y="-15.24" length="middle"/>
<pin name="25_USB_DM" x="-35.56" y="-12.7" length="middle"/>
<pin name="24_VDDUSB" x="-35.56" y="-10.16" length="middle"/>
<pin name="23_SPI_SCK/RSVD" x="-35.56" y="-7.62" length="middle"/>
<pin name="22_SPI_SCS1/RSVD" x="-35.56" y="-5.08" length="middle"/>
<pin name="21_FWD/RSVD" x="-35.56" y="-2.54" length="middle"/>
<pin name="14_GND" x="35.56" y="17.78" length="middle" rot="R180"/>
<pin name="13_GND" x="35.56" y="15.24" length="middle" rot="R180"/>
<pin name="12_RSVD" x="35.56" y="12.7" length="middle" rot="R180"/>
<pin name="11_V_BCKUP" x="35.56" y="10.16" length="middle" rot="R180"/>
<pin name="10_RESET" x="35.56" y="7.62" length="middle" rot="R180"/>
<pin name="9_CFG_COM1/TIMEPULSE2" x="35.56" y="5.08" length="middle" rot="R180"/>
<pin name="1_SDA2/SPI_MOSI" x="35.56" y="-20.32" length="middle" rot="R180"/>
<pin name="2_SCL2/SPI_MISO" x="35.56" y="-17.78" length="middle" rot="R180"/>
<pin name="3_TXD1" x="35.56" y="-15.24" length="middle" rot="R180"/>
<pin name="4_RXD1" x="35.56" y="-12.7" length="middle" rot="R180"/>
<pin name="5_NC" x="35.56" y="-10.16" length="middle" rot="R180"/>
<pin name="6_VCC" x="35.56" y="-7.62" length="middle" rot="R180"/>
<pin name="7_GND" x="35.56" y="-5.08" length="middle" rot="R180"/>
<pin name="8_VCC_OUT" x="35.56" y="-2.54" length="middle" rot="R180"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="UBLOX_LEA-6">
<gates>
<gate name="G$1" symbol="UBLOX_LEA-6" x="0" y="0"/>
</gates>
<devices>
<device name="" package="UBLOX_LEA-6T">
<connects>
<connect gate="G$1" pin="10_RESET" pad="10"/>
<connect gate="G$1" pin="11_V_BCKUP" pad="11"/>
<connect gate="G$1" pin="12_RSVD" pad="12"/>
<connect gate="G$1" pin="13_GND" pad="13"/>
<connect gate="G$1" pin="14_GND" pad="14"/>
<connect gate="G$1" pin="15_GND" pad="15"/>
<connect gate="G$1" pin="16_RF_IN" pad="16"/>
<connect gate="G$1" pin="17_GND" pad="17"/>
<connect gate="G$1" pin="18_VCC_RF" pad="18"/>
<connect gate="G$1" pin="19_V_ANT" pad="19"/>
<connect gate="G$1" pin="1_SDA2/SPI_MOSI" pad="1"/>
<connect gate="G$1" pin="20_AADET" pad="20"/>
<connect gate="G$1" pin="21_FWD/RSVD" pad="21"/>
<connect gate="G$1" pin="22_SPI_SCS1/RSVD" pad="22"/>
<connect gate="G$1" pin="23_SPI_SCK/RSVD" pad="23"/>
<connect gate="G$1" pin="24_VDDUSB" pad="24"/>
<connect gate="G$1" pin="25_USB_DM" pad="25"/>
<connect gate="G$1" pin="26_USB_DP" pad="26"/>
<connect gate="G$1" pin="27_EXTINT0/SPEED" pad="27"/>
<connect gate="G$1" pin="28_TIMEPULSE" pad="28"/>
<connect gate="G$1" pin="2_SCL2/SPI_MISO" pad="2"/>
<connect gate="G$1" pin="3_TXD1" pad="3"/>
<connect gate="G$1" pin="4_RXD1" pad="4"/>
<connect gate="G$1" pin="5_NC" pad="5"/>
<connect gate="G$1" pin="6_VCC" pad="6"/>
<connect gate="G$1" pin="7_GND" pad="7"/>
<connect gate="G$1" pin="8_VCC_OUT" pad="8"/>
<connect gate="G$1" pin="9_CFG_COM1/TIMEPULSE2" pad="9"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="wuerth_elektronik_eisos__v6.0">
<description>&lt;BR&gt;WE eiSos -- EMC &amp; Inductive Solutions&lt;br&gt;&lt;Hr&gt;
&lt;BR&gt;&lt;BR&gt; 
&lt;TABLE BORDER=0 CELLSPACING=0 CELLPADDING=0&gt;
&lt;TR&gt;   
&lt;TD BGCOLOR="#cccccc" ALIGN=CENTER&gt;&lt;FONT FACE=ARIAL SIZE=3&gt;&lt;BR&gt;&lt;br&gt;
      &amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp; &amp;nbsp;&lt;BR&gt;
       &lt;BR&gt;
       &lt;BR&gt;
       &lt;BR&gt;&lt;BR&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
&lt;TD BGCOLOR="#cccccc" ALIGN=CENTER&gt;&lt;FONT FACE=ARIAL SIZE=3&gt;&lt;br&gt;
      -----&lt;BR&gt;
      -----&lt;BR&gt;
      -----&lt;BR&gt;
      -----&lt;BR&gt;
      -----&lt;BR&gt;&lt;BR&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD BGCOLOR="#cccccc" ALIGN=CENTER&gt; &lt;FONT FACE=ARIAL SIZE=3&gt;&lt;br&gt;
      ---------------------------&lt;BR&gt;
&lt;B&gt;&lt;I&gt;&lt;span style='font-size:26pt;
  color:#FF6600;'&gt;WE &lt;/span&gt;&lt;/i&gt;&lt;/b&gt;
&lt;BR&gt;
      ---------------------------&lt;BR&gt;&lt;b&gt;Würth Elektronik&lt;/b&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD BGCOLOR="#cccccc" ALIGN=CENTER&gt;&lt;FONT FACE=ARIAL SIZE=3&gt;&lt;br&gt;
      ---------O---&lt;BR&gt;
      ----O--------&lt;BR&gt;
      ---------O---&lt;BR&gt;
      ----O--------&lt;BR&gt;
      ---------O---&lt;BR&gt;&lt;BR&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
   
&lt;TD BGCOLOR="#cccccc" ALIGN=CENTER&gt;&lt;FONT FACE=ARIAL SIZE=3&gt;&lt;BR&gt;
      &amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp; &amp;nbsp;&lt;BR&gt;
       &lt;BR&gt;
       &lt;BR&gt;
       &lt;BR&gt;
       &lt;BR&gt;&lt;BR&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
  &lt;/TR&gt;

  &lt;TR&gt;
    &lt;TD COLSPAN=7&gt;&amp;nbsp;
    &lt;/TD&gt;
  &lt;/TR&gt;
  
&lt;/TABLE&gt;
&lt;B&gt;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;More than you expect&lt;BR&gt;&lt;BR&gt;&lt;BR&gt;&lt;/B&gt;

&lt;HR&gt;&lt;BR&gt;
&lt;b&gt;Würth Elektronik eiSos GmbH &amp; Co. KG&lt;/b&gt;&lt;br&gt;
EMC &amp; Inductive Solutions&lt;br&gt;

Max-Eyth-Str.1&lt;br&gt;
D-74638 Waldenburg&lt;br&gt;
&lt;br&gt;
Tel: +49 (0)7942-945-0&lt;br&gt;
Fax:+49 (0)7942-945-405&lt;br&gt;
&lt;br&gt;
&lt;a href="http://www.we-online.com"&gt;http://www.we-online.com&lt;/a&gt;&lt;br&gt;
&lt;a href="mailto:pm.hotline@we-online.de"&gt;pm.hotline@we-online.de&lt;/a&gt; &lt;BR&gt;&lt;BR&gt;
&lt;br&gt;&lt;HR&gt;&lt;BR&gt;
Neither CadSoft nor WE-eiSos does warrant that this library is error-free or &lt;br&gt;
that it meets your specific requirements.&lt;br&gt;&lt;BR&gt;
Please contact us for more information.&lt;br&gt;&lt;BR&gt;&lt;br&gt;
&lt;hr&gt;
Version 6.0,   March 17-th 2009
&lt;HR&gt;
Copyright: Würth Elektronik eiSos</description>
<packages>
<package name="WE-MCA_7488105XX/7488940245">
<description>Multiplayer Chip Antenna WE-MCA</description>
<wire x1="0" y1="2.3" x2="0" y2="1.3" width="0.127" layer="21"/>
<wire x1="-1" y1="3.5" x2="1" y2="3.5" width="0.127" layer="21"/>
<wire x1="-1" y1="-3.5" x2="1" y2="-3.5" width="0.127" layer="21"/>
<wire x1="-1" y1="3.5" x2="-1" y2="-3.5" width="0.127" layer="21"/>
<wire x1="1" y1="3.5" x2="1" y2="-3.5" width="0.127" layer="21"/>
<smd name="2" x="0" y="-3.4" dx="1.25" dy="2" layer="1" rot="R90"/>
<smd name="1" x="0" y="3.4" dx="1.25" dy="2" layer="1" rot="R90"/>
<text x="-3" y="4.7" size="1.27" layer="25">&gt;Name</text>
<text x="-2.7" y="-5.6" size="1.27" layer="27">&gt;Value</text>
</package>
<package name="WE-MCA_7488910245">
<description>Multiplayer Chip Antenna WE-MCA</description>
<wire x1="0" y1="3.3" x2="0" y2="2.3" width="0.127" layer="21"/>
<wire x1="-1" y1="4.75" x2="1" y2="4.75" width="0.127" layer="21"/>
<wire x1="-1" y1="-4.75" x2="1" y2="-4.75" width="0.127" layer="21"/>
<wire x1="1" y1="4.75" x2="1" y2="-4.75" width="0.127" layer="21"/>
<wire x1="-1" y1="-4.75" x2="-1" y2="4.75" width="0.127" layer="21"/>
<smd name="1" x="0" y="4.5" dx="1.5" dy="1.8" layer="1" rot="R90"/>
<smd name="2" x="0" y="-4.5" dx="1.5" dy="1.8" layer="1" rot="R90"/>
<text x="-3" y="5.5" size="1.27" layer="25">&gt;NAME</text>
<text x="-3" y="-7" size="1.27" layer="27">&gt;VALUE</text>
</package>
<package name="WE-MCA_7488910157">
<description>Multiplayer Chip Antenna WE-MCA</description>
<wire x1="0" y1="3.6" x2="0" y2="2.4" width="0.127" layer="21"/>
<wire x1="-1.5" y1="5" x2="1.5" y2="5" width="0.127" layer="21"/>
<wire x1="1.5" y1="5" x2="1.5" y2="-5" width="0.127" layer="21"/>
<wire x1="1.5" y1="-5" x2="-1.5" y2="-5" width="0.127" layer="21"/>
<wire x1="-1.5" y1="-5" x2="-1.5" y2="5" width="0.127" layer="21"/>
<smd name="1" x="0" y="5" dx="1.5" dy="3" layer="1" rot="R90"/>
<smd name="2" x="0" y="-5" dx="1.5" dy="3" layer="1" rot="R90"/>
<text x="-2.5" y="6.4" size="1.27" layer="25">&gt;Name</text>
<text x="-2.2" y="-8" size="1.27" layer="27">&gt;Value</text>
</package>
<package name="WE-MCA_7488930245">
<description>Multiplayer Chip Antenna WE-MCA</description>
<wire x1="0" y1="1.1" x2="0" y2="0.5" width="0.127" layer="21"/>
<wire x1="0.8" y1="1.6" x2="0.8" y2="-1.6" width="0.127" layer="21"/>
<wire x1="-0.8" y1="1.6" x2="-0.8" y2="-1.6" width="0.127" layer="21"/>
<wire x1="-0.8" y1="1.6" x2="0.8" y2="1.6" width="0.127" layer="21"/>
<wire x1="-0.8" y1="-1.6" x2="0.8" y2="-1.6" width="0.127" layer="21"/>
<smd name="2" x="0" y="-1.7" dx="0.8" dy="1.6" layer="1" rot="R90"/>
<smd name="1" x="0" y="1.7" dx="0.8" dy="1.6" layer="1" rot="R90"/>
<text x="-2.7" y="2.5" size="1.27" layer="25">&gt;Name</text>
<text x="-2.5" y="-3.9" size="1.27" layer="27">&gt;Value</text>
</package>
</packages>
<symbols>
<symbol name="WE-MCA">
<wire x1="0" y1="0" x2="1.27" y2="0" width="0.254" layer="94"/>
<wire x1="1.27" y1="0" x2="1.27" y2="6.35" width="0.254" layer="94"/>
<wire x1="1.27" y1="6.35" x2="0" y2="7.62" width="0.254" layer="94"/>
<wire x1="1.27" y1="6.35" x2="1.27" y2="7.62" width="0.254" layer="94"/>
<wire x1="1.27" y1="6.35" x2="2.54" y2="7.62" width="0.254" layer="94"/>
<pin name="FP" x="0" y="-2.54" visible="pin" length="short" direction="pas" rot="R90"/>
<pin name="NC" x="2.54" y="-2.54" visible="pin" length="short" direction="pas" rot="R90"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="WE-MCA" prefix="ANT" uservalue="yes">
<description>&lt;b&gt;Multilayer Chip Antenna WE-MCA&lt;/b&gt;&lt;p&gt;
- Power capacity: 3 Watt max.&lt;br&gt;
- Omni-directional&lt;br&gt;&lt;br&gt;

-- For RF-applications in the area of 2.4GHz&lt;br&gt;
-- WLAN&lt;br&gt;
-- Home RF&lt;br&gt;
-- Bluetooth applications&lt;br&gt;
-- IEEE 802.11a/b&lt;br&gt;
-- GPS</description>
<gates>
<gate name="G$1" symbol="WE-MCA" x="0" y="-2.54"/>
</gates>
<devices>
<device name="7488105XX/7488940245" package="WE-MCA_7488105XX/7488940245">
<connects>
<connect gate="G$1" pin="FP" pad="1"/>
<connect gate="G$1" pin="NC" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="7488910245" package="WE-MCA_7488910245">
<connects>
<connect gate="G$1" pin="FP" pad="1"/>
<connect gate="G$1" pin="NC" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="748891057" package="WE-MCA_7488910157">
<connects>
<connect gate="G$1" pin="FP" pad="1"/>
<connect gate="G$1" pin="NC" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="7488930245" package="WE-MCA_7488930245">
<connects>
<connect gate="G$1" pin="FP" pad="1"/>
<connect gate="G$1" pin="NC" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="special">
<description>&lt;b&gt;Special Devices&lt;/b&gt;&lt;p&gt;
7-segment displays, switches, heatsinks, crystals, transformers, etc.&lt;p&gt;
&lt;author&gt;Created by librarian@cadsoft.de&lt;/author&gt;</description>
<packages>
<package name="QS">
<description>&lt;B&gt;CRYSTAL&lt;/B&gt;</description>
<wire x1="-3.429" y1="-2.286" x2="3.429" y2="-2.286" width="0.1524" layer="21"/>
<wire x1="3.429" y1="2.286" x2="-3.429" y2="2.286" width="0.1524" layer="21"/>
<wire x1="-3.429" y1="-1.778" x2="3.429" y2="-1.778" width="0.0508" layer="21"/>
<wire x1="3.429" y1="1.778" x2="-3.429" y2="1.778" width="0.0508" layer="21"/>
<wire x1="3.429" y1="1.778" x2="3.429" y2="-1.778" width="0.0508" layer="21" curve="-180"/>
<wire x1="3.429" y1="2.286" x2="3.429" y2="-2.286" width="0.1524" layer="21" curve="-180"/>
<wire x1="-3.429" y1="2.286" x2="-3.429" y2="-2.286" width="0.1524" layer="21" curve="180"/>
<wire x1="-3.429" y1="1.778" x2="-3.429" y2="-1.778" width="0.0508" layer="21" curve="180"/>
<pad name="1" x="-2.54" y="0" drill="0.6096" shape="long" rot="R90"/>
<pad name="2" x="2.54" y="0" drill="0.6096" shape="long" rot="R90"/>
<text x="-5.08" y="-3.937" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
<text x="-5.08" y="2.667" size="1.27" layer="25" ratio="10">&gt;NAME</text>
</package>
</packages>
<symbols>
<symbol name="XTAL">
<wire x1="-1.27" y1="2.54" x2="1.397" y2="2.54" width="0.4064" layer="94"/>
<wire x1="1.397" y1="2.54" x2="1.397" y2="-2.54" width="0.4064" layer="94"/>
<wire x1="1.397" y1="-2.54" x2="-1.27" y2="-2.54" width="0.4064" layer="94"/>
<wire x1="-1.27" y1="2.54" x2="-1.27" y2="-2.54" width="0.4064" layer="94"/>
<wire x1="2.3368" y1="2.54" x2="2.3368" y2="-2.54" width="0.4064" layer="94"/>
<wire x1="-2.286" y1="2.54" x2="-2.286" y2="-2.54" width="0.4064" layer="94"/>
<text x="-5.08" y="3.81" size="1.778" layer="95">&gt;NAME</text>
<text x="-5.08" y="-5.08" size="1.778" layer="96">&gt;VALUE</text>
<pin name="2" x="5.08" y="0" visible="pad" length="short" direction="pas" swaplevel="1" rot="R180"/>
<pin name="1" x="-5.08" y="0" visible="pad" length="short" direction="pas" swaplevel="1"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="XTAL/S" prefix="Q" uservalue="yes">
<description>&lt;B&gt;CRYSTAL&lt;/B&gt;</description>
<gates>
<gate name="G$1" symbol="XTAL" x="0" y="0"/>
</gates>
<devices>
<device name="" package="QS">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="Alliance_Memory_By_element14_Batch_1">
<description>Developed by element14 :&lt;br&gt;
element14 CAD Library consolidation.ulp
at 25/07/2012 09:32:54</description>
<packages>
<package name="SOIC127P1407X299-32N">
<smd name="1" x="-6.6548" y="9.525" dx="1.8034" dy="0.4572" layer="1"/>
<smd name="2" x="-6.6548" y="8.255" dx="1.8034" dy="0.4572" layer="1"/>
<smd name="3" x="-6.6548" y="6.985" dx="1.8034" dy="0.4572" layer="1"/>
<smd name="4" x="-6.6548" y="5.715" dx="1.8034" dy="0.4572" layer="1"/>
<smd name="5" x="-6.6548" y="4.445" dx="1.8034" dy="0.4572" layer="1"/>
<smd name="6" x="-6.6548" y="3.175" dx="1.8034" dy="0.4572" layer="1"/>
<smd name="7" x="-6.6548" y="1.905" dx="1.8034" dy="0.4572" layer="1"/>
<smd name="8" x="-6.6548" y="0.635" dx="1.8034" dy="0.4572" layer="1"/>
<smd name="9" x="-6.6548" y="-0.635" dx="1.8034" dy="0.4572" layer="1"/>
<smd name="10" x="-6.6548" y="-1.905" dx="1.8034" dy="0.4572" layer="1"/>
<smd name="11" x="-6.6548" y="-3.175" dx="1.8034" dy="0.4572" layer="1"/>
<smd name="12" x="-6.6548" y="-4.445" dx="1.8034" dy="0.4572" layer="1"/>
<smd name="13" x="-6.6548" y="-5.715" dx="1.8034" dy="0.4572" layer="1"/>
<smd name="14" x="-6.6548" y="-6.985" dx="1.8034" dy="0.4572" layer="1"/>
<smd name="15" x="-6.6548" y="-8.255" dx="1.8034" dy="0.4572" layer="1"/>
<smd name="16" x="-6.6548" y="-9.525" dx="1.8034" dy="0.4572" layer="1"/>
<smd name="17" x="6.6548" y="-9.525" dx="1.8034" dy="0.4572" layer="1"/>
<smd name="18" x="6.6548" y="-8.255" dx="1.8034" dy="0.4572" layer="1"/>
<smd name="19" x="6.6548" y="-6.985" dx="1.8034" dy="0.4572" layer="1"/>
<smd name="20" x="6.6548" y="-5.715" dx="1.8034" dy="0.4572" layer="1"/>
<smd name="21" x="6.6548" y="-4.445" dx="1.8034" dy="0.4572" layer="1"/>
<smd name="22" x="6.6548" y="-3.175" dx="1.8034" dy="0.4572" layer="1"/>
<smd name="23" x="6.6548" y="-1.905" dx="1.8034" dy="0.4572" layer="1"/>
<smd name="24" x="6.6548" y="-0.635" dx="1.8034" dy="0.4572" layer="1"/>
<smd name="25" x="6.6548" y="0.635" dx="1.8034" dy="0.4572" layer="1"/>
<smd name="26" x="6.6548" y="1.905" dx="1.8034" dy="0.4572" layer="1"/>
<smd name="27" x="6.6548" y="3.175" dx="1.8034" dy="0.4572" layer="1"/>
<smd name="28" x="6.6548" y="4.445" dx="1.8034" dy="0.4572" layer="1"/>
<smd name="29" x="6.6548" y="5.715" dx="1.8034" dy="0.4572" layer="1"/>
<smd name="30" x="6.6548" y="6.985" dx="1.8034" dy="0.4572" layer="1"/>
<smd name="31" x="6.6548" y="8.255" dx="1.8034" dy="0.4572" layer="1"/>
<smd name="32" x="6.6548" y="9.525" dx="1.8034" dy="0.4572" layer="1"/>
<wire x1="-7.8994" y1="-1.905" x2="-8.9154" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="8.9408" y1="-5.715" x2="7.9248" y2="-5.715" width="0.1524" layer="21"/>
<wire x1="8.9408" y1="6.9596" x2="7.9248" y2="6.9596" width="0.1524" layer="21"/>
<wire x1="5.6388" y1="10.0584" x2="5.6388" y2="10.3632" width="0.1524" layer="21"/>
<wire x1="5.6388" y1="8.7884" x2="5.6388" y2="8.9916" width="0.1524" layer="21"/>
<wire x1="5.6388" y1="7.5184" x2="5.6388" y2="7.7216" width="0.1524" layer="21"/>
<wire x1="5.6388" y1="6.2484" x2="5.6388" y2="6.4516" width="0.1524" layer="21"/>
<wire x1="-5.6388" y1="-10.3632" x2="5.6388" y2="-10.3632" width="0.1524" layer="21"/>
<wire x1="5.6388" y1="-10.3632" x2="5.6388" y2="-10.0584" width="0.1524" layer="21"/>
<wire x1="5.6388" y1="10.3632" x2="0.3048" y2="10.3632" width="0.1524" layer="21"/>
<wire x1="0.3048" y1="10.3632" x2="-0.3048" y2="10.3632" width="0.1524" layer="21"/>
<wire x1="-0.3048" y1="10.3632" x2="-5.6388" y2="10.3632" width="0.1524" layer="21"/>
<wire x1="-5.6388" y1="10.3632" x2="-5.6388" y2="10.0584" width="0.1524" layer="21"/>
<wire x1="-5.6388" y1="8.9916" x2="-5.6388" y2="8.7884" width="0.1524" layer="21"/>
<wire x1="-5.6388" y1="7.7216" x2="-5.6388" y2="7.5184" width="0.1524" layer="21"/>
<wire x1="-5.6388" y1="6.4516" x2="-5.6388" y2="6.2484" width="0.1524" layer="21"/>
<wire x1="-5.6388" y1="5.1816" x2="-5.6388" y2="4.9784" width="0.1524" layer="21"/>
<wire x1="-5.6388" y1="3.9116" x2="-5.6388" y2="3.7084" width="0.1524" layer="21"/>
<wire x1="-5.6388" y1="2.6416" x2="-5.6388" y2="2.4384" width="0.1524" layer="21"/>
<wire x1="-5.6388" y1="1.3716" x2="-5.6388" y2="1.1684" width="0.1524" layer="21"/>
<wire x1="-5.6388" y1="0.1016" x2="-5.6388" y2="-0.1016" width="0.1524" layer="21"/>
<wire x1="-5.6388" y1="-1.1684" x2="-5.6388" y2="-1.3716" width="0.1524" layer="21"/>
<wire x1="-5.6388" y1="-2.4384" x2="-5.6388" y2="-2.6416" width="0.1524" layer="21"/>
<wire x1="-5.6388" y1="-3.7084" x2="-5.6388" y2="-3.9116" width="0.1524" layer="21"/>
<wire x1="-5.6388" y1="-4.9784" x2="-5.6388" y2="-5.1816" width="0.1524" layer="21"/>
<wire x1="-5.6388" y1="-6.2484" x2="-5.6388" y2="-6.4516" width="0.1524" layer="21"/>
<wire x1="-5.6388" y1="-7.5184" x2="-5.6388" y2="-7.7216" width="0.1524" layer="21"/>
<wire x1="-5.6388" y1="-8.7884" x2="-5.6388" y2="-8.9916" width="0.1524" layer="21"/>
<wire x1="-5.6388" y1="-10.0584" x2="-5.6388" y2="-10.3632" width="0.1524" layer="21"/>
<wire x1="5.6388" y1="-8.9916" x2="5.6388" y2="-8.7884" width="0.1524" layer="21"/>
<wire x1="5.6388" y1="-7.7216" x2="5.6388" y2="-7.5184" width="0.1524" layer="21"/>
<wire x1="5.6388" y1="-6.4516" x2="5.6388" y2="-6.2484" width="0.1524" layer="21"/>
<wire x1="5.6388" y1="-5.1816" x2="5.6388" y2="-4.9784" width="0.1524" layer="21"/>
<wire x1="5.6388" y1="-3.9116" x2="5.6388" y2="-3.7084" width="0.1524" layer="21"/>
<wire x1="5.6388" y1="-2.6416" x2="5.6388" y2="-2.4384" width="0.1524" layer="21"/>
<wire x1="5.6388" y1="-1.3716" x2="5.6388" y2="-1.1684" width="0.1524" layer="21"/>
<wire x1="5.6388" y1="-0.1016" x2="5.6388" y2="0.1016" width="0.1524" layer="21"/>
<wire x1="5.6388" y1="1.1684" x2="5.6388" y2="1.3716" width="0.1524" layer="21"/>
<wire x1="5.6388" y1="2.4384" x2="5.6388" y2="2.6416" width="0.1524" layer="21"/>
<wire x1="5.6388" y1="3.7084" x2="5.6388" y2="3.9116" width="0.1524" layer="21"/>
<wire x1="5.6388" y1="4.9784" x2="5.6388" y2="5.1816" width="0.1524" layer="21"/>
<wire x1="0.3048" y1="10.3632" x2="-0.3048" y2="10.3632" width="0.1524" layer="21" curve="-180"/>
<text x="-7.493" y="9.8552" size="1.27" layer="21" ratio="6" rot="SR0">*</text>
<wire x1="-5.6388" y1="9.3218" x2="-5.6388" y2="9.7282" width="0" layer="51"/>
<wire x1="-5.6388" y1="9.7282" x2="-7.2136" y2="9.7282" width="0" layer="51"/>
<wire x1="-7.2136" y1="9.7282" x2="-7.2136" y2="9.3218" width="0" layer="51"/>
<wire x1="-7.2136" y1="9.3218" x2="-5.6388" y2="9.3218" width="0" layer="51"/>
<wire x1="-5.6388" y1="8.0518" x2="-5.6388" y2="8.4582" width="0" layer="51"/>
<wire x1="-5.6388" y1="8.4582" x2="-7.2136" y2="8.4582" width="0" layer="51"/>
<wire x1="-7.2136" y1="8.4582" x2="-7.2136" y2="8.0518" width="0" layer="51"/>
<wire x1="-7.2136" y1="8.0518" x2="-5.6388" y2="8.0518" width="0" layer="51"/>
<wire x1="-5.6388" y1="6.7818" x2="-5.6388" y2="7.1882" width="0" layer="51"/>
<wire x1="-5.6388" y1="7.1882" x2="-7.2136" y2="7.1882" width="0" layer="51"/>
<wire x1="-7.2136" y1="7.1882" x2="-7.2136" y2="6.7818" width="0" layer="51"/>
<wire x1="-7.2136" y1="6.7818" x2="-5.6388" y2="6.7818" width="0" layer="51"/>
<wire x1="-5.6388" y1="5.5118" x2="-5.6388" y2="5.9182" width="0" layer="51"/>
<wire x1="-5.6388" y1="5.9182" x2="-7.2136" y2="5.9182" width="0" layer="51"/>
<wire x1="-7.2136" y1="5.9182" x2="-7.2136" y2="5.5118" width="0" layer="51"/>
<wire x1="-7.2136" y1="5.5118" x2="-5.6388" y2="5.5118" width="0" layer="51"/>
<wire x1="-5.6388" y1="4.2418" x2="-5.6388" y2="4.6482" width="0" layer="51"/>
<wire x1="-5.6388" y1="4.6482" x2="-7.2136" y2="4.6482" width="0" layer="51"/>
<wire x1="-7.2136" y1="4.6482" x2="-7.2136" y2="4.2418" width="0" layer="51"/>
<wire x1="-7.2136" y1="4.2418" x2="-5.6388" y2="4.2418" width="0" layer="51"/>
<wire x1="-5.6388" y1="2.9718" x2="-5.6388" y2="3.3782" width="0" layer="51"/>
<wire x1="-5.6388" y1="3.3782" x2="-7.2136" y2="3.3782" width="0" layer="51"/>
<wire x1="-7.2136" y1="3.3782" x2="-7.2136" y2="2.9718" width="0" layer="51"/>
<wire x1="-7.2136" y1="2.9718" x2="-5.6388" y2="2.9718" width="0" layer="51"/>
<wire x1="-5.6388" y1="1.7018" x2="-5.6388" y2="2.1082" width="0" layer="51"/>
<wire x1="-5.6388" y1="2.1082" x2="-7.2136" y2="2.1082" width="0" layer="51"/>
<wire x1="-7.2136" y1="2.1082" x2="-7.2136" y2="1.7018" width="0" layer="51"/>
<wire x1="-7.2136" y1="1.7018" x2="-5.6388" y2="1.7018" width="0" layer="51"/>
<wire x1="-5.6388" y1="0.4318" x2="-5.6388" y2="0.8382" width="0" layer="51"/>
<wire x1="-5.6388" y1="0.8382" x2="-7.2136" y2="0.8382" width="0" layer="51"/>
<wire x1="-7.2136" y1="0.8382" x2="-7.2136" y2="0.4318" width="0" layer="51"/>
<wire x1="-7.2136" y1="0.4318" x2="-5.6388" y2="0.4318" width="0" layer="51"/>
<wire x1="-5.6388" y1="-0.8382" x2="-5.6388" y2="-0.4318" width="0" layer="51"/>
<wire x1="-5.6388" y1="-0.4318" x2="-7.2136" y2="-0.4318" width="0" layer="51"/>
<wire x1="-7.2136" y1="-0.4318" x2="-7.2136" y2="-0.8382" width="0" layer="51"/>
<wire x1="-7.2136" y1="-0.8382" x2="-5.6388" y2="-0.8382" width="0" layer="51"/>
<wire x1="-5.6388" y1="-2.1082" x2="-5.6388" y2="-1.7018" width="0" layer="51"/>
<wire x1="-5.6388" y1="-1.7018" x2="-7.2136" y2="-1.7018" width="0" layer="51"/>
<wire x1="-7.2136" y1="-1.7018" x2="-7.2136" y2="-2.1082" width="0" layer="51"/>
<wire x1="-7.2136" y1="-2.1082" x2="-5.6388" y2="-2.1082" width="0" layer="51"/>
<wire x1="-5.6388" y1="-3.3782" x2="-5.6388" y2="-2.9718" width="0" layer="51"/>
<wire x1="-5.6388" y1="-2.9718" x2="-7.2136" y2="-2.9718" width="0" layer="51"/>
<wire x1="-7.2136" y1="-2.9718" x2="-7.2136" y2="-3.3782" width="0" layer="51"/>
<wire x1="-7.2136" y1="-3.3782" x2="-5.6388" y2="-3.3782" width="0" layer="51"/>
<wire x1="-5.6388" y1="-4.6482" x2="-5.6388" y2="-4.2418" width="0" layer="51"/>
<wire x1="-5.6388" y1="-4.2418" x2="-7.2136" y2="-4.2418" width="0" layer="51"/>
<wire x1="-7.2136" y1="-4.2418" x2="-7.2136" y2="-4.6482" width="0" layer="51"/>
<wire x1="-7.2136" y1="-4.6482" x2="-5.6388" y2="-4.6482" width="0" layer="51"/>
<wire x1="-5.6388" y1="-5.9182" x2="-5.6388" y2="-5.5118" width="0" layer="51"/>
<wire x1="-5.6388" y1="-5.5118" x2="-7.2136" y2="-5.5118" width="0" layer="51"/>
<wire x1="-7.2136" y1="-5.5118" x2="-7.2136" y2="-5.9182" width="0" layer="51"/>
<wire x1="-7.2136" y1="-5.9182" x2="-5.6388" y2="-5.9182" width="0" layer="51"/>
<wire x1="-5.6388" y1="-7.1882" x2="-5.6388" y2="-6.7818" width="0" layer="51"/>
<wire x1="-5.6388" y1="-6.7818" x2="-7.2136" y2="-6.7818" width="0" layer="51"/>
<wire x1="-7.2136" y1="-6.7818" x2="-7.2136" y2="-7.1882" width="0" layer="51"/>
<wire x1="-7.2136" y1="-7.1882" x2="-5.6388" y2="-7.1882" width="0" layer="51"/>
<wire x1="-5.6388" y1="-8.4582" x2="-5.6388" y2="-8.0518" width="0" layer="51"/>
<wire x1="-5.6388" y1="-8.0518" x2="-7.2136" y2="-8.0518" width="0" layer="51"/>
<wire x1="-7.2136" y1="-8.0518" x2="-7.2136" y2="-8.4582" width="0" layer="51"/>
<wire x1="-7.2136" y1="-8.4582" x2="-5.6388" y2="-8.4582" width="0" layer="51"/>
<wire x1="-5.6388" y1="-9.7282" x2="-5.6388" y2="-9.3218" width="0" layer="51"/>
<wire x1="-5.6388" y1="-9.3218" x2="-7.2136" y2="-9.3218" width="0" layer="51"/>
<wire x1="-7.2136" y1="-9.3218" x2="-7.2136" y2="-9.7282" width="0" layer="51"/>
<wire x1="-7.2136" y1="-9.7282" x2="-5.6388" y2="-9.7282" width="0" layer="51"/>
<wire x1="5.6388" y1="-9.3218" x2="5.6388" y2="-9.7282" width="0" layer="51"/>
<wire x1="5.6388" y1="-9.7282" x2="7.2136" y2="-9.7282" width="0" layer="51"/>
<wire x1="7.2136" y1="-9.7282" x2="7.2136" y2="-9.3218" width="0" layer="51"/>
<wire x1="7.2136" y1="-9.3218" x2="5.6388" y2="-9.3218" width="0" layer="51"/>
<wire x1="5.6388" y1="-8.0518" x2="5.6388" y2="-8.4582" width="0" layer="51"/>
<wire x1="5.6388" y1="-8.4582" x2="7.2136" y2="-8.4582" width="0" layer="51"/>
<wire x1="7.2136" y1="-8.4582" x2="7.2136" y2="-8.0518" width="0" layer="51"/>
<wire x1="7.2136" y1="-8.0518" x2="5.6388" y2="-8.0518" width="0" layer="51"/>
<wire x1="5.6388" y1="-6.7818" x2="5.6388" y2="-7.1882" width="0" layer="51"/>
<wire x1="5.6388" y1="-7.1882" x2="7.2136" y2="-7.1882" width="0" layer="51"/>
<wire x1="7.2136" y1="-7.1882" x2="7.2136" y2="-6.7818" width="0" layer="51"/>
<wire x1="7.2136" y1="-6.7818" x2="5.6388" y2="-6.7818" width="0" layer="51"/>
<wire x1="5.6388" y1="-5.5118" x2="5.6388" y2="-5.9182" width="0" layer="51"/>
<wire x1="5.6388" y1="-5.9182" x2="7.2136" y2="-5.9182" width="0" layer="51"/>
<wire x1="7.2136" y1="-5.9182" x2="7.2136" y2="-5.5118" width="0" layer="51"/>
<wire x1="7.2136" y1="-5.5118" x2="5.6388" y2="-5.5118" width="0" layer="51"/>
<wire x1="5.6388" y1="-4.2418" x2="5.6388" y2="-4.6482" width="0" layer="51"/>
<wire x1="5.6388" y1="-4.6482" x2="7.2136" y2="-4.6482" width="0" layer="51"/>
<wire x1="7.2136" y1="-4.6482" x2="7.2136" y2="-4.2418" width="0" layer="51"/>
<wire x1="7.2136" y1="-4.2418" x2="5.6388" y2="-4.2418" width="0" layer="51"/>
<wire x1="5.6388" y1="-2.9718" x2="5.6388" y2="-3.3782" width="0" layer="51"/>
<wire x1="5.6388" y1="-3.3782" x2="7.2136" y2="-3.3782" width="0" layer="51"/>
<wire x1="7.2136" y1="-3.3782" x2="7.2136" y2="-2.9718" width="0" layer="51"/>
<wire x1="7.2136" y1="-2.9718" x2="5.6388" y2="-2.9718" width="0" layer="51"/>
<wire x1="5.6388" y1="-1.7018" x2="5.6388" y2="-2.1082" width="0" layer="51"/>
<wire x1="5.6388" y1="-2.1082" x2="7.2136" y2="-2.1082" width="0" layer="51"/>
<wire x1="7.2136" y1="-2.1082" x2="7.2136" y2="-1.7018" width="0" layer="51"/>
<wire x1="7.2136" y1="-1.7018" x2="5.6388" y2="-1.7018" width="0" layer="51"/>
<wire x1="5.6388" y1="-0.4318" x2="5.6388" y2="-0.8382" width="0" layer="51"/>
<wire x1="5.6388" y1="-0.8382" x2="7.2136" y2="-0.8382" width="0" layer="51"/>
<wire x1="7.2136" y1="-0.8382" x2="7.2136" y2="-0.4318" width="0" layer="51"/>
<wire x1="7.2136" y1="-0.4318" x2="5.6388" y2="-0.4318" width="0" layer="51"/>
<wire x1="5.6388" y1="0.8382" x2="5.6388" y2="0.4318" width="0" layer="51"/>
<wire x1="5.6388" y1="0.4318" x2="7.2136" y2="0.4318" width="0" layer="51"/>
<wire x1="7.2136" y1="0.4318" x2="7.2136" y2="0.8382" width="0" layer="51"/>
<wire x1="7.2136" y1="0.8382" x2="5.6388" y2="0.8382" width="0" layer="51"/>
<wire x1="5.6388" y1="2.1082" x2="5.6388" y2="1.7018" width="0" layer="51"/>
<wire x1="5.6388" y1="1.7018" x2="7.2136" y2="1.7018" width="0" layer="51"/>
<wire x1="7.2136" y1="1.7018" x2="7.2136" y2="2.1082" width="0" layer="51"/>
<wire x1="7.2136" y1="2.1082" x2="5.6388" y2="2.1082" width="0" layer="51"/>
<wire x1="5.6388" y1="3.3782" x2="5.6388" y2="2.9718" width="0" layer="51"/>
<wire x1="5.6388" y1="2.9718" x2="7.2136" y2="2.9718" width="0" layer="51"/>
<wire x1="7.2136" y1="2.9718" x2="7.2136" y2="3.3782" width="0" layer="51"/>
<wire x1="7.2136" y1="3.3782" x2="5.6388" y2="3.3782" width="0" layer="51"/>
<wire x1="5.6388" y1="4.6482" x2="5.6388" y2="4.2418" width="0" layer="51"/>
<wire x1="5.6388" y1="4.2418" x2="7.2136" y2="4.2418" width="0" layer="51"/>
<wire x1="7.2136" y1="4.2418" x2="7.2136" y2="4.6482" width="0" layer="51"/>
<wire x1="7.2136" y1="4.6482" x2="5.6388" y2="4.6482" width="0" layer="51"/>
<wire x1="5.6388" y1="5.9182" x2="5.6388" y2="5.5118" width="0" layer="51"/>
<wire x1="5.6388" y1="5.5118" x2="7.2136" y2="5.5118" width="0" layer="51"/>
<wire x1="7.2136" y1="5.5118" x2="7.2136" y2="5.9182" width="0" layer="51"/>
<wire x1="7.2136" y1="5.9182" x2="5.6388" y2="5.9182" width="0" layer="51"/>
<wire x1="5.6388" y1="7.1882" x2="5.6388" y2="6.7818" width="0" layer="51"/>
<wire x1="5.6388" y1="6.7818" x2="7.2136" y2="6.7818" width="0" layer="51"/>
<wire x1="7.2136" y1="6.7818" x2="7.2136" y2="7.1882" width="0" layer="51"/>
<wire x1="7.2136" y1="7.1882" x2="5.6388" y2="7.1882" width="0" layer="51"/>
<wire x1="5.6388" y1="8.4582" x2="5.6388" y2="8.0518" width="0" layer="51"/>
<wire x1="5.6388" y1="8.0518" x2="7.2136" y2="8.0518" width="0" layer="51"/>
<wire x1="7.2136" y1="8.0518" x2="7.2136" y2="8.4582" width="0" layer="51"/>
<wire x1="7.2136" y1="8.4582" x2="5.6388" y2="8.4582" width="0" layer="51"/>
<wire x1="5.6388" y1="9.7282" x2="5.6388" y2="9.3218" width="0" layer="51"/>
<wire x1="5.6388" y1="9.3218" x2="7.2136" y2="9.3218" width="0" layer="51"/>
<wire x1="7.2136" y1="9.3218" x2="7.2136" y2="9.7282" width="0" layer="51"/>
<wire x1="7.2136" y1="9.7282" x2="5.6388" y2="9.7282" width="0" layer="51"/>
<wire x1="-5.6388" y1="-10.3632" x2="5.6388" y2="-10.3632" width="0" layer="51"/>
<wire x1="5.6388" y1="-10.3632" x2="5.6388" y2="10.3632" width="0" layer="51"/>
<wire x1="5.6388" y1="10.3632" x2="0.3048" y2="10.3632" width="0" layer="51"/>
<wire x1="0.3048" y1="10.3632" x2="-0.3048" y2="10.3632" width="0" layer="51"/>
<wire x1="-0.3048" y1="10.3632" x2="-5.6388" y2="10.3632" width="0" layer="51"/>
<wire x1="-5.6388" y1="10.3632" x2="-5.6388" y2="-10.3632" width="0" layer="51"/>
<wire x1="0.3048" y1="10.3632" x2="-0.3048" y2="10.3632" width="0" layer="51" curve="-180"/>
<text x="-7.493" y="9.8552" size="1.27" layer="51" ratio="6" rot="SR0">*</text>
<text x="-3.4544" y="11.43" size="2.0828" layer="25" ratio="10" rot="SR0">&gt;NAME</text>
<text x="-3.4544" y="-13.335" size="2.0828" layer="27" ratio="10" rot="SR0">&gt;VALUE</text>
</package>
</packages>
<symbols>
<symbol name="AS6C4008-55SIN">
<pin name="VCC" x="-17.78" y="30.48" length="middle" direction="pwr"/>
<pin name="CE" x="-17.78" y="25.4" length="middle" direction="in"/>
<pin name="OE" x="-17.78" y="22.86" length="middle" direction="in"/>
<pin name="WE" x="-17.78" y="20.32" length="middle" direction="in"/>
<pin name="A0" x="-17.78" y="15.24" length="middle" direction="in"/>
<pin name="A1" x="-17.78" y="12.7" length="middle" direction="in"/>
<pin name="A2" x="-17.78" y="10.16" length="middle" direction="in"/>
<pin name="A3" x="-17.78" y="7.62" length="middle" direction="in"/>
<pin name="A4" x="-17.78" y="5.08" length="middle" direction="in"/>
<pin name="A5" x="-17.78" y="2.54" length="middle" direction="in"/>
<pin name="A6" x="-17.78" y="0" length="middle" direction="in"/>
<pin name="A7" x="-17.78" y="-2.54" length="middle" direction="in"/>
<pin name="A8" x="-17.78" y="-5.08" length="middle" direction="in"/>
<pin name="A9" x="-17.78" y="-7.62" length="middle" direction="in"/>
<pin name="A10" x="-17.78" y="-10.16" length="middle" direction="in"/>
<pin name="A11" x="-17.78" y="-12.7" length="middle" direction="in"/>
<pin name="A12" x="-17.78" y="-15.24" length="middle" direction="in"/>
<pin name="A13" x="-17.78" y="-17.78" length="middle" direction="in"/>
<pin name="A14" x="-17.78" y="-20.32" length="middle" direction="in"/>
<pin name="A15" x="-17.78" y="-22.86" length="middle" direction="in"/>
<pin name="A16" x="-17.78" y="-25.4" length="middle" direction="in"/>
<pin name="A17" x="-17.78" y="-27.94" length="middle" direction="in"/>
<pin name="A18" x="-17.78" y="-30.48" length="middle" direction="in"/>
<pin name="VSS" x="-17.78" y="-35.56" length="middle" direction="out"/>
<pin name="DQ0" x="17.78" y="30.48" length="middle" direction="out" rot="R180"/>
<pin name="DQ1" x="17.78" y="27.94" length="middle" direction="out" rot="R180"/>
<pin name="DQ2" x="17.78" y="25.4" length="middle" direction="out" rot="R180"/>
<pin name="DQ3" x="17.78" y="22.86" length="middle" direction="out" rot="R180"/>
<pin name="DQ4" x="17.78" y="20.32" length="middle" direction="out" rot="R180"/>
<pin name="DQ5" x="17.78" y="17.78" length="middle" direction="out" rot="R180"/>
<pin name="DQ6" x="17.78" y="15.24" length="middle" direction="out" rot="R180"/>
<pin name="DQ7" x="17.78" y="12.7" length="middle" direction="out" rot="R180"/>
<wire x1="-12.7" y1="35.56" x2="-12.7" y2="-40.64" width="0.4064" layer="94"/>
<wire x1="-12.7" y1="-40.64" x2="12.7" y2="-40.64" width="0.4064" layer="94"/>
<wire x1="12.7" y1="-40.64" x2="12.7" y2="35.56" width="0.4064" layer="94"/>
<wire x1="12.7" y1="35.56" x2="-12.7" y2="35.56" width="0.4064" layer="94"/>
<text x="-4.9276" y="38.227" size="2.0828" layer="95" ratio="10" rot="SR0">&gt;NAME</text>
<text x="-5.6642" y="-45.085" size="2.0828" layer="96" ratio="10" rot="SR0">&gt;VALUE</text>
</symbol>
</symbols>
<devicesets>
<deviceset name="AS6C4008-55SIN" prefix="U">
<description>CMOS SRAM</description>
<gates>
<gate name="A" symbol="AS6C4008-55SIN" x="0" y="0"/>
</gates>
<devices>
<device name="" package="SOIC127P1407X299-32N">
<connects>
<connect gate="A" pin="A0" pad="12"/>
<connect gate="A" pin="A1" pad="11"/>
<connect gate="A" pin="A10" pad="23"/>
<connect gate="A" pin="A11" pad="25"/>
<connect gate="A" pin="A12" pad="4"/>
<connect gate="A" pin="A13" pad="28"/>
<connect gate="A" pin="A14" pad="3"/>
<connect gate="A" pin="A15" pad="31"/>
<connect gate="A" pin="A16" pad="2"/>
<connect gate="A" pin="A17" pad="30"/>
<connect gate="A" pin="A18" pad="1"/>
<connect gate="A" pin="A2" pad="10"/>
<connect gate="A" pin="A3" pad="9"/>
<connect gate="A" pin="A4" pad="8"/>
<connect gate="A" pin="A5" pad="7"/>
<connect gate="A" pin="A6" pad="6"/>
<connect gate="A" pin="A7" pad="5"/>
<connect gate="A" pin="A8" pad="27"/>
<connect gate="A" pin="A9" pad="26"/>
<connect gate="A" pin="CE" pad="22"/>
<connect gate="A" pin="DQ0" pad="13"/>
<connect gate="A" pin="DQ1" pad="14"/>
<connect gate="A" pin="DQ2" pad="15"/>
<connect gate="A" pin="DQ3" pad="17"/>
<connect gate="A" pin="DQ4" pad="18"/>
<connect gate="A" pin="DQ5" pad="19"/>
<connect gate="A" pin="DQ6" pad="20"/>
<connect gate="A" pin="DQ7" pad="21"/>
<connect gate="A" pin="OE" pad="24"/>
<connect gate="A" pin="VCC" pad="32"/>
<connect gate="A" pin="VSS" pad="16"/>
<connect gate="A" pin="WE" pad="29"/>
</connects>
<technologies>
<technology name="">
<attribute name="MPN" value="AS6C4008-55SIN" constant="no"/>
<attribute name="OC_FARNELL" value="1562901" constant="no"/>
<attribute name="OC_NEWARK" value="94M6218" constant="no"/>
<attribute name="PACKAGE" value="SO-32" constant="no"/>
<attribute name="SUPPLIER" value="ALLIANCE MEMORY" constant="no"/>
</technology>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="ipc-7351-transistor">
<description>&lt;b&gt;IPC-7351 compliant SMT transistors&lt;/b&gt;&lt;br&gt;
&lt;br&gt;
Symbols copied from CadSoft transistor.lbr.&lt;br&gt;
Packages generated using genpkg_sot.ulp.&lt;br&gt;
Devices are Fairchild Semiconductor types.&lt;br&gt;
&lt;br&gt;
Weartronics 2006&lt;br&gt;
http://www.weartronics.com/</description>
<packages>
<package name="SOT95P280X135-3N">
<wire x1="-2.1" y1="-1.7" x2="-2.1" y2="1.7" width="0" layer="39"/>
<wire x1="-2.1" y1="1.7" x2="2.1" y2="1.7" width="0" layer="39"/>
<wire x1="2.1" y1="1.7" x2="2.1" y2="-1.7" width="0" layer="39"/>
<wire x1="2.1" y1="-1.7" x2="-2.1" y2="-1.7" width="0" layer="39"/>
<wire x1="0" y1="1.45" x2="0" y2="-1.45" width="0.127" layer="21"/>
<wire x1="-0.65" y1="-1.45" x2="-0.65" y2="1.45" width="0.127" layer="51"/>
<wire x1="-0.65" y1="1.45" x2="0.65" y2="1.45" width="0.127" layer="51"/>
<wire x1="0.65" y1="1.45" x2="0.65" y2="-1.45" width="0.127" layer="51"/>
<wire x1="0.65" y1="-1.45" x2="-0.65" y2="-1.45" width="0.127" layer="51"/>
<circle x="-1.6" y="1.779" radius="0.25" width="0" layer="21"/>
<smd name="1" x="-1.15" y="0.95" dx="0.65" dy="1.4" layer="1" roundness="100" rot="R90"/>
<smd name="2" x="-1.15" y="-0.95" dx="0.65" dy="1.4" layer="1" roundness="100" rot="R90"/>
<smd name="3" x="1.15" y="0" dx="0.65" dy="1.4" layer="1" roundness="100" rot="R90"/>
<text x="-1.096" y="1.7675" size="1.27" layer="25" font="vector" ratio="10">&gt;NAME</text>
<text x="-1.096" y="-3.0375" size="1.27" layer="27" font="vector" ratio="10">&gt;VALUE</text>
<rectangle x1="-1.4" y1="0.71" x2="-0.65" y2="1.19" layer="51"/>
<rectangle x1="-1.4" y1="0.71" x2="-0.65" y2="1.19" layer="51"/>
<rectangle x1="-1.4" y1="-1.19" x2="-0.65" y2="-0.71" layer="51"/>
<rectangle x1="-1.4" y1="-1.19" x2="-0.65" y2="-0.71" layer="51"/>
<rectangle x1="0.65" y1="-0.24" x2="1.4" y2="0.24" layer="51"/>
<rectangle x1="0.65" y1="-0.24" x2="1.4" y2="0.24" layer="51"/>
</package>
<package name="SOT230P700X160-4N">
<wire x1="-1.75" y1="-3.25" x2="-1.75" y2="3.25" width="0.127" layer="51"/>
<wire x1="-1.75" y1="3.25" x2="1.75" y2="3.25" width="0.127" layer="51"/>
<wire x1="1.75" y1="3.25" x2="1.75" y2="-3.25" width="0.127" layer="51"/>
<wire x1="1.75" y1="-3.25" x2="-1.75" y2="-3.25" width="0.127" layer="51"/>
<wire x1="-1.4" y1="-3.25" x2="-1.4" y2="3.25" width="0.127" layer="21"/>
<wire x1="-1.4" y1="3.25" x2="1.4" y2="3.25" width="0.127" layer="21"/>
<wire x1="1.4" y1="3.25" x2="1.4" y2="-3.25" width="0.127" layer="21"/>
<wire x1="1.4" y1="-3.25" x2="-1.4" y2="-3.25" width="0.127" layer="21"/>
<wire x1="-4.25" y1="-3.6" x2="-4.25" y2="3.6" width="0" layer="39"/>
<wire x1="-4.25" y1="3.6" x2="4.25" y2="3.6" width="0" layer="39"/>
<wire x1="4.25" y1="3.6" x2="4.25" y2="-3.6" width="0" layer="39"/>
<wire x1="4.25" y1="-3.6" x2="-4.25" y2="-3.6" width="0" layer="39"/>
<circle x="-2.9" y="3.279" radius="0.25" width="0" layer="21"/>
<circle x="-1" y="2.5" radius="0.375" width="0.127" layer="51"/>
<smd name="1" x="-2.9" y="2.3" dx="0.95" dy="2.15" layer="1" rot="R90"/>
<smd name="2" x="-2.9" y="0" dx="0.95" dy="2.15" layer="1" rot="R90"/>
<smd name="3" x="-2.9" y="-2.3" dx="0.95" dy="2.15" layer="1" rot="R90"/>
<smd name="4" x="2.9" y="0" dx="3.15" dy="2.15" layer="1" rot="R90"/>
<text x="-1.4635" y="3.5675" size="1.27" layer="25" font="vector" ratio="10">&gt;NAME</text>
<text x="-1.4635" y="-4.8375" size="1.27" layer="27" font="vector" ratio="10">&gt;VALUE</text>
<rectangle x1="-3.5" y1="-0.35" x2="-1.75" y2="0.35" layer="51"/>
<rectangle x1="-3.5" y1="1.95" x2="-1.75" y2="2.65" layer="51"/>
<rectangle x1="-3.5" y1="-2.65" x2="-1.75" y2="-1.95" layer="51"/>
<rectangle x1="1.75" y1="-1.5" x2="3.5" y2="1.5" layer="51"/>
</package>
<package name="SOIC127P600X175-8AN">
<wire x1="-3.75" y1="-2.75" x2="-3.75" y2="2.75" width="0" layer="39"/>
<wire x1="-3.75" y1="2.75" x2="3.75" y2="2.75" width="0" layer="39"/>
<wire x1="3.75" y1="2.75" x2="3.75" y2="-2.75" width="0" layer="39"/>
<wire x1="3.75" y1="-2.75" x2="-3.75" y2="-2.75" width="0" layer="39"/>
<wire x1="-1.55" y1="-2.5" x2="-1.55" y2="2.5" width="0.127" layer="21"/>
<wire x1="-1.55" y1="2.5" x2="1.55" y2="2.5" width="0.127" layer="21"/>
<wire x1="1.55" y1="2.5" x2="1.55" y2="-2.5" width="0.127" layer="21"/>
<wire x1="1.55" y1="-2.5" x2="-1.55" y2="-2.5" width="0.127" layer="21"/>
<wire x1="-2" y1="-2.5" x2="-2" y2="2.5" width="0.127" layer="51"/>
<wire x1="-2" y1="2.5" x2="2" y2="2.5" width="0.127" layer="51"/>
<wire x1="2" y1="2.5" x2="2" y2="-2.5" width="0.127" layer="51"/>
<wire x1="2" y1="-2.5" x2="-2" y2="-2.5" width="0.127" layer="51"/>
<circle x="-3.225" y="2.709" radius="0.25" width="0" layer="21"/>
<circle x="-0.8" y="1.75" radius="0.375" width="0" layer="21"/>
<circle x="-1" y="1.5" radius="0.5" width="0.127" layer="51"/>
<smd name="1" x="-2.7" y="1.905" dx="0.6" dy="1.55" layer="1" roundness="100" rot="R90"/>
<smd name="2" x="-2.7" y="0.635" dx="0.6" dy="1.55" layer="1" roundness="100" rot="R90"/>
<smd name="3" x="-2.7" y="-0.635" dx="0.6" dy="1.55" layer="1" roundness="100" rot="R90"/>
<smd name="4" x="-2.7" y="-1.905" dx="0.6" dy="1.55" layer="1" roundness="100" rot="R90"/>
<smd name="5" x="2.7" y="-1.905" dx="0.6" dy="1.55" layer="1" roundness="100" rot="R90"/>
<smd name="6" x="2.7" y="-0.635" dx="0.6" dy="1.55" layer="1" roundness="100" rot="R90"/>
<smd name="7" x="2.7" y="0.635" dx="0.6" dy="1.55" layer="1" roundness="100" rot="R90"/>
<smd name="8" x="2.7" y="1.905" dx="0.6" dy="1.55" layer="1" roundness="100" rot="R90"/>
<text x="-2.721" y="2.8175" size="1.27" layer="25" font="vector" ratio="10">&gt;NAME</text>
<text x="-2.721" y="-4.0875" size="1.27" layer="27" font="vector" ratio="10">&gt;VALUE</text>
<rectangle x1="-3" y1="1.6925" x2="-2" y2="2.1175" layer="51"/>
<rectangle x1="-3" y1="0.4225" x2="-2" y2="0.8475" layer="51"/>
<rectangle x1="-3" y1="-0.8475" x2="-2" y2="-0.4225" layer="51"/>
<rectangle x1="-3" y1="-2.1175" x2="-2" y2="-1.6925" layer="51"/>
<rectangle x1="2" y1="-2.1175" x2="3" y2="-1.6925" layer="51"/>
<rectangle x1="2" y1="-0.8475" x2="3" y2="-0.4225" layer="51"/>
<rectangle x1="2" y1="0.4225" x2="3" y2="0.8475" layer="51"/>
<rectangle x1="2" y1="1.6925" x2="3" y2="2.1175" layer="51"/>
</package>
</packages>
<symbols>
<symbol name="MOSFET-NCH">
<wire x1="0" y1="-2.54" x2="1.3208" y2="-2.54" width="0.1524" layer="94"/>
<wire x1="2.54" y1="0.762" x2="2.54" y2="0" width="0.254" layer="94"/>
<wire x1="2.54" y1="0" x2="2.54" y2="-0.762" width="0.254" layer="94"/>
<wire x1="2.54" y1="3.683" x2="2.54" y2="2.54" width="0.254" layer="94"/>
<wire x1="2.54" y1="2.54" x2="2.54" y2="1.397" width="0.254" layer="94"/>
<wire x1="2.54" y1="0" x2="5.08" y2="0" width="0.1524" layer="94"/>
<wire x1="5.08" y1="0" x2="5.08" y2="-2.54" width="0.1524" layer="94"/>
<wire x1="2.54" y1="-1.397" x2="2.54" y2="-2.54" width="0.254" layer="94"/>
<wire x1="2.54" y1="-2.54" x2="2.54" y2="-3.683" width="0.254" layer="94"/>
<wire x1="1.397" y1="2.54" x2="1.397" y2="-2.54" width="0.254" layer="94"/>
<wire x1="2.54" y1="2.54" x2="5.08" y2="2.54" width="0.1524" layer="94"/>
<wire x1="5.08" y1="-2.54" x2="2.54" y2="-2.54" width="0.1524" layer="94"/>
<wire x1="7.62" y1="2.54" x2="7.62" y2="0.508" width="0.1524" layer="94"/>
<wire x1="7.62" y1="0.508" x2="7.62" y2="-2.54" width="0.1524" layer="94"/>
<wire x1="5.08" y1="-2.54" x2="7.62" y2="-2.54" width="0.1524" layer="94"/>
<wire x1="5.08" y1="2.54" x2="7.62" y2="2.54" width="0.1524" layer="94"/>
<wire x1="8.128" y1="0.508" x2="7.62" y2="0.508" width="0.1524" layer="94"/>
<wire x1="7.62" y1="0.508" x2="7.112" y2="0.508" width="0.1524" layer="94"/>
<circle x="5.08" y="-2.54" radius="0.3592" width="0" layer="94"/>
<circle x="5.08" y="2.54" radius="0.3592" width="0" layer="94"/>
<text x="-8.89" y="0" size="1.778" layer="96">&gt;VALUE</text>
<text x="-8.89" y="2.54" size="1.778" layer="95">&gt;NAME</text>
<pin name="S" x="5.08" y="-7.62" visible="off" length="middle" direction="pas" rot="R90"/>
<pin name="G" x="-2.54" y="-2.54" visible="off" length="short" direction="pas"/>
<pin name="D" x="5.08" y="7.62" visible="off" length="middle" direction="pas" rot="R270"/>
<polygon width="0.1524" layer="94">
<vertex x="7.62" y="0.508"/>
<vertex x="7.112" y="-0.254"/>
<vertex x="8.128" y="-0.254"/>
</polygon>
<polygon width="0.1524" layer="94">
<vertex x="2.794" y="0"/>
<vertex x="3.81" y="0.762"/>
<vertex x="3.81" y="-0.762"/>
</polygon>
</symbol>
</symbols>
<devicesets>
<deviceset name="MOSFET-NCH_" prefix="Q">
<gates>
<gate name="G$1" symbol="MOSFET-NCH" x="0" y="0"/>
</gates>
<devices>
<device name="SOT-23" package="SOT95P280X135-3N">
<connects>
<connect gate="G$1" pin="D" pad="3"/>
<connect gate="G$1" pin="G" pad="1"/>
<connect gate="G$1" pin="S" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="SOT-223" package="SOT230P700X160-4N">
<connects>
<connect gate="G$1" pin="D" pad="4"/>
<connect gate="G$1" pin="G" pad="1"/>
<connect gate="G$1" pin="S" pad="3"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="SO-8" package="SOIC127P600X175-8AN">
<connects>
<connect gate="G$1" pin="D" pad="5"/>
<connect gate="G$1" pin="G" pad="4"/>
<connect gate="G$1" pin="S" pad="1"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="atmel-arm">
<description>&lt;b&gt;Atmel ARM Processors&lt;/b&gt; - v1.02 (05/13/09)&lt;p&gt;
&lt;p&gt;THIS LIBRARY IS PROVIDED AS IS AND WITHOUT WARRANTY OF ANY KIND, EXPRESSED OR IMPLIED.&lt;br&gt;
USE AT YOUR OWN RISK!&lt;p&gt;
&lt;author&gt;Copyright (C) 2008-2009, Bob Starr&lt;br&gt; http://www.bobstarr.net&lt;br&gt;&lt;/author&gt;</description>
<packages>
<package name="LQFP100">
<description>&lt;b&gt;LQFP100&lt;/b&gt;</description>
<wire x1="-6.2499" y1="-7" x2="-7" y2="-6.2499" width="0.2032" layer="21"/>
<wire x1="-7" y1="-6.2499" x2="-7" y2="6.7501" width="0.2032" layer="21"/>
<wire x1="-7" y1="6.7501" x2="-6.7501" y2="7" width="0.2032" layer="21"/>
<wire x1="-6.7501" y1="7" x2="6.7501" y2="7" width="0.2032" layer="21"/>
<wire x1="6.7501" y1="7" x2="7" y2="6.7501" width="0.2032" layer="21"/>
<wire x1="7" y1="6.7501" x2="7" y2="-6.7501" width="0.2032" layer="21"/>
<wire x1="7" y1="-6.7501" x2="6.7501" y2="-7" width="0.2032" layer="21"/>
<wire x1="6.7501" y1="-7" x2="-6.2499" y2="-7" width="0.2032" layer="21"/>
<circle x="-5.75" y="-5.75" radius="0.5" width="0" layer="21"/>
<smd name="1" x="-6" y="-8" dx="1.25" dy="0.3" layer="1" roundness="75" rot="R90"/>
<smd name="2" x="-5.5001" y="-8" dx="1.25" dy="0.3" layer="1" roundness="75" rot="R90"/>
<smd name="3" x="-5" y="-8" dx="1.25" dy="0.3" layer="1" roundness="75" rot="R90"/>
<smd name="4" x="-4.5001" y="-8" dx="1.25" dy="0.3" layer="1" roundness="75" rot="R90"/>
<smd name="5" x="-4" y="-8" dx="1.25" dy="0.3" layer="1" roundness="75" rot="R90"/>
<smd name="6" x="-3.5001" y="-8" dx="1.25" dy="0.3" layer="1" roundness="75" rot="R90"/>
<smd name="7" x="-3" y="-8" dx="1.25" dy="0.3" layer="1" roundness="75" rot="R90"/>
<smd name="8" x="-2.5001" y="-8" dx="1.25" dy="0.3" layer="1" roundness="75" rot="R90"/>
<smd name="9" x="-2" y="-8" dx="1.25" dy="0.3" layer="1" roundness="75" rot="R90"/>
<smd name="10" x="-1.5001" y="-8" dx="1.25" dy="0.3" layer="1" roundness="75" rot="R90"/>
<smd name="11" x="-1" y="-8" dx="1.25" dy="0.3" layer="1" roundness="75" rot="R90"/>
<smd name="12" x="-0.5001" y="-8" dx="1.25" dy="0.3" layer="1" roundness="75" rot="R90"/>
<smd name="13" x="0" y="-8" dx="1.25" dy="0.3" layer="1" roundness="75" rot="R90"/>
<smd name="14" x="0.5001" y="-8" dx="1.25" dy="0.3" layer="1" roundness="75" rot="R90"/>
<smd name="15" x="1" y="-8" dx="1.25" dy="0.3" layer="1" roundness="75" rot="R90"/>
<smd name="16" x="1.5001" y="-8" dx="1.25" dy="0.3" layer="1" roundness="75" rot="R90"/>
<smd name="17" x="2" y="-8" dx="1.25" dy="0.3" layer="1" roundness="75" rot="R90"/>
<smd name="18" x="2.5001" y="-8" dx="1.25" dy="0.3" layer="1" roundness="75" rot="R90"/>
<smd name="19" x="3" y="-8" dx="1.25" dy="0.3" layer="1" roundness="75" rot="R90"/>
<smd name="20" x="3.5001" y="-8" dx="1.25" dy="0.3" layer="1" roundness="75" rot="R90"/>
<smd name="21" x="4" y="-8" dx="1.25" dy="0.3" layer="1" roundness="75" rot="R90"/>
<smd name="22" x="4.5001" y="-8" dx="1.25" dy="0.3" layer="1" roundness="75" rot="R90"/>
<smd name="23" x="5" y="-8" dx="1.25" dy="0.3" layer="1" roundness="75" rot="R90"/>
<smd name="24" x="5.5001" y="-8" dx="1.25" dy="0.3" layer="1" roundness="75" rot="R90"/>
<smd name="25" x="6" y="-8" dx="1.25" dy="0.3" layer="1" roundness="75" rot="R90"/>
<smd name="26" x="8" y="-6" dx="0.3" dy="1.25" layer="1" roundness="75" rot="R90"/>
<smd name="27" x="8" y="-5.5001" dx="0.3" dy="1.25" layer="1" roundness="75" rot="R90"/>
<smd name="28" x="8" y="-5" dx="0.3" dy="1.25" layer="1" roundness="75" rot="R90"/>
<smd name="29" x="8" y="-4.5001" dx="0.3" dy="1.25" layer="1" roundness="75" rot="R90"/>
<smd name="30" x="8" y="-4" dx="0.3" dy="1.25" layer="1" roundness="75" rot="R90"/>
<smd name="31" x="8" y="-3.5001" dx="0.3" dy="1.25" layer="1" roundness="75" rot="R90"/>
<smd name="32" x="8" y="-3" dx="0.3" dy="1.25" layer="1" roundness="75" rot="R90"/>
<smd name="33" x="8" y="-2.5001" dx="0.3" dy="1.25" layer="1" roundness="75" rot="R90"/>
<smd name="34" x="8" y="-2" dx="0.3" dy="1.25" layer="1" roundness="75" rot="R90"/>
<smd name="35" x="8" y="-1.5001" dx="0.3" dy="1.25" layer="1" roundness="75" rot="R90"/>
<smd name="36" x="8" y="-1" dx="0.3" dy="1.25" layer="1" roundness="75" rot="R90"/>
<smd name="37" x="8" y="-0.5001" dx="0.3" dy="1.25" layer="1" roundness="75" rot="R90"/>
<smd name="38" x="8" y="0" dx="0.3" dy="1.25" layer="1" roundness="75" rot="R90"/>
<smd name="39" x="8" y="0.5001" dx="0.3" dy="1.25" layer="1" roundness="75" rot="R90"/>
<smd name="40" x="8" y="1" dx="0.3" dy="1.25" layer="1" roundness="75" rot="R90"/>
<smd name="41" x="8" y="1.5001" dx="0.3" dy="1.25" layer="1" roundness="75" rot="R90"/>
<smd name="42" x="8" y="2" dx="0.3" dy="1.25" layer="1" roundness="75" rot="R90"/>
<smd name="43" x="8" y="2.5001" dx="0.3" dy="1.25" layer="1" roundness="75" rot="R90"/>
<smd name="44" x="8" y="3" dx="0.3" dy="1.25" layer="1" roundness="75" rot="R90"/>
<smd name="45" x="8" y="3.5001" dx="0.3" dy="1.25" layer="1" roundness="75" rot="R90"/>
<smd name="46" x="8" y="4" dx="0.3" dy="1.25" layer="1" roundness="75" rot="R90"/>
<smd name="47" x="8" y="4.5001" dx="0.3" dy="1.25" layer="1" roundness="75" rot="R90"/>
<smd name="48" x="8" y="5" dx="0.3" dy="1.25" layer="1" roundness="75" rot="R90"/>
<smd name="49" x="8" y="5.5001" dx="0.3" dy="1.25" layer="1" roundness="75" rot="R90"/>
<smd name="50" x="8" y="6" dx="0.3" dy="1.25" layer="1" roundness="75" rot="R90"/>
<smd name="51" x="6" y="8" dx="1.25" dy="0.3" layer="1" roundness="75" rot="R90"/>
<smd name="52" x="5.5001" y="8" dx="1.25" dy="0.3" layer="1" roundness="75" rot="R90"/>
<smd name="53" x="5" y="8" dx="1.25" dy="0.3" layer="1" roundness="75" rot="R90"/>
<smd name="54" x="4.5001" y="8" dx="1.25" dy="0.3" layer="1" roundness="75" rot="R90"/>
<smd name="55" x="4" y="8" dx="1.25" dy="0.3" layer="1" roundness="75" rot="R90"/>
<smd name="56" x="3.5001" y="8" dx="1.25" dy="0.3" layer="1" roundness="75" rot="R90"/>
<smd name="57" x="3" y="8" dx="1.25" dy="0.3" layer="1" roundness="75" rot="R90"/>
<smd name="58" x="2.5001" y="8" dx="1.25" dy="0.3" layer="1" roundness="75" rot="R90"/>
<smd name="59" x="2" y="8" dx="1.25" dy="0.3" layer="1" roundness="75" rot="R90"/>
<smd name="60" x="1.5001" y="8" dx="1.25" dy="0.3" layer="1" roundness="75" rot="R90"/>
<smd name="61" x="1" y="8" dx="1.25" dy="0.3" layer="1" roundness="75" rot="R90"/>
<smd name="62" x="0.5001" y="8" dx="1.25" dy="0.3" layer="1" roundness="75" rot="R90"/>
<smd name="63" x="0" y="8" dx="1.25" dy="0.3" layer="1" roundness="75" rot="R90"/>
<smd name="64" x="-0.5001" y="8" dx="1.25" dy="0.3" layer="1" roundness="75" rot="R90"/>
<smd name="65" x="-1" y="8" dx="1.25" dy="0.3" layer="1" roundness="75" rot="R90"/>
<smd name="66" x="-1.5001" y="8" dx="1.25" dy="0.3" layer="1" roundness="75" rot="R90"/>
<smd name="67" x="-2" y="8" dx="1.25" dy="0.3" layer="1" roundness="75" rot="R90"/>
<smd name="68" x="-2.5001" y="8" dx="1.25" dy="0.3" layer="1" roundness="75" rot="R90"/>
<smd name="69" x="-3" y="8" dx="1.25" dy="0.3" layer="1" roundness="75" rot="R90"/>
<smd name="70" x="-3.5001" y="8" dx="1.25" dy="0.3" layer="1" roundness="75" rot="R90"/>
<smd name="71" x="-4" y="8" dx="1.25" dy="0.3" layer="1" roundness="75" rot="R90"/>
<smd name="72" x="-4.5001" y="8" dx="1.25" dy="0.3" layer="1" roundness="75" rot="R90"/>
<smd name="73" x="-5" y="8" dx="1.25" dy="0.3" layer="1" roundness="75" rot="R90"/>
<smd name="74" x="-5.5001" y="8" dx="1.25" dy="0.3" layer="1" roundness="75" rot="R90"/>
<smd name="75" x="-6" y="8" dx="1.25" dy="0.3" layer="1" roundness="75" rot="R90"/>
<smd name="76" x="-8" y="6" dx="0.3" dy="1.25" layer="1" roundness="75" rot="R90"/>
<smd name="77" x="-8" y="5.5001" dx="0.3" dy="1.25" layer="1" roundness="75" rot="R90"/>
<smd name="78" x="-8" y="5" dx="0.3" dy="1.25" layer="1" roundness="75" rot="R90"/>
<smd name="79" x="-8" y="4.5001" dx="0.3" dy="1.25" layer="1" roundness="75" rot="R90"/>
<smd name="80" x="-8" y="4" dx="0.3" dy="1.25" layer="1" roundness="75" rot="R90"/>
<smd name="81" x="-8" y="3.5001" dx="0.3" dy="1.25" layer="1" roundness="75" rot="R90"/>
<smd name="82" x="-8" y="3" dx="0.3" dy="1.25" layer="1" roundness="75" rot="R90"/>
<smd name="83" x="-8" y="2.5001" dx="0.3" dy="1.25" layer="1" roundness="75" rot="R90"/>
<smd name="84" x="-8" y="2" dx="0.3" dy="1.25" layer="1" roundness="75" rot="R90"/>
<smd name="85" x="-8" y="1.5001" dx="0.3" dy="1.25" layer="1" roundness="75" rot="R90"/>
<smd name="86" x="-8" y="1" dx="0.3" dy="1.25" layer="1" roundness="75" rot="R90"/>
<smd name="87" x="-8" y="0.5001" dx="0.3" dy="1.25" layer="1" roundness="75" rot="R90"/>
<smd name="88" x="-8" y="0" dx="0.3" dy="1.25" layer="1" roundness="75" rot="R90"/>
<smd name="89" x="-8" y="-0.5001" dx="0.3" dy="1.25" layer="1" roundness="75" rot="R90"/>
<smd name="90" x="-8" y="-1" dx="0.3" dy="1.25" layer="1" roundness="75" rot="R90"/>
<smd name="91" x="-8" y="-1.5001" dx="0.3" dy="1.25" layer="1" roundness="75" rot="R90"/>
<smd name="92" x="-8" y="-2" dx="0.3" dy="1.25" layer="1" roundness="75" rot="R90"/>
<smd name="93" x="-8" y="-2.5001" dx="0.3" dy="1.25" layer="1" roundness="75" rot="R90"/>
<smd name="94" x="-8" y="-3" dx="0.3" dy="1.25" layer="1" roundness="75" rot="R90"/>
<smd name="95" x="-8" y="-3.5001" dx="0.3" dy="1.25" layer="1" roundness="75" rot="R90"/>
<smd name="96" x="-8" y="-4" dx="0.3" dy="1.25" layer="1" roundness="75" rot="R90"/>
<smd name="97" x="-8" y="-4.5001" dx="0.3" dy="1.25" layer="1" roundness="75" rot="R90"/>
<smd name="98" x="-8" y="-5" dx="0.3" dy="1.25" layer="1" roundness="75" rot="R90"/>
<smd name="99" x="-8" y="-5.5001" dx="0.3" dy="1.25" layer="1" roundness="75" rot="R90"/>
<smd name="100" x="-8" y="-6" dx="0.3" dy="1.25" layer="1" roundness="75" rot="R90"/>
<text x="-9.525" y="-5.715" size="1.016" layer="25" ratio="18" rot="R90">&gt;NAME</text>
<text x="-2" y="-1.5" size="0.8128" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="-6.5249" y1="-7.7251" x2="-5.4751" y2="-7.4249" layer="51" rot="R90"/>
<rectangle x1="-6.0249" y1="-7.725" x2="-4.9751" y2="-7.425" layer="51" rot="R90"/>
<rectangle x1="-5.5249" y1="-7.7251" x2="-4.4751" y2="-7.4249" layer="51" rot="R90"/>
<rectangle x1="-5.0249" y1="-7.725" x2="-3.9751" y2="-7.425" layer="51" rot="R90"/>
<rectangle x1="-4.5249" y1="-7.7251" x2="-3.4751" y2="-7.4249" layer="51" rot="R90"/>
<rectangle x1="-4.0249" y1="-7.725" x2="-2.9751" y2="-7.425" layer="51" rot="R90"/>
<rectangle x1="-3.5249" y1="-7.7251" x2="-2.4751" y2="-7.4249" layer="51" rot="R90"/>
<rectangle x1="-3.0249" y1="-7.725" x2="-1.9751" y2="-7.425" layer="51" rot="R90"/>
<rectangle x1="-2.5249" y1="-7.7251" x2="-1.4751" y2="-7.4249" layer="51" rot="R90"/>
<rectangle x1="-2.0249" y1="-7.725" x2="-0.9751" y2="-7.425" layer="51" rot="R90"/>
<rectangle x1="-1.5249" y1="-7.7251" x2="-0.4751" y2="-7.4249" layer="51" rot="R90"/>
<rectangle x1="-1.0249" y1="-7.725" x2="0.0249" y2="-7.425" layer="51" rot="R90"/>
<rectangle x1="-0.5249" y1="-7.7251" x2="0.5249" y2="-7.4249" layer="51" rot="R90"/>
<rectangle x1="-0.0249" y1="-7.725" x2="1.0249" y2="-7.425" layer="51" rot="R90"/>
<rectangle x1="0.4751" y1="-7.7251" x2="1.5249" y2="-7.4249" layer="51" rot="R90"/>
<rectangle x1="0.9751" y1="-7.725" x2="2.0249" y2="-7.425" layer="51" rot="R90"/>
<rectangle x1="1.4751" y1="-7.7251" x2="2.5249" y2="-7.4249" layer="51" rot="R90"/>
<rectangle x1="1.9751" y1="-7.725" x2="3.0249" y2="-7.425" layer="51" rot="R90"/>
<rectangle x1="2.4751" y1="-7.7251" x2="3.5249" y2="-7.4249" layer="51" rot="R90"/>
<rectangle x1="2.9751" y1="-7.725" x2="4.0249" y2="-7.425" layer="51" rot="R90"/>
<rectangle x1="3.4751" y1="-7.7251" x2="4.5249" y2="-7.4249" layer="51" rot="R90"/>
<rectangle x1="3.9751" y1="-7.725" x2="5.0249" y2="-7.425" layer="51" rot="R90"/>
<rectangle x1="4.4751" y1="-7.7251" x2="5.5249" y2="-7.4249" layer="51" rot="R90"/>
<rectangle x1="4.9751" y1="-7.725" x2="6.0249" y2="-7.425" layer="51" rot="R90"/>
<rectangle x1="5.4751" y1="-7.7251" x2="6.5249" y2="-7.4249" layer="51" rot="R90"/>
<rectangle x1="7.4249" y1="-6.5249" x2="7.7251" y2="-5.4751" layer="51" rot="R90"/>
<rectangle x1="7.425" y1="-6.0249" x2="7.725" y2="-4.9751" layer="51" rot="R90"/>
<rectangle x1="7.4249" y1="-5.5249" x2="7.7251" y2="-4.4751" layer="51" rot="R90"/>
<rectangle x1="7.425" y1="-5.0249" x2="7.725" y2="-3.9751" layer="51" rot="R90"/>
<rectangle x1="7.4249" y1="-4.5249" x2="7.7251" y2="-3.4751" layer="51" rot="R90"/>
<rectangle x1="7.425" y1="-4.0249" x2="7.725" y2="-2.9751" layer="51" rot="R90"/>
<rectangle x1="7.4249" y1="-3.5249" x2="7.7251" y2="-2.4751" layer="51" rot="R90"/>
<rectangle x1="7.425" y1="-3.0249" x2="7.725" y2="-1.9751" layer="51" rot="R90"/>
<rectangle x1="7.4249" y1="-2.5249" x2="7.7251" y2="-1.4751" layer="51" rot="R90"/>
<rectangle x1="7.425" y1="-2.0249" x2="7.725" y2="-0.9751" layer="51" rot="R90"/>
<rectangle x1="7.4249" y1="-1.5249" x2="7.7251" y2="-0.4751" layer="51" rot="R90"/>
<rectangle x1="7.425" y1="-1.0249" x2="7.725" y2="0.0249" layer="51" rot="R90"/>
<rectangle x1="7.4249" y1="-0.5249" x2="7.7251" y2="0.5249" layer="51" rot="R90"/>
<rectangle x1="7.425" y1="-0.0249" x2="7.725" y2="1.0249" layer="51" rot="R90"/>
<rectangle x1="7.4249" y1="0.4751" x2="7.7251" y2="1.5249" layer="51" rot="R90"/>
<rectangle x1="7.425" y1="0.9751" x2="7.725" y2="2.0249" layer="51" rot="R90"/>
<rectangle x1="7.4249" y1="1.4751" x2="7.7251" y2="2.5249" layer="51" rot="R90"/>
<rectangle x1="7.425" y1="1.9751" x2="7.725" y2="3.0249" layer="51" rot="R90"/>
<rectangle x1="7.4249" y1="2.4751" x2="7.7251" y2="3.5249" layer="51" rot="R90"/>
<rectangle x1="7.425" y1="2.9751" x2="7.725" y2="4.0249" layer="51" rot="R90"/>
<rectangle x1="7.4249" y1="3.4751" x2="7.7251" y2="4.5249" layer="51" rot="R90"/>
<rectangle x1="7.425" y1="3.9751" x2="7.725" y2="5.0249" layer="51" rot="R90"/>
<rectangle x1="7.4249" y1="4.4751" x2="7.7251" y2="5.5249" layer="51" rot="R90"/>
<rectangle x1="7.425" y1="4.9751" x2="7.725" y2="6.0249" layer="51" rot="R90"/>
<rectangle x1="7.4249" y1="5.4751" x2="7.7251" y2="6.5249" layer="51" rot="R90"/>
<rectangle x1="5.4751" y1="7.4249" x2="6.5249" y2="7.7251" layer="51" rot="R90"/>
<rectangle x1="4.9751" y1="7.425" x2="6.0249" y2="7.725" layer="51" rot="R90"/>
<rectangle x1="4.4751" y1="7.4249" x2="5.5249" y2="7.7251" layer="51" rot="R90"/>
<rectangle x1="3.9751" y1="7.425" x2="5.0249" y2="7.725" layer="51" rot="R90"/>
<rectangle x1="3.4751" y1="7.4249" x2="4.5249" y2="7.7251" layer="51" rot="R90"/>
<rectangle x1="2.9751" y1="7.425" x2="4.0249" y2="7.725" layer="51" rot="R90"/>
<rectangle x1="2.4751" y1="7.4249" x2="3.5249" y2="7.7251" layer="51" rot="R90"/>
<rectangle x1="1.9751" y1="7.425" x2="3.0249" y2="7.725" layer="51" rot="R90"/>
<rectangle x1="1.4751" y1="7.4249" x2="2.5249" y2="7.7251" layer="51" rot="R90"/>
<rectangle x1="0.9751" y1="7.425" x2="2.0249" y2="7.725" layer="51" rot="R90"/>
<rectangle x1="0.4751" y1="7.4249" x2="1.5249" y2="7.7251" layer="51" rot="R90"/>
<rectangle x1="-0.0249" y1="7.425" x2="1.0249" y2="7.725" layer="51" rot="R90"/>
<rectangle x1="-0.5249" y1="7.4249" x2="0.5249" y2="7.7251" layer="51" rot="R90"/>
<rectangle x1="-1.0249" y1="7.425" x2="0.0249" y2="7.725" layer="51" rot="R90"/>
<rectangle x1="-1.5249" y1="7.4249" x2="-0.4751" y2="7.7251" layer="51" rot="R90"/>
<rectangle x1="-2.0249" y1="7.425" x2="-0.9751" y2="7.725" layer="51" rot="R90"/>
<rectangle x1="-2.5249" y1="7.4249" x2="-1.4751" y2="7.7251" layer="51" rot="R90"/>
<rectangle x1="-3.0249" y1="7.425" x2="-1.9751" y2="7.725" layer="51" rot="R90"/>
<rectangle x1="-3.5249" y1="7.4249" x2="-2.4751" y2="7.7251" layer="51" rot="R90"/>
<rectangle x1="-4.0249" y1="7.425" x2="-2.9751" y2="7.725" layer="51" rot="R90"/>
<rectangle x1="-4.5249" y1="7.4249" x2="-3.4751" y2="7.7251" layer="51" rot="R90"/>
<rectangle x1="-5.0249" y1="7.425" x2="-3.9751" y2="7.725" layer="51" rot="R90"/>
<rectangle x1="-5.5249" y1="7.4249" x2="-4.4751" y2="7.7251" layer="51" rot="R90"/>
<rectangle x1="-6.0249" y1="7.425" x2="-4.9751" y2="7.725" layer="51" rot="R90"/>
<rectangle x1="-6.5249" y1="7.4249" x2="-5.4751" y2="7.7251" layer="51" rot="R90"/>
<rectangle x1="-7.7251" y1="5.4751" x2="-7.4249" y2="6.5249" layer="51" rot="R90"/>
<rectangle x1="-7.725" y1="4.9751" x2="-7.425" y2="6.0249" layer="51" rot="R90"/>
<rectangle x1="-7.7251" y1="4.4751" x2="-7.4249" y2="5.5249" layer="51" rot="R90"/>
<rectangle x1="-7.725" y1="3.9751" x2="-7.425" y2="5.0249" layer="51" rot="R90"/>
<rectangle x1="-7.7251" y1="3.4751" x2="-7.4249" y2="4.5249" layer="51" rot="R90"/>
<rectangle x1="-7.725" y1="2.9751" x2="-7.425" y2="4.0249" layer="51" rot="R90"/>
<rectangle x1="-7.7251" y1="2.4751" x2="-7.4249" y2="3.5249" layer="51" rot="R90"/>
<rectangle x1="-7.725" y1="1.9751" x2="-7.425" y2="3.0249" layer="51" rot="R90"/>
<rectangle x1="-7.7251" y1="1.4751" x2="-7.4249" y2="2.5249" layer="51" rot="R90"/>
<rectangle x1="-7.725" y1="0.9751" x2="-7.425" y2="2.0249" layer="51" rot="R90"/>
<rectangle x1="-7.7251" y1="0.4751" x2="-7.4249" y2="1.5249" layer="51" rot="R90"/>
<rectangle x1="-7.725" y1="-0.0249" x2="-7.425" y2="1.0249" layer="51" rot="R90"/>
<rectangle x1="-7.7251" y1="-0.5249" x2="-7.4249" y2="0.5249" layer="51" rot="R90"/>
<rectangle x1="-7.725" y1="-1.0249" x2="-7.425" y2="0.0249" layer="51" rot="R90"/>
<rectangle x1="-7.7251" y1="-1.5249" x2="-7.4249" y2="-0.4751" layer="51" rot="R90"/>
<rectangle x1="-7.725" y1="-2.0249" x2="-7.425" y2="-0.9751" layer="51" rot="R90"/>
<rectangle x1="-7.7251" y1="-2.5249" x2="-7.4249" y2="-1.4751" layer="51" rot="R90"/>
<rectangle x1="-7.725" y1="-3.0249" x2="-7.425" y2="-1.9751" layer="51" rot="R90"/>
<rectangle x1="-7.7251" y1="-3.5249" x2="-7.4249" y2="-2.4751" layer="51" rot="R90"/>
<rectangle x1="-7.725" y1="-4.0249" x2="-7.425" y2="-2.9751" layer="51" rot="R90"/>
<rectangle x1="-7.7251" y1="-4.5249" x2="-7.4249" y2="-3.4751" layer="51" rot="R90"/>
<rectangle x1="-7.725" y1="-5.0249" x2="-7.425" y2="-3.9751" layer="51" rot="R90"/>
<rectangle x1="-7.7251" y1="-5.5249" x2="-7.4249" y2="-4.4751" layer="51" rot="R90"/>
<rectangle x1="-7.725" y1="-6.0249" x2="-7.425" y2="-4.9751" layer="51" rot="R90"/>
<rectangle x1="-7.7251" y1="-6.5249" x2="-7.4249" y2="-5.4751" layer="51" rot="R90"/>
</package>
</packages>
<symbols>
<symbol name="AT91SAM7X512/256/128">
<wire x1="25.4" y1="81.28" x2="25.4" y2="-81.28" width="0.4064" layer="94"/>
<wire x1="25.4" y1="-81.28" x2="-25.4" y2="-81.28" width="0.4064" layer="94"/>
<wire x1="-25.4" y1="-81.28" x2="-25.4" y2="81.28" width="0.4064" layer="94"/>
<wire x1="-25.4" y1="81.28" x2="25.4" y2="81.28" width="0.4064" layer="94"/>
<text x="-25.4" y="82.55" size="1.778" layer="95">&gt;NAME</text>
<text x="-25.4" y="-83.82" size="1.778" layer="96">&gt;VALUE</text>
<pin name="ADVREF" x="-30.48" y="-12.7" length="middle"/>
<pin name="GND@2" x="-30.48" y="-63.5" length="middle" direction="pwr"/>
<pin name="AD4" x="-30.48" y="22.86" length="middle"/>
<pin name="AD5" x="-30.48" y="20.32" length="middle"/>
<pin name="AD6" x="-30.48" y="17.78" length="middle"/>
<pin name="AD7" x="-30.48" y="15.24" length="middle"/>
<pin name="VDDIN" x="-30.48" y="-38.1" length="middle" direction="pwr"/>
<pin name="VDDOUT" x="-30.48" y="-40.64" length="middle"/>
<pin name="SPI0_MOSI/PA17" x="30.48" y="35.56" length="middle" rot="R180"/>
<pin name="SPI0_SPCK/PA18" x="30.48" y="33.02" length="middle" rot="R180"/>
<pin name="SPI1_NPCS0/TF/PA21" x="30.48" y="25.4" length="middle" rot="R180"/>
<pin name="VDDCORE@15" x="-30.48" y="-50.8" length="middle"/>
<pin name="CANRX/PA19" x="30.48" y="30.48" length="middle" rot="R180"/>
<pin name="SPI1_SPCK/TK/PA22" x="30.48" y="22.86" length="middle" rot="R180"/>
<pin name="SPI1_MOSI/TD/PA23" x="30.48" y="20.32" length="middle" rot="R180"/>
<pin name="CANTX/PA20" x="30.48" y="27.94" length="middle" rot="R180"/>
<pin name="GND@16" x="-30.48" y="-66.04" length="middle" direction="pwr"/>
<pin name="VDDIO@17" x="-30.48" y="-17.78" length="middle" direction="pwr"/>
<pin name="SPI0_MISO/PA16" x="30.48" y="38.1" length="middle" rot="R180"/>
<pin name="TCLK2/SPI0_NPCS3/PA15" x="30.48" y="40.64" length="middle" rot="R180"/>
<pin name="IRQ1/SPI0_NPCS2/PA14" x="30.48" y="43.18" length="middle" rot="R180"/>
<pin name="PCK1/SPI0_NPCS1/PA13" x="30.48" y="45.72" length="middle" rot="R180"/>
<pin name="SPI1_MISO/RD/PA24" x="30.48" y="17.78" length="middle" rot="R180"/>
<pin name="VDDCORE@37" x="-30.48" y="-53.34" length="middle"/>
<pin name="SPI1_NPCS1/RK/PA25" x="30.48" y="15.24" length="middle" rot="R180"/>
<pin name="SPI1_NPCS2/RF/PA26" x="30.48" y="12.7" length="middle" rot="R180"/>
<pin name="SPI_NPCS0/PA12" x="30.48" y="48.26" length="middle" rot="R180"/>
<pin name="TWCK/PA11" x="30.48" y="50.8" length="middle" rot="R180"/>
<pin name="TWD/PA10" x="30.48" y="53.34" length="middle" rot="R180"/>
<pin name="SPI0_NPCS3/CTS1/PA9" x="30.48" y="55.88" length="middle" rot="R180"/>
<pin name="SPI0_NPCS2/RTS1/PA8" x="30.48" y="58.42" length="middle" rot="R180"/>
<pin name="SPI0_NPCS1/SCK1/PA7" x="30.48" y="60.96" length="middle" rot="R180"/>
<pin name="TDI" x="-30.48" y="-2.54" length="middle"/>
<pin name="TXD1/PA6" x="30.48" y="63.5" length="middle" rot="R180"/>
<pin name="RXD1/PA5" x="30.48" y="66.04" length="middle" rot="R180"/>
<pin name="SPI1_NPCS3/CTS0/PA4" x="30.48" y="68.58" length="middle" rot="R180"/>
<pin name="DRXD/PCK3/PA27" x="30.48" y="10.16" length="middle" rot="R180"/>
<pin name="DTXD/PA28" x="30.48" y="7.62" length="middle" rot="R180"/>
<pin name="NRST" x="-30.48" y="78.74" length="middle"/>
<pin name="TST" x="-30.48" y="58.42" length="middle"/>
<pin name="SPI1_NPCS3/FIQ/PA29" x="30.48" y="5.08" length="middle" rot="R180"/>
<pin name="PCK2/IRQ0/PA30" x="30.48" y="2.54" length="middle" rot="R180"/>
<pin name="SPI1_NPCS2/RTS0/PA3" x="30.48" y="71.12" length="middle" rot="R180"/>
<pin name="SPI1_NPCS1/SCK0/PA2" x="30.48" y="73.66" length="middle" rot="R180"/>
<pin name="VDDIO@33" x="-30.48" y="-20.32" length="middle" direction="pwr"/>
<pin name="GND@32" x="-30.48" y="-68.58" length="middle" direction="pwr"/>
<pin name="TXD0/PA1" x="30.48" y="76.2" length="middle" rot="R180"/>
<pin name="RXD0/PA0" x="30.48" y="78.74" length="middle" rot="R180"/>
<pin name="TDO" x="-30.48" y="-7.62" length="middle"/>
<pin name="JTAGSEL" x="-30.48" y="5.08" length="middle"/>
<pin name="TMS" x="-30.48" y="-5.08" length="middle"/>
<pin name="TCK" x="-30.48" y="0" length="middle"/>
<pin name="VDDCORE@62" x="-30.48" y="-55.88" length="middle"/>
<pin name="ERASE" x="-30.48" y="53.34" length="middle"/>
<pin name="DDM" x="-30.48" y="38.1" length="middle"/>
<pin name="DDP" x="-30.48" y="33.02" length="middle"/>
<pin name="VDDIO@48" x="-30.48" y="-22.86" length="middle" direction="pwr"/>
<pin name="VDDFLASH" x="-30.48" y="-33.02" length="middle" direction="pwr"/>
<pin name="GND@52" x="-30.48" y="-71.12" length="middle" direction="pwr"/>
<pin name="XOUT" x="-30.48" y="63.5" length="middle"/>
<pin name="XIN/PGMCK" x="-30.48" y="73.66" length="middle"/>
<pin name="PLLRC" x="-30.48" y="48.26" length="middle"/>
<pin name="VDDPLL" x="-30.48" y="-45.72" length="middle"/>
<pin name="PCK0/ETXCK/EREFCK/PB0" x="30.48" y="-2.54" length="middle" rot="R180"/>
<pin name="ETXEN/PB1" x="30.48" y="-5.08" length="middle" rot="R180"/>
<pin name="ETX0/PB2" x="30.48" y="-7.62" length="middle" rot="R180"/>
<pin name="ETX1/PB3" x="30.48" y="-10.16" length="middle" rot="R180"/>
<pin name="ECRS/PB4" x="30.48" y="-12.7" length="middle" rot="R180"/>
<pin name="ERX0/PB5" x="30.48" y="-15.24" length="middle" rot="R180"/>
<pin name="ERX1/PB6" x="30.48" y="-17.78" length="middle" rot="R180"/>
<pin name="ERXER/PB7" x="30.48" y="-20.32" length="middle" rot="R180"/>
<pin name="EMDC/PB8" x="30.48" y="-22.86" length="middle" rot="R180"/>
<pin name="EMDIO/PB9" x="30.48" y="-25.4" length="middle" rot="R180"/>
<pin name="SPI1_NPCS1/ETX2/PB10" x="30.48" y="-27.94" length="middle" rot="R180"/>
<pin name="SPI1_NPCS2/ETX3/PB11" x="30.48" y="-30.48" length="middle" rot="R180"/>
<pin name="TCLK0/ETXER/PB12" x="30.48" y="-33.02" length="middle" rot="R180"/>
<pin name="SPI0_NPCS1/ERX2/PB13" x="30.48" y="-35.56" length="middle" rot="R180"/>
<pin name="SPI0_NPCS2/ERX3/PB14" x="30.48" y="-38.1" length="middle" rot="R180"/>
<pin name="ERXDV/ECRSDV/PB15" x="30.48" y="-40.64" length="middle" rot="R180"/>
<pin name="SP1_NPCS3/ECOL/PB16" x="30.48" y="-43.18" length="middle" rot="R180"/>
<pin name="SPI0_NPCS3/ERXCK/PB17" x="30.48" y="-45.72" length="middle" rot="R180"/>
<pin name="ADTRG/EF100/PB18" x="30.48" y="-48.26" length="middle" rot="R180"/>
<pin name="TCLK1/PWM0/PB19" x="30.48" y="-50.8" length="middle" rot="R180"/>
<pin name="PCK0/PWM1/PB20" x="30.48" y="-53.34" length="middle" rot="R180"/>
<pin name="PWM2/PCK1/PB21" x="30.48" y="-55.88" length="middle" rot="R180"/>
<pin name="PCK2/PWM3/PB22" x="30.48" y="-58.42" length="middle" rot="R180"/>
<pin name="DCD1/TIOA0/PB23" x="30.48" y="-60.96" length="middle" rot="R180"/>
<pin name="DSR1/TIOB0/PB24" x="30.48" y="-63.5" length="middle" rot="R180"/>
<pin name="DTR1/TIOA1/PB25" x="30.48" y="-66.04" length="middle" rot="R180"/>
<pin name="RI1/TIOB1/PB26" x="30.48" y="-68.58" length="middle" rot="R180"/>
<pin name="AD0/PWM0/TIOA2/PB27" x="30.48" y="-71.12" length="middle" rot="R180"/>
<pin name="AD1/PWM1/TIOB2/PB28" x="30.48" y="-73.66" length="middle" rot="R180"/>
<pin name="AD2/PWM2/PCK1/PB29" x="30.48" y="-76.2" length="middle" rot="R180"/>
<pin name="AD3/PWM3/PCK2/PB30" x="30.48" y="-78.74" length="middle" rot="R180"/>
<pin name="VDDIO@61" x="-30.48" y="-25.4" length="middle" direction="pwr"/>
<pin name="GND@68" x="-30.48" y="-73.66" length="middle" direction="pwr"/>
<pin name="GND@83" x="-30.48" y="-76.2" length="middle" direction="pwr"/>
<pin name="VDDIO@84" x="-30.48" y="-27.94" length="middle" direction="pwr"/>
<pin name="VDDCORE@87" x="-30.48" y="-58.42" length="middle"/>
<pin name="GND@96" x="-30.48" y="-78.74" length="middle" direction="pwr"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="AT91SAM7X512/256/128" prefix="U">
<description>AT91SAM7X512/256/128/64/321&lt;p&gt;
ARM7 Processor</description>
<gates>
<gate name="G$1" symbol="AT91SAM7X512/256/128" x="0" y="0"/>
</gates>
<devices>
<device name="-AU" package="LQFP100">
<connects>
<connect gate="G$1" pin="AD0/PWM0/TIOA2/PB27" pad="9"/>
<connect gate="G$1" pin="AD1/PWM1/TIOB2/PB28" pad="10"/>
<connect gate="G$1" pin="AD2/PWM2/PCK1/PB29" pad="11"/>
<connect gate="G$1" pin="AD3/PWM3/PCK2/PB30" pad="12"/>
<connect gate="G$1" pin="AD4" pad="3"/>
<connect gate="G$1" pin="AD5" pad="4"/>
<connect gate="G$1" pin="AD6" pad="5"/>
<connect gate="G$1" pin="AD7" pad="6"/>
<connect gate="G$1" pin="ADTRG/EF100/PB18" pad="63"/>
<connect gate="G$1" pin="ADVREF" pad="1"/>
<connect gate="G$1" pin="CANRX/PA19" pad="46"/>
<connect gate="G$1" pin="CANTX/PA20" pad="47"/>
<connect gate="G$1" pin="DCD1/TIOA0/PB23" pad="69"/>
<connect gate="G$1" pin="DDM" pad="93"/>
<connect gate="G$1" pin="DDP" pad="94"/>
<connect gate="G$1" pin="DRXD/PCK3/PA27" pad="73"/>
<connect gate="G$1" pin="DSR1/TIOB0/PB24" pad="70"/>
<connect gate="G$1" pin="DTR1/TIOA1/PB25" pad="71"/>
<connect gate="G$1" pin="DTXD/PA28" pad="74"/>
<connect gate="G$1" pin="ECRS/PB4" pad="54"/>
<connect gate="G$1" pin="EMDC/PB8" pad="28"/>
<connect gate="G$1" pin="EMDIO/PB9" pad="27"/>
<connect gate="G$1" pin="ERASE" pad="92"/>
<connect gate="G$1" pin="ERX0/PB5" pad="34"/>
<connect gate="G$1" pin="ERX1/PB6" pad="31"/>
<connect gate="G$1" pin="ERXDV/ECRSDV/PB15" pad="35"/>
<connect gate="G$1" pin="ERXER/PB7" pad="38"/>
<connect gate="G$1" pin="ETX0/PB2" pad="42"/>
<connect gate="G$1" pin="ETX1/PB3" pad="43"/>
<connect gate="G$1" pin="ETXEN/PB1" pad="41"/>
<connect gate="G$1" pin="GND@16" pad="16"/>
<connect gate="G$1" pin="GND@2" pad="2"/>
<connect gate="G$1" pin="GND@32" pad="32"/>
<connect gate="G$1" pin="GND@52" pad="52"/>
<connect gate="G$1" pin="GND@68" pad="68"/>
<connect gate="G$1" pin="GND@83" pad="83"/>
<connect gate="G$1" pin="GND@96" pad="96"/>
<connect gate="G$1" pin="IRQ1/SPI0_NPCS2/PA14" pad="22"/>
<connect gate="G$1" pin="JTAGSEL" pad="77"/>
<connect gate="G$1" pin="NRST" pad="57"/>
<connect gate="G$1" pin="PCK0/ETXCK/EREFCK/PB0" pad="40"/>
<connect gate="G$1" pin="PCK0/PWM1/PB20" pad="65"/>
<connect gate="G$1" pin="PCK1/SPI0_NPCS1/PA13" pad="21"/>
<connect gate="G$1" pin="PCK2/IRQ0/PA30" pad="80"/>
<connect gate="G$1" pin="PCK2/PWM3/PB22" pad="67"/>
<connect gate="G$1" pin="PLLRC" pad="99"/>
<connect gate="G$1" pin="PWM2/PCK1/PB21" pad="66"/>
<connect gate="G$1" pin="RI1/TIOB1/PB26" pad="72"/>
<connect gate="G$1" pin="RXD0/PA0" pad="81"/>
<connect gate="G$1" pin="RXD1/PA5" pad="89"/>
<connect gate="G$1" pin="SP1_NPCS3/ECOL/PB16" pad="53"/>
<connect gate="G$1" pin="SPI0_MISO/PA16" pad="24"/>
<connect gate="G$1" pin="SPI0_MOSI/PA17" pad="25"/>
<connect gate="G$1" pin="SPI0_NPCS1/ERX2/PB13" pad="30"/>
<connect gate="G$1" pin="SPI0_NPCS1/SCK1/PA7" pad="91"/>
<connect gate="G$1" pin="SPI0_NPCS2/ERX3/PB14" pad="29"/>
<connect gate="G$1" pin="SPI0_NPCS2/RTS1/PA8" pad="13"/>
<connect gate="G$1" pin="SPI0_NPCS3/CTS1/PA9" pad="14"/>
<connect gate="G$1" pin="SPI0_NPCS3/ERXCK/PB17" pad="36"/>
<connect gate="G$1" pin="SPI0_SPCK/PA18" pad="26"/>
<connect gate="G$1" pin="SPI1_MISO/RD/PA24" pad="56"/>
<connect gate="G$1" pin="SPI1_MOSI/TD/PA23" pad="55"/>
<connect gate="G$1" pin="SPI1_NPCS0/TF/PA21" pad="49"/>
<connect gate="G$1" pin="SPI1_NPCS1/ETX2/PB10" pad="44"/>
<connect gate="G$1" pin="SPI1_NPCS1/RK/PA25" pad="59"/>
<connect gate="G$1" pin="SPI1_NPCS1/SCK0/PA2" pad="86"/>
<connect gate="G$1" pin="SPI1_NPCS2/ETX3/PB11" pad="45"/>
<connect gate="G$1" pin="SPI1_NPCS2/RF/PA26" pad="60"/>
<connect gate="G$1" pin="SPI1_NPCS2/RTS0/PA3" pad="85"/>
<connect gate="G$1" pin="SPI1_NPCS3/CTS0/PA4" pad="88"/>
<connect gate="G$1" pin="SPI1_NPCS3/FIQ/PA29" pad="75"/>
<connect gate="G$1" pin="SPI1_SPCK/TK/PA22" pad="50"/>
<connect gate="G$1" pin="SPI_NPCS0/PA12" pad="20"/>
<connect gate="G$1" pin="TCK" pad="79"/>
<connect gate="G$1" pin="TCLK0/ETXER/PB12" pad="39"/>
<connect gate="G$1" pin="TCLK1/PWM0/PB19" pad="64"/>
<connect gate="G$1" pin="TCLK2/SPI0_NPCS3/PA15" pad="23"/>
<connect gate="G$1" pin="TDI" pad="51"/>
<connect gate="G$1" pin="TDO" pad="76"/>
<connect gate="G$1" pin="TMS" pad="78"/>
<connect gate="G$1" pin="TST" pad="58"/>
<connect gate="G$1" pin="TWCK/PA11" pad="19"/>
<connect gate="G$1" pin="TWD/PA10" pad="18"/>
<connect gate="G$1" pin="TXD0/PA1" pad="82"/>
<connect gate="G$1" pin="TXD1/PA6" pad="90"/>
<connect gate="G$1" pin="VDDCORE@15" pad="15"/>
<connect gate="G$1" pin="VDDCORE@37" pad="37"/>
<connect gate="G$1" pin="VDDCORE@62" pad="62"/>
<connect gate="G$1" pin="VDDCORE@87" pad="87"/>
<connect gate="G$1" pin="VDDFLASH" pad="95"/>
<connect gate="G$1" pin="VDDIN" pad="8"/>
<connect gate="G$1" pin="VDDIO@17" pad="17"/>
<connect gate="G$1" pin="VDDIO@33" pad="33"/>
<connect gate="G$1" pin="VDDIO@48" pad="48"/>
<connect gate="G$1" pin="VDDIO@61" pad="61"/>
<connect gate="G$1" pin="VDDIO@84" pad="84"/>
<connect gate="G$1" pin="VDDOUT" pad="7"/>
<connect gate="G$1" pin="VDDPLL" pad="100"/>
<connect gate="G$1" pin="XIN/PGMCK" pad="97"/>
<connect gate="G$1" pin="XOUT" pad="98"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
</libraries>
<attributes>
</attributes>
<variantdefs>
</variantdefs>
<classes>
<class number="0" name="default" width="0" drill="0">
</class>
</classes>
<parts>
<part name="IC1" library="memory-micron" deviceset="MT29F8G08DAA" device=""/>
<part name="MOTORBOARD" library="con-lsta" deviceset="FE10-2" device=""/>
<part name="USB" library="con-cypressindustries" deviceset="MINI-USB_4P-" device="85-32004-10X"/>
<part name="UART" library="con-cypressindustries" deviceset="MINI-USB_SHIELD5P2-" device="32005-601"/>
<part name="IC2" library="ftdichip" deviceset="FT232R" device="L"/>
<part name="U1" library="SparkFun-Sensors" deviceset="MPL3115A2" device="LGA8"/>
<part name="U$2" library="SparkFun-Sensors" deviceset="HMC1052L" device=""/>
<part name="GPS" library="Ava" deviceset="UBLOX_LEA-6" device="" value="GPS"/>
<part name="RF7020" library="con-lsta" deviceset="FE05-1" device=""/>
<part name="6DOF" library="con-lsta" deviceset="FE05-1" device=""/>
<part name="LIDAR" library="con-lsta" deviceset="FE06-2" device=""/>
<part name="SV5" library="con-lsta" deviceset="FE05-2" device=""/>
<part name="ANT1" library="wuerth_elektronik_eisos__v6.0" deviceset="WE-MCA" device="7488910245"/>
<part name="Q1" library="special" deviceset="XTAL/S" device=""/>
<part name="U2" library="Alliance_Memory_By_element14_Batch_1" deviceset="AS6C4008-55SIN" device=""/>
<part name="Q2" library="ipc-7351-transistor" deviceset="MOSFET-NCH_" device="SOT-223"/>
<part name="U3" library="atmel-arm" deviceset="AT91SAM7X512/256/128" device="-AU"/>
</parts>
<sheets>
<sheet>
<plain>
<frame x1="10.16" y1="5.08" x2="375.92" y2="238.76" columns="8" rows="5" layer="91"/>
</plain>
<instances>
<instance part="IC1" gate="G$1" x="294.64" y="152.4"/>
<instance part="MOTORBOARD" gate="G$1" x="66.04" y="124.46"/>
<instance part="USB" gate="G$1" x="43.18" y="81.28" rot="R180"/>
<instance part="UART" gate="G41" x="43.18" y="55.88" rot="R180"/>
<instance part="IC2" gate="1" x="78.74" y="53.34"/>
<instance part="U1" gate="G$1" x="279.4" y="91.44"/>
<instance part="U$2" gate="G$1" x="269.24" y="40.64"/>
<instance part="GPS" gate="G$1" x="58.42" y="195.58"/>
<instance part="RF7020" gate="G$1" x="284.48" y="215.9" rot="R180"/>
<instance part="6DOF" gate="G$1" x="287.02" y="195.58" rot="R180"/>
<instance part="LIDAR" gate="G$1" x="350.52" y="50.8"/>
<instance part="SV5" gate="G$1" x="228.6" y="68.58"/>
<instance part="ANT1" gate="G$1" x="20.32" y="162.56"/>
<instance part="Q1" gate="G$1" x="157.48" y="45.72"/>
<instance part="U2" gate="A" x="248.92" y="170.18"/>
<instance part="Q2" gate="G$1" x="327.66" y="66.04"/>
<instance part="U3" gate="G$1" x="144.78" y="142.24"/>
</instances>
<busses>
</busses>
<nets>
</nets>
</sheet>
</sheets>
</schematic>
</drawing>
</eagle>
