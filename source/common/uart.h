#ifndef UART_H
#define UART_H

#include <stdint.h>

#define F_CPU 16000000UL
#define UARTRX_BUFSIZE 20



void uart_Init(uint16_t baudrate);
void uart_SendChar(char byte);
void uart_SendString(const char* str);
uint8_t uart_Receive(char* target, uint8_t amount);
uint8_t uart_BufferDataAmountGet(void);
uint8_t uart_BufferStatusGet(void);
void uart_BufferFlush(void);






#endif