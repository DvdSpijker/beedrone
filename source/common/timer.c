/*
 * timer.c
 *
 * Created: 25-1-2017 20:08:57
 *  Author: Dorus
 */

#include <stdlib.h>
#include <stdint.h>
#include <avr/io.h>
#include <avr/interrupt.h>
#include <math.h>

#include "timer.h"


struct _tmr {
    uint8_t timer_prescaler;
    volatile uint16_t delay_counter;
    Timer0Callback_t timer_callback;
};

struct _tmr timer0;


int8_t timer_Init(uint16_t frequency, Timer0Callback_t callback)
{
    DDRB |= (1 << PINB4);

    if (callback == NULL) {
        return -1;
    }

    timer0.timer_callback = callback;
    timer0.timer_prescaler = (1 << CS01) | (1 << CS00);
    TCCR0A |= (1 << WGM01); //Se t CTC Bit

    uint16_t ctc_val = (uint16_t)floor( (double)(F_CPU / (double)64) / frequency) - 1;
    if(ctc_val > 255) {
        return -2;
    } else {
        OCR0A = (uint8_t)ctc_val;
    }

    TIMSK0 |= (1 << OCIE0A); //Setup interrupt
    TCNT0 = 0x00;
    TIFR0 |= (1 << OCF0A);


    sei();

    return 0;
}


void timer_Start(void)
{
    TCCR0B |= timer0.timer_prescaler;
}

void timer_Stop(void)
{
    TCCR0B &= ~(timer0.timer_prescaler);
}


ISR(TIMER0_COMPA_vect)
{

    timer0.timer_callback();
}