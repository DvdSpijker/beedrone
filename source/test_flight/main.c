/*
 * FirstFlight_Firmware.c
 *
 * Created: 12-2-2017 15:03:00
 * Author : Dorus
 */

#include <avr/io.h>
#include <stdlib.h>
#include <stdint.h>
#include <stdbool.h>
#include <stdio.h>

static int putch(char c, FILE *stream);
static FILE mystdout = FDEV_SETUP_STREAM(putch, NULL, _FDEV_SETUP_WRITE);

#include "timer.h"
#include "uart.h"
#include "pwmgen.h"

#include "MPU6050/MPU6050.h"

#define PWM_M_1 PINB0
#define PWM_M_2 PINB4
#define PWM_M_3 PINB2
#define PWM_M_4 PINB3

#define UART_TX PIND0
#define UART_RX PIND1

uint8_t PwmGenChM1;
uint8_t PwmGenChM2;
uint8_t PwmGenChM3;
uint8_t PwmGenChM4;

void ReadParseCommand(void);
void MotorsStopAll(void);

static int putch(char c, FILE *stream)
{
    uart_SendChar(c);
    return 1;
}

int main(void)
{
    stdout = &mystdout;
    DDRB |= (1 << PWM_M_1) | (1 << PWM_M_2) | (1 << PWM_M_3) | (1 << PWM_M_4);

    DDRD |= (1 << UART_TX);
    DDRB &= ~(1 << UART_RX);

    uart_Init(9600);
    uart_SendString("BeeDrone> UART initialized\n");

    // MPU6050
    MPU6050(MPU6050_ADDRESS_AD0_LOW);

    // Initialize device
    printf("Initializing I2C devices...\r\n");
    MPU6050_initialize();

    // Verify connection
    printf("Testing device connections...\r\n");
    printf(MPU6050_testConnection() ? "MPU6050 connection successful\r\n" :
           "MPU6050 connection failed\r\n");

    printf("Reading internal sensor offsets...\r\n");
    printf("%d\t", MPU6050_getXAccelOffset());
    printf("%d\t", MPU6050_getYAccelOffset());
    printf("%d\t", MPU6050_getZAccelOffset());
    printf("%d\t", MPU6050_getXGyroOffset());
    printf("%d\t", MPU6050_getYGyroOffset());
    printf("%d\t\n", MPU6050_getZGyroOffset());

    int8_t result = PwmGenInit(50);
    if(result == 0) {
        uart_SendString("BeeDrone> PWMGen initialized\n");

        PwmGenChM1 = PwmGenChannelCreate(50, 1, &PORTB, PWM_M_1);
        if(PwmGenChM1  != INVALID_CHANNEL) {
            uart_SendString("BeeDrone> PWMGen Channel M 1 created\n");
            PwmGenChannelStop(PwmGenChM1);
        } else {
            uart_SendString("BeeDrone> PWMGen Channel M 1 creation failed!\n");
        }

        PwmGenChM2 = PwmGenChannelCreate(50, 1, &PORTB, PWM_M_2);
        if(PwmGenChM2 != INVALID_CHANNEL) {
            uart_SendString("BeeDrone> PWMGen Channel M 2 created\n");
            PwmGenChannelStop(PwmGenChM2);
        } else {
            uart_SendString("BeeDrone> PWMGen Channel M 2 creation failed!\n");
        }

        PwmGenChM3 = PwmGenChannelCreate(50, 1, &PORTB, PWM_M_3);
        if(PwmGenChM3 != INVALID_CHANNEL) {
            uart_SendString("BeeDrone> PWMGen Channel M 3 created\n");
            PwmGenChannelStop(PwmGenChM3);
        } else {
            uart_SendString("BeeDrone> PWMGen Channel M 3 creation failed!\n");
        }

        PwmGenChM4 = PwmGenChannelCreate(50, 1, &PORTB, PWM_M_4);
        if(PwmGenChM4 != INVALID_CHANNEL) {
            uart_SendString("BeeDrone> PWMGen Channel M 4 created\n");
            PwmGenChannelStop(PwmGenChM4);
        } else {
            uart_SendString("BeeDrone> PWMGen Channel M 4 creation failed!\n");
        }

        uart_SendString("BeeDrone> PWM channels initialized\n");

    } else {
        uart_SendString("BeeDrone> PWMGen initialization failed!\n");
    }

    int16_t ax, ay, az, gx, gy, gz;
    while (1) {

        // Read raw accel/gyro measurements from device
        MPU6050_getMotion6(&ax, &ay, &az, &gx, &gy, &gz);

        // Display tab-separated accel/gyro x/y/z values
        printf("a/g:\t%d\t%d\t%d\t%d\t%d\t%d\r\n", ax, ay, az, gx, gy, gz);

        /* Parse commands from UART interface. */
        ReadParseCommand();

    }
}


void MotorsStopAll(void)
{
    PwmGenChannelStop(PwmGenChM1);
    PwmGenChannelStop(PwmGenChM2);
    PwmGenChannelStop(PwmGenChM3);
    PwmGenChannelStop(PwmGenChM4);
}

void ReadParseCommand(void)
{
    static char rx_buf[20];
    static uint8_t n_rx = 0;
    static uint8_t dc;

    if(uart_BufferDataAmountGet() >= 3) {
        n_rx = uart_Receive(rx_buf, 3);
        if (n_rx == 3) {
            dc = (rx_buf[0] - 48) * 100 + (rx_buf[1] - 48) * 10 + (rx_buf[2] - 48);
            if(dc >= 1 && dc <= 100) {
                PwmGenChannelStart(PwmGenChM1);
                PwmGenChannelStart(PwmGenChM2);
                PwmGenChannelStart(PwmGenChM3);
                PwmGenChannelStart(PwmGenChM4);

                uart_SendString("BeeDrone> PWMGen duty cycle set: ");
                itoa(dc, rx_buf, 10);
                uart_SendString(rx_buf);
                uart_SendString("\n");
                if(dc <= 15) {
                    PwmGenChannelDutyCycleSet(PwmGenChM1, dc);
                    PwmGenChannelDutyCycleSet(PwmGenChM2, dc);
                    PwmGenChannelDutyCycleSet(PwmGenChM3, dc);
                    PwmGenChannelDutyCycleSet(PwmGenChM4, dc);
                } else {
                    MotorsStopAll();
                    uart_SendString("BeeDrone> PWMGen channels stopped.\n");
                }

            } else if(dc == 0) {
                MotorsStopAll();
                uart_SendString("BeeDrone> PWMGen channels stopped.\n");
            }
        } else {
            uart_BufferFlush();
        }
    }
}