/*
 * timer.h
 *
 * Created: 25-1-2017 20:09:16
 *  Author: Dorus
 */ 


#ifndef TIMER_H_
#define TIMER_H_


#define F_CPU 16000000UL

typedef void (*Timer0Callback_t)(void);

int8_t timer_Init(uint16_t frequency, Timer0Callback_t callback);

void timer_Start(void);

void timer_Stop(void);

#endif /* TIMER_H_ */